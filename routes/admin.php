<?php


Auth::routes();

Route::get('/', 'HomeController@index')->name('dashboard');
Route::get('/home', 'HomeController@index')->name('dashboard');
Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('about', 'HomeController@index');

Route::resource('roles', 'RoleController');

Route::resource('modules', 'ModuleController');

Route::get('/module/step1/{id?}', 'ModuleController@getStep1')->name('modules.create');
Route::get('/module/step2/{tablename?}', 'ModuleController@getStep2')->name('modules.create');
Route::get('/getJoinFields/{tablename?}', 'ModuleController@getJoinFields');
Route::get('/module/step3/{tablename?}', 'ModuleController@getStep3')->name('modules.create');

Route::post('/step1', 'ModuleController@postStep1');
Route::post('/step2', 'ModuleController@postStep2');
Route::post('/step3', 'ModuleController@postStep3');


Route::resource('users', 'UserController');

Route::resource('permissions', 'PermissionController');

//Route::resource('profile', 'UserController');

Route::get('user/profile', 'UserController@profile')->name('users.profile');
Route::get('profiles', 'UserController@profile')->name('profiles');
//Route::patch('users/profile-update/{id}', 'UserController@profileUpdate')->name('users.profile-update');

Route::resource('languages', 'LanguageController');

Route::resource('pages', 'PageController');

Route::resource('contactus', 'ContactUsController');

Route::resource('notifications', 'NotificationController');

Route::resource('menus', 'MenuController');

//Menu #TODO need to be fixed
Route::get('statusChange/{id}', 'MenuController@statusChange');

Route::resource('settings', 'SettingController');



Route::resource('add-new-areas', 'AddNewAreaController');

Route::resource('areas', 'AreaController');

Route::resource('blocks', 'BlockController');
Route::get('blocks/create/{id}', 'BlockController@create');
Route::get('blocks/store/{id}', 'BlockController@store');

Route::resource('projects', 'ProjectController');

Route::resource('properties', 'PropertyController');
Route::get('properties/create/{id}', 'PropertyController@create');
Route::get('properties/store/{id}', 'PropertyController@store');

Route::resource('modes', 'ModeController');

Route::resource('schedules', 'ScheduleController');
Route::get('schedules/create/{id}', 'ScheduleController@create');
Route::get('schedules/store/{id}', 'ScheduleController@store');

Route::resource('custom-schedules', 'CustomScheduleController');

Route::resource('types', 'TypeController');

Route::resource('employees', 'EmployeeController');

Route::resource('clients', 'ClientController');

Route::resource('clients', 'ClientController');

Route::resource('employee-types', 'EmployeeTypeController');

Route::resource('designations', 'DesignationController');

//Route::get('/project/view', 'ProjectController@view');

Route::get('/project/view','ProjectController@view');
//Route::get('/project/view/1','ProjectController@welcome');


//Route::get('project/view', function ($view) {
////    return view('welcome');
////        return 'Hello';
//    return $view->id;
//});


Route::resource('inquiries', 'InquiryController');

Route::resource('bookings', 'BookingController');


Route::post('bookings/fetcharea', 'BookingController@fetcharea');

Route::post('bookings/fetchblock', 'BookingController@fetchblock');

Route::post('bookings/fetchproperty', 'BookingController@fetchProperties');

Route::resource('banks', 'BankController');

Route::resource('payments', 'PaymentController');
Route::get('payments/create/{id}', 'PaymentController@create');
Route::get('payments/store/{id}', 'PaymentController@store');
