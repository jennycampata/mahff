<?php

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Images Resize Route
Route::get('/resize/{img}', function ($img) {

    ob_end_clean();
    try {
        $w = request()->get('w');
        $h = request()->get('h');
        if ($h && $w) {
            // Image Handler lib
            return Image::make(asset("storage/app/$img"))->resize($h, $w, function ($c) {
                $c->aspectRatio();
                $c->upsize();
            })->response('png');
        } else {
            return response(file_get_contents(storage_path("/app/$img")))
                ->header('Content-Type', 'image/png');
        }

    } catch (\Exception $e) {
//        dd($e->getMessage());
        return abort(404, $e->getMessage());
    }
})->name('resize')->where('img', '(.*)');


/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

## No Token Required
Route::post('v1/register', 'AuthAPIController@register')->name('register');

Route::post('v1/login', 'AuthAPIController@login')->name('login');
Route::post('v1/social_login', 'AuthAPIController@socialLogin')->name('socialLogin');

Route::get('v1/forget-password', 'AuthAPIController@getForgetPasswordCode')->name('forget-password');
//Route::post('v1/resend-code', 'AuthAPIController@resendCode');
Route::post('v1/verify-reset-code', 'AuthAPIController@verifyCode')->name('verify-code');
Route::post('v1/reset-password', 'AuthAPIController@updatePassword')->name('reset-password');

if (is_object(JWTAuth::getToken())) {
    ## Token Required to below APIs
    Route::middleware('auth:api')->group(function () {
        ## Token Required to below APIs
        Route::post('v1/logout', 'AuthAPIController@logout');

        Route::post('v1/change-password', 'AuthAPIController@changePassword');

        Route::post('v1/refresh', 'AuthAPIController@refresh');
        Route::post('v1/me', 'AuthAPIController@me');

        Route::resource('v1/users', 'UserAPIController');

        Route::resource('v1/roles', 'RoleAPIController');
        Route::resource('v1/permissions', 'PermissionAPIController');

        Route::resource('v1/languages', 'LanguageAPIController');

        Route::resource('v1/pages', 'PageAPIController');

        Route::resource('v1/contactus', 'ContactUsAPIController');

        Route::resource('v1/notifications', 'NotificationAPIController');

        Route::resource('v1/menus', 'MenuAPIController');

        Route::resource('v1/settings', 'SettingAPIController');
    });
} else {
    ## routes for
}

Route::resource('v1/add-new-areas', 'AddNewAreaAPIController');

Route::resource('v1/areas', 'AreaAPIController');

Route::resource('v1/blocks', 'BlockAPIController');

Route::resource('v1/projects', 'ProjectAPIController');

Route::resource('v1/properties', 'PropertyAPIController');

Route::resource('v1/modes', 'ModeAPIController');

Route::resource('v1/schedules', 'ScheduleAPIController');

Route::resource('v1/custom-schedules', 'CustomScheduleAPIController');

Route::resource('v1/types', 'TypeAPIController');

Route::resource('v1/employees', 'EmployeeAPIController');

Route::resource('v1/clients', 'ClientAPIController');

Route::resource('v1/clients', 'ClientAPIController');

Route::resource('v1/employee-types', 'EmployeeTypeAPIController');

Route::resource('v1/designations', 'DesignationAPIController');

Route::resource('v1/inquiries', 'InquiryAPIController');

Route::resource('v1/bookings', 'BookingAPIController');

Route::resource('v1/banks', 'BankAPIController');

Route::resource('v1/payments', 'PaymentAPIController');