<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateInquiryAPIRequest;
use App\Http\Requests\Api\UpdateInquiryAPIRequest;
use App\Models\Inquiry;
use App\Repositories\Admin\InquiryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class InquiryController
 * @package App\Http\Controllers\Api
 */

class InquiryAPIController extends AppBaseController
{
    /** @var  InquiryRepository */
    private $inquiryRepository;

    public function __construct(InquiryRepository $inquiryRepo)
    {
        $this->inquiryRepository = $inquiryRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/inquiries",
     *      summary="Get a listing of the Inquiries.",
     *      tags={"Inquiry"},
     *      description="Get all Inquiries",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Inquiry")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->inquiryRepository->pushCriteria(new RequestCriteria($request));
        $this->inquiryRepository->pushCriteria(new LimitOffsetCriteria($request));
        $inquiries = $this->inquiryRepository->all();

        return $this->sendResponse($inquiries->toArray(), 'Inquiries retrieved successfully');
    }

    /**
     * @param CreateInquiryAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/inquiries",
     *      summary="Store a newly created Inquiry in storage",
     *      tags={"Inquiry"},
     *      description="Store Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Inquiry that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Inquiry")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Inquiry"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateInquiryAPIRequest $request)
    {
        $inquiries = $this->inquiryRepository->saveRecord($request);

        return $this->sendResponse($inquiries->toArray(), 'Inquiry saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/inquiries/{id}",
     *      summary="Display the specified Inquiry",
     *      tags={"Inquiry"},
     *      description="Get Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Inquiry",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Inquiry"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Inquiry $inquiry */
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            return $this->sendError('Inquiry not found');
        }

        return $this->sendResponse($inquiry->toArray(), 'Inquiry retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateInquiryAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/inquiries/{id}",
     *      summary="Update the specified Inquiry in storage",
     *      tags={"Inquiry"},
     *      description="Update Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Inquiry",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Inquiry that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Inquiry")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Inquiry"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateInquiryAPIRequest $request)
    {
        /** @var Inquiry $inquiry */
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            return $this->sendError('Inquiry not found');
        }

        $inquiry = $this->inquiryRepository->updateRecord($request, $id);

        return $this->sendResponse($inquiry->toArray(), 'Inquiry updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/inquiries/{id}",
     *      summary="Remove the specified Inquiry from storage",
     *      tags={"Inquiry"},
     *      description="Delete Inquiry",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Inquiry",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Inquiry $inquiry */
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            return $this->sendError('Inquiry not found');
        }

        $this->inquiryRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Inquiry deleted successfully');
    }
}
