<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateBlockAPIRequest;
use App\Http\Requests\Api\UpdateBlockAPIRequest;
use App\Models\Block;
use App\Repositories\Admin\BlockRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class BlockController
 * @package App\Http\Controllers\Api
 */

class BlockAPIController extends AppBaseController
{
    /** @var  BlockRepository */
    private $blockRepository;

    public function __construct(BlockRepository $blockRepo)
    {
        $this->blockRepository = $blockRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/blocks",
     *      summary="Get a listing of the Blocks.",
     *      tags={"Block"},
     *      description="Get all Blocks",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Block")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->blockRepository->pushCriteria(new RequestCriteria($request));
        $this->blockRepository->pushCriteria(new LimitOffsetCriteria($request));
        $blocks = $this->blockRepository->all();

        return $this->sendResponse($blocks->toArray(), 'Blocks retrieved successfully');
    }

    /**
     * @param CreateBlockAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/blocks",
     *      summary="Store a newly created Block in storage",
     *      tags={"Block"},
     *      description="Store Block",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Block that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Block")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Block"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateBlockAPIRequest $request)
    {
        $blocks = $this->blockRepository->saveRecord($request);

        return $this->sendResponse($blocks->toArray(), 'Block saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/blocks/{id}",
     *      summary="Display the specified Block",
     *      tags={"Block"},
     *      description="Get Block",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Block",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Block"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Block $block */
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            return $this->sendError('Block not found');
        }

        return $this->sendResponse($block->toArray(), 'Block retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateBlockAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/blocks/{id}",
     *      summary="Update the specified Block in storage",
     *      tags={"Block"},
     *      description="Update Block",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Block",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Block that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Block")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Block"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateBlockAPIRequest $request)
    {
        /** @var Block $block */
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            return $this->sendError('Block not found');
        }

        $block = $this->blockRepository->updateRecord($request, $id);

        return $this->sendResponse($block->toArray(), 'Block updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/blocks/{id}",
     *      summary="Remove the specified Block from storage",
     *      tags={"Block"},
     *      description="Delete Block",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Block",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Block $block */
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            return $this->sendError('Block not found');
        }

        $this->blockRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Block deleted successfully');
    }
}
