<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateDesignationAPIRequest;
use App\Http\Requests\Api\UpdateDesignationAPIRequest;
use App\Models\Designation;
use App\Repositories\Admin\DesignationRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class DesignationController
 * @package App\Http\Controllers\Api
 */

class DesignationAPIController extends AppBaseController
{
    /** @var  DesignationRepository */
    private $designationRepository;

    public function __construct(DesignationRepository $designationRepo)
    {
        $this->designationRepository = $designationRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/designations",
     *      summary="Get a listing of the Designations.",
     *      tags={"Designation"},
     *      description="Get all Designations",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Designation")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->designationRepository->pushCriteria(new RequestCriteria($request));
        $this->designationRepository->pushCriteria(new LimitOffsetCriteria($request));
        $designations = $this->designationRepository->all();

        return $this->sendResponse($designations->toArray(), 'Designations retrieved successfully');
    }

    /**
     * @param CreateDesignationAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/designations",
     *      summary="Store a newly created Designation in storage",
     *      tags={"Designation"},
     *      description="Store Designation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Designation that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Designation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Designation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateDesignationAPIRequest $request)
    {
        $designations = $this->designationRepository->saveRecord($request);

        return $this->sendResponse($designations->toArray(), 'Designation saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/designations/{id}",
     *      summary="Display the specified Designation",
     *      tags={"Designation"},
     *      description="Get Designation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Designation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Designation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Designation $designation */
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            return $this->sendError('Designation not found');
        }

        return $this->sendResponse($designation->toArray(), 'Designation retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateDesignationAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/designations/{id}",
     *      summary="Update the specified Designation in storage",
     *      tags={"Designation"},
     *      description="Update Designation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Designation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Designation that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Designation")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Designation"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateDesignationAPIRequest $request)
    {
        /** @var Designation $designation */
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            return $this->sendError('Designation not found');
        }

        $designation = $this->designationRepository->updateRecord($request, $id);

        return $this->sendResponse($designation->toArray(), 'Designation updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/designations/{id}",
     *      summary="Remove the specified Designation from storage",
     *      tags={"Designation"},
     *      description="Delete Designation",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Designation",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Designation $designation */
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            return $this->sendError('Designation not found');
        }

        $this->designationRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Designation deleted successfully');
    }
}
