<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateModeAPIRequest;
use App\Http\Requests\Api\UpdateModeAPIRequest;
use App\Models\Mode;
use App\Repositories\Admin\ModeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class ModeController
 * @package App\Http\Controllers\Api
 */

class ModeAPIController extends AppBaseController
{
    /** @var  ModeRepository */
    private $modeRepository;

    public function __construct(ModeRepository $modeRepo)
    {
        $this->modeRepository = $modeRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/modes",
     *      summary="Get a listing of the Modes.",
     *      tags={"Mode"},
     *      description="Get all Modes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/Mode")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->modeRepository->pushCriteria(new RequestCriteria($request));
        $this->modeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $modes = $this->modeRepository->all();

        return $this->sendResponse($modes->toArray(), 'Modes retrieved successfully');
    }

    /**
     * @param CreateModeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/modes",
     *      summary="Store a newly created Mode in storage",
     *      tags={"Mode"},
     *      description="Store Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mode that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mode")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateModeAPIRequest $request)
    {
        $modes = $this->modeRepository->saveRecord($request);

        return $this->sendResponse($modes->toArray(), 'Mode saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/modes/{id}",
     *      summary="Display the specified Mode",
     *      tags={"Mode"},
     *      description="Get Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var Mode $mode */
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            return $this->sendError('Mode not found');
        }

        return $this->sendResponse($mode->toArray(), 'Mode retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateModeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/modes/{id}",
     *      summary="Update the specified Mode in storage",
     *      tags={"Mode"},
     *      description="Update Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Mode that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/Mode")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/Mode"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateModeAPIRequest $request)
    {
        /** @var Mode $mode */
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            return $this->sendError('Mode not found');
        }

        $mode = $this->modeRepository->updateRecord($request, $id);

        return $this->sendResponse($mode->toArray(), 'Mode updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/modes/{id}",
     *      summary="Remove the specified Mode from storage",
     *      tags={"Mode"},
     *      description="Delete Mode",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of Mode",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var Mode $mode */
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            return $this->sendError('Mode not found');
        }

        $this->modeRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Mode deleted successfully');
    }
}
