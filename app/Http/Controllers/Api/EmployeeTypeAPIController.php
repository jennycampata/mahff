<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateEmployeeTypeAPIRequest;
use App\Http\Requests\Api\UpdateEmployeeTypeAPIRequest;
use App\Models\EmployeeType;
use App\Repositories\Admin\EmployeeTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class EmployeeTypeController
 * @package App\Http\Controllers\Api
 */

class EmployeeTypeAPIController extends AppBaseController
{
    /** @var  EmployeeTypeRepository */
    private $employeeTypeRepository;

    public function __construct(EmployeeTypeRepository $employeeTypeRepo)
    {
        $this->employeeTypeRepository = $employeeTypeRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/employee-types",
     *      summary="Get a listing of the EmployeeTypes.",
     *      tags={"EmployeeType"},
     *      description="Get all EmployeeTypes",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/EmployeeType")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->employeeTypeRepository->pushCriteria(new RequestCriteria($request));
        $this->employeeTypeRepository->pushCriteria(new LimitOffsetCriteria($request));
        $employeeTypes = $this->employeeTypeRepository->all();

        return $this->sendResponse($employeeTypes->toArray(), 'Employee Types retrieved successfully');
    }

    /**
     * @param CreateEmployeeTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/employee-types",
     *      summary="Store a newly created EmployeeType in storage",
     *      tags={"EmployeeType"},
     *      description="Store EmployeeType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EmployeeType that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EmployeeType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EmployeeType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateEmployeeTypeAPIRequest $request)
    {
        $employeeTypes = $this->employeeTypeRepository->saveRecord($request);

        return $this->sendResponse($employeeTypes->toArray(), 'Employee Type saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/employee-types/{id}",
     *      summary="Display the specified EmployeeType",
     *      tags={"EmployeeType"},
     *      description="Get EmployeeType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EmployeeType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EmployeeType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var EmployeeType $employeeType */
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            return $this->sendError('Employee Type not found');
        }

        return $this->sendResponse($employeeType->toArray(), 'Employee Type retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateEmployeeTypeAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/employee-types/{id}",
     *      summary="Update the specified EmployeeType in storage",
     *      tags={"EmployeeType"},
     *      description="Update EmployeeType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EmployeeType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="EmployeeType that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/EmployeeType")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/EmployeeType"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateEmployeeTypeAPIRequest $request)
    {
        /** @var EmployeeType $employeeType */
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            return $this->sendError('Employee Type not found');
        }

        $employeeType = $this->employeeTypeRepository->updateRecord($request, $id);

        return $this->sendResponse($employeeType->toArray(), 'EmployeeType updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/employee-types/{id}",
     *      summary="Remove the specified EmployeeType from storage",
     *      tags={"EmployeeType"},
     *      description="Delete EmployeeType",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of EmployeeType",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var EmployeeType $employeeType */
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            return $this->sendError('Employee Type not found');
        }

        $this->employeeTypeRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Employee Type deleted successfully');
    }
}
