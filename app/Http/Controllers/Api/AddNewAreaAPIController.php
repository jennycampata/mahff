<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateAddNewAreaAPIRequest;
use App\Http\Requests\Api\UpdateAddNewAreaAPIRequest;
use App\Models\AddNewArea;
use App\Repositories\Admin\AddNewAreaRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class AddNewAreaController
 * @package App\Http\Controllers\Api
 */

class AddNewAreaAPIController extends AppBaseController
{
    /** @var  AddNewAreaRepository */
    private $addNewAreaRepository;

    public function __construct(AddNewAreaRepository $addNewAreaRepo)
    {
        $this->addNewAreaRepository = $addNewAreaRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/add-new-areas",
     *      summary="Get a listing of the AddNewAreas.",
     *      tags={"AddNewArea"},
     *      description="Get all AddNewAreas",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/AddNewArea")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->addNewAreaRepository->pushCriteria(new RequestCriteria($request));
        $this->addNewAreaRepository->pushCriteria(new LimitOffsetCriteria($request));
        $addNewAreas = $this->addNewAreaRepository->all();

        return $this->sendResponse($addNewAreas->toArray(), 'Add New Areas retrieved successfully');
    }

    /**
     * @param CreateAddNewAreaAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/add-new-areas",
     *      summary="Store a newly created AddNewArea in storage",
     *      tags={"AddNewArea"},
     *      description="Store AddNewArea",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AddNewArea that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AddNewArea")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AddNewArea"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateAddNewAreaAPIRequest $request)
    {
        $addNewAreas = $this->addNewAreaRepository->saveRecord($request);

        return $this->sendResponse($addNewAreas->toArray(), 'Add New Area saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/add-new-areas/{id}",
     *      summary="Display the specified AddNewArea",
     *      tags={"AddNewArea"},
     *      description="Get AddNewArea",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AddNewArea",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AddNewArea"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var AddNewArea $addNewArea */
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            return $this->sendError('Add New Area not found');
        }

        return $this->sendResponse($addNewArea->toArray(), 'Add New Area retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateAddNewAreaAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/add-new-areas/{id}",
     *      summary="Update the specified AddNewArea in storage",
     *      tags={"AddNewArea"},
     *      description="Update AddNewArea",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AddNewArea",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="AddNewArea that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/AddNewArea")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/AddNewArea"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateAddNewAreaAPIRequest $request)
    {
        /** @var AddNewArea $addNewArea */
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            return $this->sendError('Add New Area not found');
        }

        $addNewArea = $this->addNewAreaRepository->updateRecord($request, $id);

        return $this->sendResponse($addNewArea->toArray(), 'AddNewArea updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/add-new-areas/{id}",
     *      summary="Remove the specified AddNewArea from storage",
     *      tags={"AddNewArea"},
     *      description="Delete AddNewArea",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of AddNewArea",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var AddNewArea $addNewArea */
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            return $this->sendError('Add New Area not found');
        }

        $this->addNewAreaRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Add New Area deleted successfully');
    }
}
