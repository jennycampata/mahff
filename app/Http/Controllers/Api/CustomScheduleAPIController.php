<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\CreateCustomScheduleAPIRequest;
use App\Http\Requests\Api\UpdateCustomScheduleAPIRequest;
use App\Models\CustomSchedule;
use App\Repositories\Admin\CustomScheduleRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use Prettus\Repository\Criteria\RequestCriteria;
use Illuminate\Http\Response;

/**
 * Class CustomScheduleController
 * @package App\Http\Controllers\Api
 */

class CustomScheduleAPIController extends AppBaseController
{
    /** @var  CustomScheduleRepository */
    private $customScheduleRepository;

    public function __construct(CustomScheduleRepository $customScheduleRepo)
    {
        $this->customScheduleRepository = $customScheduleRepo;
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     * @return Response
     *
     * @SWG\Get(
     *      path="/custom-schedules",
     *      summary="Get a listing of the CustomSchedules.",
     *      tags={"CustomSchedule"},
     *      description="Get all CustomSchedules",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="limit",
     *          description="Change the Default Record Count. If not found, Returns All Records in DB.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *     @SWG\Parameter(
     *          name="offset",
     *          description="Change the Default Offset of the Query. If not found, 0 will be used.",
     *          type="integer",
     *          required=false,
     *          in="query"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/CustomSchedule")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->customScheduleRepository->pushCriteria(new RequestCriteria($request));
        $this->customScheduleRepository->pushCriteria(new LimitOffsetCriteria($request));
        $customSchedules = $this->customScheduleRepository->all();

        return $this->sendResponse($customSchedules->toArray(), 'Custom Schedules retrieved successfully');
    }

    /**
     * @param CreateCustomScheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/custom-schedules",
     *      summary="Store a newly created CustomSchedule in storage",
     *      tags={"CustomSchedule"},
     *      description="Store CustomSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomSchedule that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomSchedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomSchedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreateCustomScheduleAPIRequest $request)
    {
        $customSchedules = $this->customScheduleRepository->saveRecord($request);

        return $this->sendResponse($customSchedules->toArray(), 'Custom Schedule saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/custom-schedules/{id}",
     *      summary="Display the specified CustomSchedule",
     *      tags={"CustomSchedule"},
     *      description="Get CustomSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomSchedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomSchedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var CustomSchedule $customSchedule */
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            return $this->sendError('Custom Schedule not found');
        }

        return $this->sendResponse($customSchedule->toArray(), 'Custom Schedule retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdateCustomScheduleAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/custom-schedules/{id}",
     *      summary="Update the specified CustomSchedule in storage",
     *      tags={"CustomSchedule"},
     *      description="Update CustomSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomSchedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="CustomSchedule that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/CustomSchedule")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/CustomSchedule"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdateCustomScheduleAPIRequest $request)
    {
        /** @var CustomSchedule $customSchedule */
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            return $this->sendError('Custom Schedule not found');
        }

        $customSchedule = $this->customScheduleRepository->updateRecord($request, $id);

        return $this->sendResponse($customSchedule->toArray(), 'CustomSchedule updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/custom-schedules/{id}",
     *      summary="Remove the specified CustomSchedule from storage",
     *      tags={"CustomSchedule"},
     *      description="Delete CustomSchedule",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="Authorization",
     *          description="User Auth Token{ Bearer ABC123 }",
     *          type="string",
     *          required=true,
     *          default="Bearer ABC123",
     *          in="header"
     *      ),
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of CustomSchedule",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var CustomSchedule $customSchedule */
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            return $this->sendError('Custom Schedule not found');
        }

        $this->customScheduleRepository->deleteRecord($id);

        return $this->sendResponse($id, 'Custom Schedule deleted successfully');
    }
}
