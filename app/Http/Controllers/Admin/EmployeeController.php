<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\EmployeeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateEmployeeRequest;
use App\Http\Requests\Admin\UpdateEmployeeRequest;
use App\Models\Employee;
use App\Models\UserDetail;
use App\Repositories\Admin\EmployeeRepository;
use App\Repositories\Admin\DesignationRepository;
use App\Repositories\Admin\ProjectRepository;
use App\Repositories\Admin\EmployeeTypeRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\EmployeeDetails;
use App\Repositories\Admin\UserRepository;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class EmployeeController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  EmployeeRepository */
    private $employeeRepository;

    private $designationRepository;
    private $projectRepository;
    private $employeeTypeRepository;

    private $userRepository;


    public function __construct(EmployeeRepository $employeeRepo,
                                DesignationRepository $designationRepo, ProjectRepository $projectRepo, EmployeeTypeRepository $employeeTypeRepo, UserRepository $userRepo)
    {
        $this->employeeRepository = $employeeRepo;
        $this->designationRepository = $designationRepo;
        $this->projectRepository = $projectRepo;
        $this->employeeTypeRepository = $employeeTypeRepo;
        $this->userRepository = $userRepo;
        $this->ModelName = 'employees';
        $this->BreadCrumbName = 'Employees';
    }

    /**
     * Display a listing of the Employee.
     *
     * @param EmployeeDataTable $employeeDataTable
     * @return Response
     */
    public function index(EmployeeDataTable $employeeDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $employeeDataTable->render('admin.employees.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Employee.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        $designation = $this->designationRepository->pluck('name', 'id');
        $project = $this->projectRepository->pluck('name', 'id');
        $employeetype = $this->employeeTypeRepository->pluck('name', 'id');

        return view('admin.employees.create')->with([
            'title'        => $this->BreadCrumbName,
            'designation'  => $designation,
            'project'      => $project,
            'employeetype' => $employeetype
        ]);
    }

    /**
     * Store a newly created Employee in storage.
     *
     * @param CreateEmployeeRequest $request
     *
     * @return Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        $employee = $this->employeeRepository->saveRecord($request);

        $details = [];
        $details['project_id'] = $request->project_id;
        $details['user_id'] = $employee->id;
        $details['designation_id'] = $request->designation_id;
        $details['employee_type_id'] = $request->employee_type_id;
        $details['contact_no'] = $request->contact_no;
        $details['address'] = $request->address;
        $details['cnic_no'] = $request->cnic_no;
        $details['gender'] = $request->gender;
        $details['marriage_status'] = $request->marriage_status;
        $details['salary'] = $request->salary;
        $userDetail = [];
        $userDetail['first_name'] = $request->name;
        $userDetail['user_id'] = $employee->id;
        UserDetail::where('id', $employee->id)->update($userDetail);
        $employ = EmployeeDetails::create($details);
        $this->userRepository->attachRole($employee->id, Employee::EMPLOYEE);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.employees.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.employees.edit', $employee->id));
        } else {
            $redirect_to = redirect(route('admin.employees.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName,
//            'vendor'   => $vendor
        ]);
    }

    /**
     * Display the specified Employee.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $employee = $this->employeeRepository->findWithoutFail($id);

        if (empty($employee)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employees.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $employee);
        return view('admin.employees.show')->with(['employee' => $employee, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Employee.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $employee = $this->employeeRepository->findWithoutFail($id);

        if (empty($employee)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employees.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $employee);
        return view('admin.employees.edit')->with(['employee' => $employee, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Employee in storage.
     *
     * @param  int $id
     * @param UpdateEmployeeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmployeeRequest $request)
    {
        $employee = $this->employeeRepository->findWithoutFail($id);

        if (empty($employee)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employees.index'));
        }

        $employee = $this->employeeRepository->updateRecord($request, $employee);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.employees.create'));
        } else {
            $redirect_to = redirect(route('admin.employees.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Employee from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $employee = $this->employeeRepository->findWithoutFail($id);

        if (empty($employee)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employees.index'));
        }

        $this->employeeRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.employees.index'))->with(['title' => $this->BreadCrumbName]);
    }


}
