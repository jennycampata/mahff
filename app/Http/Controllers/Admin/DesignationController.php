<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\DesignationDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateDesignationRequest;
use App\Http\Requests\Admin\UpdateDesignationRequest;
use App\Repositories\Admin\DesignationRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class DesignationController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  DesignationRepository */
    private $designationRepository;

    public function __construct(DesignationRepository $designationRepo)
    {
        $this->designationRepository = $designationRepo;
        $this->ModelName = 'designations';
        $this->BreadCrumbName = 'Designations';
    }

    /**
     * Display a listing of the Designation.
     *
     * @param DesignationDataTable $designationDataTable
     * @return Response
     */
    public function index(DesignationDataTable $designationDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $designationDataTable->render('admin.designations.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Designation.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.designations.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Designation in storage.
     *
     * @param CreateDesignationRequest $request
     *
     * @return Response
     */
    public function store(CreateDesignationRequest $request)
    {
        $designation = $this->designationRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.designations.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.designations.edit', $designation->id));
        } else {
            $redirect_to = redirect(route('admin.designations.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Designation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.designations.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $designation);
        return view('admin.designations.show')->with(['designation' => $designation, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Designation.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.designations.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $designation);
        return view('admin.designations.edit')->with(['designation' => $designation, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Designation in storage.
     *
     * @param  int              $id
     * @param UpdateDesignationRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDesignationRequest $request)
    {
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.designations.index'));
        }

        $designation = $this->designationRepository->updateRecord($request, $designation);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.designations.create'));
        } else {
            $redirect_to = redirect(route('admin.designations.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Designation from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $designation = $this->designationRepository->findWithoutFail($id);

        if (empty($designation)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.designations.index'));
        }

        $this->designationRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.designations.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
