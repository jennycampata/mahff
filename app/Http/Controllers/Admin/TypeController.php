<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\TypeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateTypeRequest;
use App\Http\Requests\Admin\UpdateTypeRequest;
use App\Repositories\Admin\TypeRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class TypeController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  TypeRepository */
    private $typeRepository;

    public function __construct(TypeRepository $typeRepo)
    {
        $this->typeRepository = $typeRepo;
        $this->ModelName = 'types';
        $this->BreadCrumbName = 'Types';
    }

    /**
     * Display a listing of the Type.
     *
     * @param TypeDataTable $typeDataTable
     * @return Response
     */
    public function index(TypeDataTable $typeDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $typeDataTable->render('admin.types.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Type.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.types.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Type in storage.
     *
     * @param CreateTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateTypeRequest $request)
    {
        $type = $this->typeRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.types.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.types.edit', $type->id));
        } else {
            $redirect_to = redirect(route('admin.types.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.types.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $type);
        return view('admin.types.show')->with(['type' => $type, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Type.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.types.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $type);
        return view('admin.types.edit')->with(['type' => $type, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Type in storage.
     *
     * @param  int              $id
     * @param UpdateTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTypeRequest $request)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.types.index'));
        }

        $type = $this->typeRepository->updateRecord($request, $type);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.types.create'));
        } else {
            $redirect_to = redirect(route('admin.types.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Type from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $type = $this->typeRepository->findWithoutFail($id);

        if (empty($type)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.types.index'));
        }

        $this->typeRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.types.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
