<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\ScheduleDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateScheduleRequest;
use App\Http\Requests\Admin\UpdateScheduleRequest;
use App\Models\Schedule;
use App\Models\ScheduleDetails;
use App\Repositories\Admin\ScheduleRepository;
use App\Repositories\Admin\AreaRepository;
use App\Repositories\Admin\ProjectRepository;
use App\Repositories\Admin\ModeRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class ScheduleController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  ScheduleRepository */
    private $scheduleRepository;
    private $projectRepository;
    private $areaRepository;
    private $modeRepository;
    public function __construct(ScheduleRepository $scheduleRepo, AreaRepository $areaRepo, ProjectRepository $projectRepo,ModeRepository $modeRepo)
    {
        $this->scheduleRepository = $scheduleRepo;
        $this->areaRepository=$areaRepo;
        $this->projectRepository=$projectRepo;
        $this->modeRepository=$modeRepo;
        $this->ModelName = 'schedules';
        $this->BreadCrumbName = 'Schedules';
    }

    /**
     * Display a listing of the Schedule.
     *
     * @param ScheduleDataTable $scheduleDataTable
     * @return Response
     */
    public function index(ScheduleDataTable $scheduleDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $scheduleDataTable->render('admin.schedules.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Schedule.
     *
     * @return Response
     */
    public function create($id)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        $area=$this->areaRepository->pluck('name','id');
        $project=$this->projectRepository->pluck('name','id');
        $mode=$this->modeRepository->pluck('name','id');

        return view('admin.schedules.create')->with([
            'title' => $this->BreadCrumbName,
            'area'=>$area,
            'project'=>$project,
            'mode'=>$mode,
            'project_id'=>$id
        ]);



    }

    /**
     * Store a newly created Schedule in storage.
     *
     * @param CreateScheduleRequest $request
     *
     * @return Response
     */
    public function store(CreateScheduleRequest $request)
    {
        $verify = $this->scheduleRepository->findWhere([
            'area_id'=> $request->area_id,
            'project_id'=> $request->project_id,
            ])->first();

        if($verify != null && $request->custom_schedule == null){
            $status['status'] = Schedule::INACTIVE;
            $this->scheduleRepository->updateRecord($status, $verify);
        }
        if($request->custom_schedule == 1){
            $request['status']= Schedule::CUSTOMSCH;
        }
        $schedule = $this->scheduleRepository->saveRecord($request);
        $details = [];
        foreach ($request->mode_id as $key=>$value){
            $details['mode_id']= $request->mode_id[$key];
            $details['time_duration']= $request->time_duration[$key];
            $details['amount']= $request->amount[$key];
            $details['schedule_id']= $schedule->id;
            ScheduleDetails::create($details);
        }



        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.schedules.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.schedules.edit', $schedule->id));
        } else {
            $redirect_to = redirect(route('admin.schedules.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Schedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $schedule);
        return view('admin.schedules.show')->with(['schedule' => $schedule, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Schedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);
        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $schedule);
        $area=$this->areaRepository->pluck('name','id');
        $project=$this->projectRepository->pluck('name','id');
        $mode=$this->modeRepository->pluck('name','id');
        return view('admin.schedules.edit')->with([
            'schedule' => $schedule,
            'title' => $this->BreadCrumbName,
            'area'=>$area,
            'project'=>$project,
            'mode'=>$mode
        ]);
    }

    /**
     * Update the specified Schedule in storage.
     *
     * @param  int              $id
     * @param UpdateScheduleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateScheduleRequest $request)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        $schedule = $this->scheduleRepository->updateRecord($request, $schedule);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.schedules.create'));
        } else {
            $redirect_to = redirect(route('admin.schedules.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Schedule from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $schedule = $this->scheduleRepository->findWithoutFail($id);

        if (empty($schedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.schedules.index'));
        }

        $this->scheduleRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.schedules.index'))->with(['title' => $this->BreadCrumbName]);
    }

}
