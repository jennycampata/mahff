<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\PropertyDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreatePropertyRequest;
use App\Http\Requests\Admin\UpdatePropertyRequest;
use App\Repositories\Admin\PropertyRepository;
use App\Repositories\Admin\AddNewAreaRepository;
use App\Repositories\Admin\BlockRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class PropertyController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  PropertyRepository */
    private $propertyRepository;
    private    $addnewareaRepository;
    private  $blockRepository;

    public function __construct(PropertyRepository $propertyRepo ,AddNewAreaRepository $addnewareaRepo , BlockRepository $blockRepo )
    {
        $this->propertyRepository = $propertyRepo;
        $this->ModelName = 'properties';
        $this->BreadCrumbName = 'Properties';
        $this->addnewareaRepository = $addnewareaRepo;
        $this->blockRepository=$blockRepo;
    }

    /**
     * Display a listing of the Property.
     *
     * @param PropertyDataTable $propertyDataTable
     * @return Response
     */
    public function index(PropertyDataTable $propertyDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $propertyDataTable->render('admin.properties.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Property.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);

        $area = $this->addnewareaRepository->pluck('name','id');
        $block = $this->blockRepository->pluck('name','id');

        return view('admin.properties.create')->with([
            'title' => $this->BreadCrumbName,
            'area'=> $area,
            'block' => $block,
            'project_id' => $request->get('project_id',null),
        ]);

    }

    /**
     * Store a newly created Property in storage.
     *
     * @param CreatePropertyRequest $request
     *
     * @return Response
     */
    public function store(CreatePropertyRequest $request)
    {

        $property = $this->propertyRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.projects.show',$request->project_id));
        } else {
            $redirect_to = redirect(route('admin.projects.show',$request->project_id));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Property.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $property = $this->propertyRepository->findWithoutFail($id);

        if (empty($property)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.properties.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $property);
        return view('admin.properties.show')->with(['property' => $property, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Property.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $property = $this->propertyRepository->findWithoutFail($id);
        $area = $this->addnewareaRepository->pluck('name','id');
        $block = $this->blockRepository->pluck('name','id');

        if (empty($property)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.properties.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $property);
        return view('admin.properties.edit')->with([
            'property' => $property,
            'title' => $this->BreadCrumbName,
            'area' => $area,
            'block' => $block
            ]);
    }

    /**
     * Update the specified Property in storage.
     *
     * @param  int              $id
     * @param UpdatePropertyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePropertyRequest $request)
    {
        $property = $this->propertyRepository->findWithoutFail($id);

        if (empty($property)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.properties.index'));
        }

        $property = $this->propertyRepository->updateRecord($request, $property);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.projects.show',$request->project_id));
        } else {
            $redirect_to = redirect(route('admin.projects.show',$request->project_id));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Property from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $property = $this->propertyRepository->findWithoutFail($id);

        if (empty($property)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.projects.index'));
        }

        $this->propertyRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.projects.show'))->with(['title' => $this->BreadCrumbName]);
    }
}
