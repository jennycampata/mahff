<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\ModeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateModeRequest;
use App\Http\Requests\Admin\UpdateModeRequest;
use App\Repositories\Admin\ModeRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class ModeController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  ModeRepository */
    private $modeRepository;

    public function __construct(ModeRepository $modeRepo)
    {
        $this->modeRepository = $modeRepo;
        $this->ModelName = 'modes';
        $this->BreadCrumbName = 'Modes';
    }

    /**
     * Display a listing of the Mode.
     *
     * @param ModeDataTable $modeDataTable
     * @return Response
     */
    public function index(ModeDataTable $modeDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $modeDataTable->render('admin.modes.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Mode.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.modes.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Mode in storage.
     *
     * @param CreateModeRequest $request
     *
     * @return Response
     */
    public function store(CreateModeRequest $request)
    {
        $mode = $this->modeRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.modes.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.modes.edit', $mode->id));
        } else {
            $redirect_to = redirect(route('admin.modes.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Mode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.modes.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $mode);
        return view('admin.modes.show')->with(['mode' => $mode, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Mode.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.modes.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $mode);
        return view('admin.modes.edit')->with(['mode' => $mode, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Mode in storage.
     *
     * @param  int              $id
     * @param UpdateModeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateModeRequest $request)
    {
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.modes.index'));
        }

        $mode = $this->modeRepository->updateRecord($request, $mode);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.modes.create'));
        } else {
            $redirect_to = redirect(route('admin.modes.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Mode from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $mode = $this->modeRepository->findWithoutFail($id);

        if (empty($mode)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.modes.index'));
        }

        $this->modeRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.modes.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
