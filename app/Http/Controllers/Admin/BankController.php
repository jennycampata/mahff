<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\BankDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBankRequest;
use App\Http\Requests\Admin\UpdateBankRequest;
use App\Repositories\Admin\BankRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\Admin\ProjectRepository;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class BankController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  BankRepository */
    private $bankRepository;
    private $projectRepository;

    public function __construct(BankRepository $bankRepo, ProjectRepository $projectRepository)
    {
        $this->bankRepository = $bankRepo;
        $this->projectRepository = $projectRepository;
        $this->ModelName = 'banks';
        $this->BreadCrumbName = 'Banks';
    }

    /**
     * Display a listing of the Bank.
     *
     * @param BankDataTable $bankDataTable
     * @return Response
     */
    public function index(BankDataTable $bankDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $bankDataTable->render('admin.banks.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Bank.
     *
     * @return Response
     */
    public function create()
    {
        $project = $this->projectRepository->pluck('name', 'id');
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.banks.create')->with([
            'title'   => $this->BreadCrumbName,
            'project' => $project,
        ]);
    }

    /**
     * Store a newly created Bank in storage.
     *
     * @param CreateBankRequest $request
     *
     * @return Response
     */
    public function store(CreateBankRequest $request)
    {
        $bank = $this->bankRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.banks.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.banks.edit', $bank->id));
        } else {
            $redirect_to = redirect(route('admin.banks.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $bank = $this->bankRepository->findWithoutFail($id);

        if (empty($bank)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.banks.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $bank);
        return view('admin.banks.show')->with(['bank' => $bank, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Bank.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $bank = $this->bankRepository->findWithoutFail($id);

        if (empty($bank)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.banks.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $bank);
        return view('admin.banks.edit')->with(['bank' => $bank, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Bank in storage.
     *
     * @param  int $id
     * @param UpdateBankRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBankRequest $request)
    {
        $bank = $this->bankRepository->findWithoutFail($id);

        if (empty($bank)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.banks.index'));
        }

        $bank = $this->bankRepository->updateRecord($request, $bank);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.banks.create'));
        } else {
            $redirect_to = redirect(route('admin.banks.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Bank from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $bank = $this->bankRepository->findWithoutFail($id);

        if (empty($bank)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.banks.index'));
        }

        $this->bankRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.banks.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
