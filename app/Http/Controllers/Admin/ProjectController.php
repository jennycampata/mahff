<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\Admin\BlockDataTable;
use App\DataTables\Admin\ModeDataTable;
use App\DataTables\Admin\PropertyDataTable;
use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\ProjectDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateProjectRequest;
use App\Http\Requests\Admin\UpdateProjectRequest;
use App\Repositories\Admin\ProjectRepository;
use App\Repositories\Admin\TypeRepository;
use App\Http\Controllers\AppBaseController;
use http\Env\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class ProjectController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  ProjectRepository */
    private $projectRepository;
    private $typeRepository;

    public function __construct(ProjectRepository $projectRepo,TypeRepository $typeRepo)
    {
        $this->projectRepository = $projectRepo;
        $this->ModelName = 'projects';
        $this->BreadCrumbName = 'Projects';
        $this->typeRepository = $typeRepo;
    }

    /**
     * Display a listing of the Project.
     *
     * @param ProjectDataTable $projectDataTable
     * @return Response
     */
    public function index(ProjectDataTable $projectDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $projectDataTable->render('admin.projects.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Project.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        $type = $this->typeRepository->pluck('name', 'id');
        return view('admin.projects.create')->with([
            'title' => $this->BreadCrumbName,
            'type' => $type


        ]);
    }

    /**
     * Store a newly created Project in storage.
     *
     * @param CreateProjectRequest $request
     *
     * @return Response
     */
    public function store(CreateProjectRequest $request)
    {
        $project = $this->projectRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.projects.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.projects.edit', $project->id));
        } else {
            $redirect_to = redirect(route('admin.projects.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Project.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id, PropertyDataTable $propertyDataTable)
    {

        $project = $this->projectRepository->findWithoutFail($id);
        $propertyDataTable->project_id = $id;
//        $blockDataTable->project_id= $id;

        if (empty($project)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.projects.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $project);
        return $propertyDataTable->render('admin.projects.show',[
            'project' => $project,
            'title' => $this->BreadCrumbName,

        ]);
//        return view('admin.projects.show')->with([
//            'project' => $project,
//            'title' => $this->BreadCrumbName,
//            'propertyDataTable' => $propertyDataTable,
//            'blockDatatable' => $blockDataTable,
//        ]);
    }

    /**
     * Show the form for editing the specified Project.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $project = $this->projectRepository->findWithoutFail($id);
        $type = $this->typeRepository->pluck('name', 'id');


        if (empty($project)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.projects.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $project);
        return view('admin.projects.edit')->with([
            'project' => $project,
            'title' => $this->BreadCrumbName,
            'type' => $type
        ]);
    }

    /**
     * Update the specified Project in storage.
     *
     * @param  int              $id
     * @param UpdateProjectRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateProjectRequest $request)
    {
        $project = $this->projectRepository->findWithoutFail($id);

        if (empty($project)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.projects.index'));
        }

        $project = $this->projectRepository->updateRecord($request, $project);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.projects.create'));
        } else {
            $redirect_to = redirect(route('admin.projects.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Project from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $project = $this->projectRepository->findWithoutFail($id);

        if (empty($project)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.projects.index'));
        }

        $this->projectRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.projects.index'))->with(['title' => $this->BreadCrumbName]);
    }

    public function view(){
//        $name = "John Doe";
//        return View::make("index")->with("name", $name);
//        dd('thhhhhhh');
//        dd(id);

//        return view('admin.projects.fields');

//        return $request;

    }
    public function welcome()
    {
        return view('welcome');
    }



}
