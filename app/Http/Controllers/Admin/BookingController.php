<?php

namespace App\Http\Controllers\Admin;

use App\Criteria\UserCriteria;
use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\BookingDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBookingRequest;
use App\Http\Requests\Admin\UpdateBookingRequest;
use App\Models\Client;
use App\Models\Employee;
use App\Models\Schedule;
use App\Repositories\Admin\AreaRepository;
use App\Repositories\Admin\BookingRepository;
use App\Repositories\Admin\ClientRepository;
use App\Repositories\Admin\ProjectRepository;
use App\Repositories\Admin\BlockRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\Admin\PropertyRepository;
use App\Repositories\Admin\ScheduleRepository;
use App\Repositories\Admin\UserRepository;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class BookingController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  BookingRepository */
    private $bookingRepository;
    private $projectRepository;
    private $blockRepository;
    private $areaRepository;
    private $scheduleRepository;
    private $userRepository;
    private $clientRepository;
    private $propertyRepository;

    public function __construct(PropertyRepository $propertyRepo, UserRepository $userRepo, ClientRepository $clientRepo, ScheduleRepository $scheRepo, BookingRepository $bookingRepo, ProjectRepository $projectrepo, BlockRepository $blockRepo, AreaRepository $areaRepo)
    {
        $this->bookingRepository = $bookingRepo;
        $this->areaRepository = $areaRepo;
        $this->ModelName = 'bookings';
        $this->BreadCrumbName = 'Bookings';
        $this->projectRepository = $projectrepo;
        $this->blockRepository = $blockRepo;
        $this->clientRepository = $clientRepo;
        $this->userRepository = $userRepo;
        $this->scheduleRepository = $scheRepo;
        $this->propertyRepository = $propertyRepo;

    }

    /**
     * Display a listing of the Booking.
     *
     * @param BookingDataTable $bookingDataTable
     * @return Response
     */
    public function index(BookingDataTable $bookingDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $bookingDataTable->render('admin.bookings.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Booking.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        $client = $this->clientRepository->resetCriteria()->
        pushCriteria(new UserCriteria(['role' => Client::Client]))->pluck('name', 'id');

        $employee = $this->userRepository->resetCriteria()->
        pushCriteria(new UserCriteria(['role' => Employee::EMPLOYEE]))->pluck('name', 'id');

        $project = $this->projectRepository->pluck('name', 'id');

        $block = [];
//            $this->blockRepository->pluck('name', 'id');

        //Fetch Area Belongs to Project
        $area = $this->areaRepository->pluck('name', 'id');

        //Fetch Custom Schedule Related Area
        $schedule = $this->scheduleRepository->findWhere(['status' => Schedule::CUSTOMSCH])->pluck('name', 'id');
        //Fetch Properties Belong To Blocks

        return view('admin.bookings.create')->with
        ([
            'title'    => $this->BreadCrumbName,
            'project'  => $project,
            'block'    => $block,
            'area'     => $area,
            'client'   => $client,
            'schedule' => $schedule,
            'employee' => $employee,
        ]);
    }

    /**
     * Store a newly created Booking in storage.
     *
     * @param CreateBookingRequest $request
     *
     * @return Response
     */
    public function store(CreateBookingRequest $request)
    {
        $booking = $this->bookingRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.bookings.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.bookings.edit', $booking->id));
        } else {
            $redirect_to = redirect(route('admin.bookings.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Booking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        //Client

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }
        $count = 0;
        foreach ($booking->schedule->detail as $row) {
            $count += $row->amount;
            $installment = $row->where('mode_id', Schedule::INSTALLMENT)->pluck('amount');
            $duration = $row->where('mode_id', Schedule::INSTALLMENT)->pluck('time_duration');
        }
        $payment = 0;
//        foreach ($booking->payment-> as $row) {
//            $payment += $row->
//        }
        $total = $count;
        $paid = 0;
        $installment = $installment[0] / $duration[0];

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $booking);
        return view('admin.bookings.show')->with([
            'booking'     => $booking,
            'total'       => $total,
            'paid'        => $paid,
            'installment' => $installment,
            'title'       => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Booking.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $booking);
        return view('admin.bookings.edit')->with(['booking' => $booking, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Booking in storage.
     *
     * @param  int $id
     * @param UpdateBookingRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBookingRequest $request)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        $booking = $this->bookingRepository->updateRecord($request, $booking);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.bookings.create'));
        } else {
            $redirect_to = redirect(route('admin.bookings.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Booking from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $booking = $this->bookingRepository->findWithoutFail($id);

        if (empty($booking)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.bookings.index'));
        }

        $this->bookingRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.bookings.index'))->with(['title' => $this->BreadCrumbName]);
    }

    public function fetcharea(Request $request)
    {
        $id = $request->get('data')[0];
        $area = $this->scheduleRepository->findWhere(['project_id' => $id])->pluck('area_id');
        $a = $this->areaRepository->all()->whereIn('id', $area);
        return $this->sendResponse($a, true);
    }

    public function fetchblock(Request $request)
    {
        $id = $request->get('data')[0];
        $project_id = $request->get('data')[1];
        $block = $this->blockRepository->all()->where('area_id', $id)->where('project_id', $project_id);
        return $this->sendResponse($block, true);
    }

    public function fetchProperties(Request $request)
    {
        $id = $request->get('data')[0];
        $properties = $this->propertyRepository->all()->where('block_id', $id);
        return $this->sendResponse($properties, true);
    }
}
