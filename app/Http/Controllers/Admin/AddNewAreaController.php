<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\AddNewAreaDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateAddNewAreaRequest;
use App\Http\Requests\Admin\UpdateAddNewAreaRequest;
use App\Repositories\Admin\AddNewAreaRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class AddNewAreaController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  AddNewAreaRepository */
    private $addNewAreaRepository;

    public function __construct(AddNewAreaRepository $addNewAreaRepo)
    {
        $this->addNewAreaRepository = $addNewAreaRepo;
        $this->ModelName = 'add-new-areas';
        $this->BreadCrumbName = 'Add New Areas';
    }

    /**
     * Display a listing of the AddNewArea.
     *
     * @param AddNewAreaDataTable $addNewAreaDataTable
     * @return Response
     */
    public function index(AddNewAreaDataTable $addNewAreaDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $addNewAreaDataTable->render('admin.add_new_areas.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new AddNewArea.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.add_new_areas.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created AddNewArea in storage.
     *
     * @param CreateAddNewAreaRequest $request
     *
     * @return Response
     */
    public function store(CreateAddNewAreaRequest $request)
    {
        $addNewArea = $this->addNewAreaRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.add-new-areas.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.add-new-areas.edit', $addNewArea->id));
        } else {
            $redirect_to = redirect(route('admin.add-new-areas.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified AddNewArea.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.add-new-areas.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $addNewArea);
        return view('admin.add_new_areas.show')->with(['addNewArea' => $addNewArea, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified AddNewArea.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.add-new-areas.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $addNewArea);
        return view('admin.add_new_areas.edit')->with(['addNewArea' => $addNewArea, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified AddNewArea in storage.
     *
     * @param  int              $id
     * @param UpdateAddNewAreaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAddNewAreaRequest $request)
    {
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.add-new-areas.index'));
        }

        $addNewArea = $this->addNewAreaRepository->updateRecord($request, $addNewArea);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.add-new-areas.create'));
        } else {
            $redirect_to = redirect(route('admin.add-new-areas.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified AddNewArea from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $addNewArea = $this->addNewAreaRepository->findWithoutFail($id);

        if (empty($addNewArea)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.add-new-areas.index'));
        }

        $this->addNewAreaRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.add-new-areas.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
