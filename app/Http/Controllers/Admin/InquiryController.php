<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\InquiryDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateInquiryRequest;
use App\Http\Requests\Admin\UpdateInquiryRequest;
use App\Repositories\Admin\InquiryRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class InquiryController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  InquiryRepository */
    private $inquiryRepository;

    public function __construct(InquiryRepository $inquiryRepo)
    {
        $this->inquiryRepository = $inquiryRepo;
        $this->ModelName = 'inquiries';
        $this->BreadCrumbName = 'Inquiries';
    }

    /**
     * Display a listing of the Inquiry.
     *
     * @param InquiryDataTable $inquiryDataTable
     * @return Response
     */
    public function index(InquiryDataTable $inquiryDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $inquiryDataTable->render('admin.inquiries.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Inquiry.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.inquiries.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Inquiry in storage.
     *
     * @param CreateInquiryRequest $request
     *
     * @return Response
     */
    public function store(CreateInquiryRequest $request)
    {
        $inquiry = $this->inquiryRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.inquiries.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.inquiries.edit', $inquiry->id));
        } else {
            $redirect_to = redirect(route('admin.inquiries.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Inquiry.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.inquiries.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $inquiry);
        return view('admin.inquiries.show')->with(['inquiry' => $inquiry, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Inquiry.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.inquiries.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $inquiry);
        return view('admin.inquiries.edit')->with(['inquiry' => $inquiry, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Inquiry in storage.
     *
     * @param  int              $id
     * @param UpdateInquiryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateInquiryRequest $request)
    {
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.inquiries.index'));
        }

        $inquiry = $this->inquiryRepository->updateRecord($request, $inquiry);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.inquiries.create'));
        } else {
            $redirect_to = redirect(route('admin.inquiries.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Inquiry from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $inquiry = $this->inquiryRepository->findWithoutFail($id);

        if (empty($inquiry)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.inquiries.index'));
        }

        $this->inquiryRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.inquiries.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
