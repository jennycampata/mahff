<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\EmployeeTypeDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateEmployeeTypeRequest;
use App\Http\Requests\Admin\UpdateEmployeeTypeRequest;
use App\Repositories\Admin\EmployeeTypeRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class EmployeeTypeController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  EmployeeTypeRepository */
    private $employeeTypeRepository;

    public function __construct(EmployeeTypeRepository $employeeTypeRepo)
    {
        $this->employeeTypeRepository = $employeeTypeRepo;
        $this->ModelName = 'employee-types';
        $this->BreadCrumbName = 'Employee Types';
    }

    /**
     * Display a listing of the EmployeeType.
     *
     * @param EmployeeTypeDataTable $employeeTypeDataTable
     * @return Response
     */
    public function index(EmployeeTypeDataTable $employeeTypeDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $employeeTypeDataTable->render('admin.employee_types.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new EmployeeType.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.employee_types.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created EmployeeType in storage.
     *
     * @param CreateEmployeeTypeRequest $request
     *
     * @return Response
     */
    public function store(CreateEmployeeTypeRequest $request)
    {
        $employeeType = $this->employeeTypeRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.employee-types.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.employee-types.edit', $employeeType->id));
        } else {
            $redirect_to = redirect(route('admin.employee-types.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified EmployeeType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employee-types.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $employeeType);
        return view('admin.employee_types.show')->with(['employeeType' => $employeeType, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified EmployeeType.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employee-types.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $employeeType);
        return view('admin.employee_types.edit')->with(['employeeType' => $employeeType, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified EmployeeType in storage.
     *
     * @param  int              $id
     * @param UpdateEmployeeTypeRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmployeeTypeRequest $request)
    {
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employee-types.index'));
        }

        $employeeType = $this->employeeTypeRepository->updateRecord($request, $employeeType);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.employee-types.create'));
        } else {
            $redirect_to = redirect(route('admin.employee-types.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified EmployeeType from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $employeeType = $this->employeeTypeRepository->findWithoutFail($id);

        if (empty($employeeType)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.employee-types.index'));
        }

        $this->employeeTypeRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.employee-types.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
