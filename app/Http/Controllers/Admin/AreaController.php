<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\AreaDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateAreaRequest;
use App\Http\Requests\Admin\UpdateAreaRequest;
use App\Repositories\Admin\AreaRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class AreaController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  AreaRepository */
    private $areaRepository;

    public function __construct(AreaRepository $areaRepo)
    {
        $this->areaRepository = $areaRepo;
        $this->ModelName = 'areas';
        $this->BreadCrumbName = 'Areas';
    }

    /**
     * Display a listing of the Area.
     *
     * @param AreaDataTable $areaDataTable
     * @return Response
     */
    public function index(AreaDataTable $areaDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $areaDataTable->render('admin.areas.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Area.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.areas.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Area in storage.
     *
     * @param CreateAreaRequest $request
     *
     * @return Response
     */
    public function store(CreateAreaRequest $request)
    {
        $area = $this->areaRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.areas.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.areas.edit', $area->id));
        } else {
            $redirect_to = redirect(route('admin.areas.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Area.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $area = $this->areaRepository->findWithoutFail($id);

        if (empty($area)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.areas.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $area);
        return view('admin.areas.show')->with(['area' => $area, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Area.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $area = $this->areaRepository->findWithoutFail($id);

        if (empty($area)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.areas.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $area);
        return view('admin.areas.edit')->with(['area' => $area, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Area in storage.
     *
     * @param  int              $id
     * @param UpdateAreaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAreaRequest $request)
    {
        $area = $this->areaRepository->findWithoutFail($id);

        if (empty($area)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.areas.index'));
        }

        $area = $this->areaRepository->updateRecord($request, $area);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.areas.create'));
        } else {
            $redirect_to = redirect(route('admin.areas.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Area from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $area = $this->areaRepository->findWithoutFail($id);

        if (empty($area)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.areas.index'));
        }

        $this->areaRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.areas.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
