<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\BlockDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateBlockRequest;
use App\Http\Requests\Admin\UpdateBlockRequest;
use App\Repositories\Admin\BlockRepository;
use App\Repositories\Admin\AreaRepository;
use App\Http\Controllers\AppBaseController;
use http\Env\Request;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class BlockController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  BlockRepository */
    private $blockRepository;
    private  $areaRepository;

    public function __construct(BlockRepository $blockRepo, AreaRepository $areaRepo)
    {
        $this->blockRepository = $blockRepo;
        $this->areaRepository = $areaRepo;
        $this->ModelName = 'blocks';
        $this->BreadCrumbName = 'Blocks';
    }

    /**
     * Display a listing of the Block.
     *
     * @param BlockDataTable $blockDataTable
     * @return Response
     */
    public function index(BlockDataTable $blockDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $blockDataTable->render('admin.blocks.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Block.
     *
     * @return Response
     */
    public function create($id)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        $area = $this->areaRepository->pluck('name','id');


        return view('admin.blocks.create')->with([
            'title' => $this->BreadCrumbName,
            'area_id'=> $area,
        'project_id'=>$id

        ]);
    }

    /**
     * Store a newly created Block in storage.
     *
     * @param CreateBlockRequest $request
     *
     * @return Response
     */
    public function store(CreateBlockRequest $request)
    {
        $block = $this->blockRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.blocks.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.blocks.edit', $block->id));
        } else {
            $redirect_to = redirect(route('admin.blocks.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Block.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.blocks.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $block);
        return view('admin.blocks.show')->with(['block' => $block, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Block.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.blocks.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $block);
        return view('admin.blocks.edit')->with(['block' => $block, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Block in storage.
     *
     * @param  int              $id
     * @param UpdateBlockRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateBlockRequest $request)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.blocks.index'));
        }

        $block = $this->blockRepository->updateRecord($request, $block);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.blocks.create'));
        } else {
            $redirect_to = redirect(route('admin.blocks.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Block from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $block = $this->blockRepository->findWithoutFail($id);

        if (empty($block)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.blocks.index'));
        }

        $this->blockRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.blocks.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
