<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\CustomScheduleDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateCustomScheduleRequest;
use App\Http\Requests\Admin\UpdateCustomScheduleRequest;
use App\Repositories\Admin\CustomScheduleRepository;
use App\Http\Controllers\AppBaseController;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class CustomScheduleController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  CustomScheduleRepository */
    private $customScheduleRepository;

    public function __construct(CustomScheduleRepository $customScheduleRepo)
    {
        $this->customScheduleRepository = $customScheduleRepo;
        $this->ModelName = 'custom-schedules';
        $this->BreadCrumbName = 'Custom Schedules';
    }

    /**
     * Display a listing of the CustomSchedule.
     *
     * @param CustomScheduleDataTable $customScheduleDataTable
     * @return Response
     */
    public function index(CustomScheduleDataTable $customScheduleDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return $customScheduleDataTable->render('admin.custom_schedules.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new CustomSchedule.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName);
        return view('admin.custom_schedules.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created CustomSchedule in storage.
     *
     * @param CreateCustomScheduleRequest $request
     *
     * @return Response
     */
    public function store(CreateCustomScheduleRequest $request)
    {
        $customSchedule = $this->customScheduleRepository->saveRecord($request);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.custom-schedules.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.custom-schedules.edit', $customSchedule->id));
        } else {
            $redirect_to = redirect(route('admin.custom-schedules.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified CustomSchedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.custom-schedules.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $customSchedule);
        return view('admin.custom_schedules.show')->with(['customSchedule' => $customSchedule, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified CustomSchedule.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.custom-schedules.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName,$this->BreadCrumbName, $customSchedule);
        return view('admin.custom_schedules.edit')->with(['customSchedule' => $customSchedule, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified CustomSchedule in storage.
     *
     * @param  int              $id
     * @param UpdateCustomScheduleRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCustomScheduleRequest $request)
    {
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.custom-schedules.index'));
        }

        $customSchedule = $this->customScheduleRepository->updateRecord($request, $customSchedule);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.custom-schedules.create'));
        } else {
            $redirect_to = redirect(route('admin.custom-schedules.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified CustomSchedule from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customSchedule = $this->customScheduleRepository->findWithoutFail($id);

        if (empty($customSchedule)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.custom-schedules.index'));
        }

        $this->customScheduleRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.custom-schedules.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
