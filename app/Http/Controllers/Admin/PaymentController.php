<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\PaymentDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreatePaymentRequest;
use App\Http\Requests\Admin\UpdatePaymentRequest;
use App\Models\ScheduleDetails;
use App\Repositories\Admin\BookingRepository;
use App\Repositories\Admin\ModeRepository;
use App\Repositories\Admin\PaymentRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\Admin\ScheduleRepository;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class PaymentController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  PaymentRepository */
    private $paymentRepository;
    private $bookingRepository;
    private $modeRepository;

    public function __construct(PaymentRepository $paymentRepo, BookingRepository $bookingRepo, ModeRepository $modeRepository)
    {
        $this->paymentRepository = $paymentRepo;
        $this->bookingRepository = $bookingRepo;
        $this->modeRepository = $modeRepository;
        $this->ModelName = 'payments';
        $this->BreadCrumbName = 'Payments';
    }

    /**
     * Display a listing of the Payment.
     *
     * @param PaymentDataTable $paymentDataTable
     * @return Response
     */
    public function index(PaymentDataTable $paymentDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $paymentDataTable->render('admin.payments.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Payment.
     *
     * @return Response
     */
    public function create($id)
    {
        $property = $this->bookingRepository->find($id);
        $schedule_mode = ScheduleDetails::where('schedule_id', $property->schedule_id)->pluck('mode_id');
        $mode = $this->modeRepository->findWhereIn('id', [$schedule_mode])->pluck('name');
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.payments.create')->with
        ([
            'title'       => $this->BreadCrumbName,
            'booking_id'  => $id,
            'property_id' => $property->property_id,
            'mode'        => $mode,
        ]);
    }

    /**
     * Store a newly created Payment in storage.
     *
     * @param CreatePaymentRequest $request
     *
     * @return Response
     */
    public function store(CreatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->saveRecord($request);
        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.payments.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.payments.edit', $payment->id));
        } else {
            $redirect_to = redirect(route('admin.payments.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.payments.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $payment);
        return view('admin.payments.show')->with(['payment' => $payment, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Payment.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.payments.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $payment);
        return view('admin.payments.edit')->with(['payment' => $payment, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Payment in storage.
     *
     * @param  int $id
     * @param UpdatePaymentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePaymentRequest $request)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.payments.index'));
        }

        $payment = $this->paymentRepository->updateRecord($request, $payment);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.payments.create'));
        } else {
            $redirect_to = redirect(route('admin.payments.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Payment from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $payment = $this->paymentRepository->findWithoutFail($id);

        if (empty($payment)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.payments.index'));
        }

        $this->paymentRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.payments.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
