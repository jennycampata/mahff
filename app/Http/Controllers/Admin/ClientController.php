<?php

namespace App\Http\Controllers\Admin;

use App\Helper\BreadcrumbsRegister;
use App\DataTables\Admin\ClientDataTable;
use App\Http\Requests\Admin;
use App\Http\Requests\Admin\CreateClientRequest;
use App\Http\Requests\Admin\UpdateClientRequest;
use App\Models\Client;
use App\Repositories\Admin\ClientRepository;
use App\Http\Controllers\AppBaseController;
use App\Repositories\Admin\UserRepository;
use App\Models\ClientDetails;
use Laracasts\Flash\Flash;
use Illuminate\Http\Response;

class ClientController extends AppBaseController
{
    /** ModelName */
    private $ModelName;

    /** BreadCrumbName */
    private $BreadCrumbName;

    /** @var  ClientRepository */
    private $clientRepository;
    private $userRepository;

    public function __construct(ClientRepository $clientRepo, UserRepository $userRepo)
    {
        $this->clientRepository = $clientRepo;
        $this->userRepository = $userRepo;
        $this->ModelName = 'clients';
        $this->BreadCrumbName = 'Clients';
    }

    /**
     * Display a listing of the Client.
     *
     * @param ClientDataTable $clientDataTable
     * @return Response
     */
    public function index(ClientDataTable $clientDataTable)
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return $clientDataTable->render('admin.clients.index', ['title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for creating a new Client.
     *
     * @return Response
     */
    public function create()
    {
        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName);
        return view('admin.clients.create')->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Store a newly created Client in storage.
     *
     * @param CreateClientRequest $request
     *
     * @return Response
     */
    public function store(CreateClientRequest $request)
    {
        $client = $this->clientRepository->saveRecord($request);

        $details = [];
        $details['email'] = $request->designation_id;
        $details['user_id'] = $client->id;
        $details['contact_no'] = $request->employee_type_id;
        $details['address'] = $request->contact_no;
        $details['postal_address'] = $request->address;
        $details['contact_no2'] = $request->cnic_no;
        $details['cnic_no'] = $request->gender;
        $details['father_name_husband_name'] = $request->gender;
        $details['nominee_name'] = $request->marriage_status;
        $details['nominee_relation'] = $request->salary;
        $details['nominee_address'] = $request->cnic_no;
        $details['nominee_email'] = $request->gender;
        $details['nominee_contactno'] = $request->marriage_status;
        $details['nominee_contactno2'] = $request->salary;

        $clint = ClientDetails::create($details);
        $this->userRepository->attachRole($client->id, Client::Client);

        Flash::success($this->BreadCrumbName . ' saved successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.clients.create'));
        } elseif (isset($request->translation)) {
            $redirect_to = redirect(route('admin.clients.edit', $client->id));
        } else {
            $redirect_to = redirect(route('admin.clients.index'));
        }
        return $redirect_to->with([
            'title' => $this->BreadCrumbName
        ]);
    }

    /**
     * Display the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.clients.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $client);
        return view('admin.clients.show')->with(['client' => $client, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Show the form for editing the specified Client.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.clients.index'));
        }

        BreadcrumbsRegister::Register($this->ModelName, $this->BreadCrumbName, $client);
        return view('admin.clients.edit')->with(['client' => $client, 'title' => $this->BreadCrumbName]);
    }

    /**
     * Update the specified Client in storage.
     *
     * @param  int $id
     * @param UpdateClientRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateClientRequest $request)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.clients.index'));
        }

        $client = $this->clientRepository->updateRecord($request, $client);

        $details = [];
        $details['email'] = $request->designation_id;
        $details['user_id'] = $client->id;
        $details['contact_no'] = $request->employee_type_id;
        $details['address'] = $request->contact_no;
        $details['postal_address'] = $request->address;
        $details['contact_no2'] = $request->cnic_no;
        $details['cnic_no'] = $request->gender;
        $details['father_name_husband_name'] = $request->gender;
        $details['nominee_name'] = $request->marriage_status;
        $details['nominee_relation'] = $request->salary;
        $details['nominee_address'] = $request->cnic_no;
        $details['nominee_email'] = $request->gender;
        $details['nominee_contactno'] = $request->marriage_status;
        $details['nominee_contactno2'] = $request->salary;

        ClientDetails::where('user_id', $id)->update($details);

        Flash::success($this->BreadCrumbName . ' updated successfully.');
        if (isset($request->continue)) {
            $redirect_to = redirect(route('admin.clients.create'));
        } else {
            $redirect_to = redirect(route('admin.clients.index'));
        }
        return $redirect_to->with(['title' => $this->BreadCrumbName]);
    }

    /**
     * Remove the specified Client from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $client = $this->clientRepository->findWithoutFail($id);

        if (empty($client)) {
            Flash::error($this->BreadCrumbName . ' not found');
            return redirect(route('admin.clients.index'));
        }

        $this->clientRepository->deleteRecord($id);

        Flash::success($this->BreadCrumbName . ' deleted successfully.');
        return redirect(route('admin.clients.index'))->with(['title' => $this->BreadCrumbName]);
    }
}
