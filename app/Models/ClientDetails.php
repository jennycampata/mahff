<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Employee",
 *      required={"id", "name", "email", "password"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class ClientDetails extends Model
{
    use SoftDeletes;

    public $table = 'client_details';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'user_id',
        'email',
        'contact_no',
        'address',
        'postal_address',
        'contact_no2',
        'cnic_no',
        'father_name_husband_name',
        'nominee_name',
        'nominee_relation',
        'nominee_address',
        'nominee_email',
        'nominee_contactno',
        'nominee_contactno2',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'                       => 'integer',
        'user_id'                  => 'string',
        'email'                    => 'string',
        'contact_no'               => 'string',
        'address'                  => 'string',
        'postal_address'           => 'string',
        'contact_no2'              => 'integer',
        'cnic_no'                  => 'integer',
        'father_name_husband_name' => 'string',
        'nominee_name'             => 'string',
        'nominee_relation'         => 'string',
        'nominee_address'          => 'string',
        'nominee_email'            => 'string',
        'nominee_contactno'        => 'string',
        'nominee_contactno2'       => 'string',

    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'user_id'                  => 'required',
        'email'                    => 'required',
        'contact_no'               => 'required',
        'address'                  => 'required',
        'postal_address'           => 'required',
        'contact_no2'              => 'required',
        'cnic_no'                  => 'required',
        'father_name_husband_name' => 'required',
        'nominee_name'             => 'required',
        'nominee_relation'         => 'required',
        'nominee_address'          => 'required',
        'nominee_email'            => 'required',
        'nominee_contactno'        => 'required',
        'nominee_contactno2'       => 'required',

    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'user_id'                  => 'required',
        'email'                    => 'required',
        'contact_no'               => 'required',
        'address'                  => 'required',
        'postal_address'           => 'required',
        'contact_no2'              => 'required',
        'cnic_no'                  => 'required',
        'father_name_husband_name' => 'required',
        'nominee_name'             => 'required',
        'nominee_relation'         => 'required',
        'nominee_address'          => 'required',
        'nominee_email'            => 'required',
        'nominee_contactno'        => 'required',
        'nominee_contactno2'       => 'required',

    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'user_id'                  => 'required',
        'email'                    => 'required',
        'contact_no'               => 'required',
        'address'                  => 'required',
        'postal_address'           => 'required',
        'contact_no2'              => 'required',
        'cnic_no'                  => 'required',
        'father_name_husband_name' => 'required',
        'nominee_name'             => 'required',
        'nominee_relation'         => 'required',
        'nominee_address'          => 'required',
        'nominee_email'            => 'required',
        'nominee_contactno'        => 'required',
        'nominee_contactno2'       => 'required',

    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'user_id'                  => 'required',
        'email'                    => 'required',
        'contact_no'               => 'required',
        'address'                  => 'required',
        'postal_address'           => 'required',
        'contact_no2'              => 'required',
        'father_name_husband_name' => 'required',
        'cnic_no'                  => 'required',
        'nominee_name'             => 'required',
        'nominee_relation'         => 'required',
        'nominee_address'          => 'required',
        'nominee_email'            => 'required',
        'nominee_contactno'        => 'required',
        'nominee_contactno2'       => 'required',

    ];


}
