<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Schedule",
 *      required={"id", "area_id", "project_id", "mode_id", "time_duration", "amount"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="area_id",
 *          description="area_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="project_id",
 *          description="project_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="mode_id",
 *          description="mode_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="time_duration",
 *          description="time_duration",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="amount",
 *          description="amount",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Schedule extends Model

{
    use SoftDeletes;

    public $table = 'schedules';


    protected $dates = ['deleted_at'];

    const INACTIVE = 1;
    const ACTIVE = 0;
    const CUSTOMSCH = 2;
    const TRASH = 3;
    const INSTALLMENT = 3;
    public $fillable = [

        'area_id',
        'status',
        'project_id',
        'name',
//        'custom_schedule',
        'mode_id',
        'time_duration',
        'amount'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'            => 'integer',
        'area_id'       => 'integer',
        'project_id'    => 'integer',
        'name'          => 'string',
//        'custom_schedule'=> 'integer',
        'mode_id'       => 'integer',
        'time_duration' => 'integer',
        'amount'        => 'integer'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'area_id'       => 'required',
        'project_id'    => 'required',
        'name'          => 'required',
//        'custom_schedule'=> 'integer',
        'mode_id'       => 'required',
        'time_duration' => 'required',
        'amount'        => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'area_id'       => 'required',
        'project_id'    => 'required',
        'name'          => 'required',
//        'custom_schedule'=> 'integer',
        'mode_id'       => 'required',
        'time_duration' => 'required',
        'amount'        => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'area_id'       => 'required',
        'project_id'    => 'required',
        'name'          => 'required',
//        'custom_schedule'=> 'integer',
        'mode_id'       => 'required',
        'time_duration' => 'required',
        'amount'        => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'area_id'       => 'required',
        'project_id'    => 'required',
        'name'          => 'required',
//        'custom_schedule'=> 'integer',
        'mode_id'       => 'required',
        'time_duration' => 'required',
        'amount'        => 'required'
    ];


    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    public function detail()
    {
        return $this->hasMany(ScheduleDetails::class, 'schedule_id');
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

}
