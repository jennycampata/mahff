<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Employee",
 *      required={"id", "name", "email", "password"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="email",
 *          description="email",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="password",
 *          description="password",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="remember_token",
 *          description="remember_token",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class EmployeeDetails extends Model
{
    use SoftDeletes;

    public $table = 'employee_details';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'project_id',
        'designation_id',
        'employee_type_id',
        'user_id',
        'contact_no',
        'address',
        'cnic_no',
        'gender',
        'marriage_status',
        'salary',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'project_id' => 'integer',
        'designation_id' => 'integer',
        'employee_type_id' => 'integer',
        'user_id' => 'integer',
        'contact_no'=> 'string',
        'address'=> 'string',
        'cnic_no' => 'integer',
        'gender' => 'integer',
        'marriage_status' => 'integer',
        'salary' => 'integer',
        
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
     protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'project_id' => 'required',
        'designation_id' => 'required',
        'employee_type_id' => 'required',
        'user_id' => 'required',
        'contact_no'=> 'required',
        'address'=> 'required',
        'cnic_no' => 'required',
        'gender' => 'required',
        'marriage_status' => 'required',
        'salary' => 'required',
        
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'project_id' => 'required',
        'designation_id' => 'required',
        'employee_type_id' => 'required',
        'user_id' => 'required',
        'contact_no'=> 'required',
        'address'=> 'required',
        'cnic_no' => 'required',
        'gender' => 'required',
        'marriage_status' => 'required',
        'salary' => 'required',
        
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'project_id' => 'required',
        'designation_id' => 'required',
        'employee_type_id' => 'required',
        'user_id' => 'required',
        'contact_no'=> 'required',
        'address'=> 'required',
        'cnic_no' => 'required',
        'gender' => 'required',
        'marriage_status' => 'required',
        'salary' => 'required',
        
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'project_id' => 'required',
        'designation_id' => 'required',
        'employee_type_id' => 'required',
        'user_id' => 'required',
        'contact_no'=> 'required',
        'address'=> 'required',
        'cnic_no' => 'required',
        'gender' => 'required',
        'marriage_status' => 'required',
        'salary' => 'required',
        
    ];

    
}
