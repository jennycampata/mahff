<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Project",
 *      required={"id", "name", "type_id", "detail", "address", "starting_date", "ending_date", "count"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="name",
 *          description="name",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="type_id",
 *          description="type_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="detail",
 *          description="detail",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="address",
 *          description="address",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="starting_date",
 *          description="starting_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="ending_date",
 *          description="ending_date",
 *          type="string",
 *          format="date"
 *      ),
 *      @SWG\Property(
 *          property="count",
 *          description="count",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Project extends Model
{
    use SoftDeletes;

    public $table = 'projects';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'type_id',
        'detail',
        'address',
        'starting_date',
        'ending_date',
        'count'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'type_id' => 'integer',
        'detail' => 'string',
        'address' => 'string',
        'starting_date' => 'string',
        'ending_date' => 'string',
        'count' => 'integer'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
     protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'type_id' => 'required',
        'detail' => 'required',
        'address' => 'required',
        'starting_date' => 'required',
        'ending_date' => 'required',
        'count' => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'name' => 'required',
        'type_id' => 'required',
        'detail' => 'required',
        'address' => 'required',
        'starting_date' => 'required',
        'ending_date' => 'required',
        'count' => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'name' => 'required',
        'type_id' => 'required',
        'detail' => 'required',
        'address' => 'required',
        'starting_date' => 'required',
        'ending_date' => 'required',
        'count' => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'name' => 'required',
        'type_id' => 'required',
        'detail' => 'required',
        'address' => 'required',
        'starting_date' => 'required',
        'ending_date' => 'required',
        'count' => 'required'
    ];

    public function employee(){

        return $this->hasMany(Employee::class, 'project_id');
    }

    public function projecttype(){

        return $this->belongsTo(Type::class);
    }

    public function blocks()
    {
        return $this->hasMany(Block::class,'project_id');
    }

    public function properties()
    {
        return $this->hasMany(Property::class,'project_id');
    }

    public function schedule()
    {
        return $this->hasMany(Property::class,'project_id');
    }


    public function booking()
    {
        return $this->hasMany(Booking::class,'project_id');
    }


}
