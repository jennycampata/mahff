<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Payment",
 *      required={"id", "type", "property_id", "schedule_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="type",
 *          description="type",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="property_id",
 *          description="property_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="schedule_id",
 *          description="schedule_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Payment extends Model
{
    use SoftDeletes;

    public $table = 'payments';


    protected $dates = ['deleted_at'];

    const BANK = 0;
    const CASH = 1;
    const PETTY_CASH = 2;

    public static $PAYMENT_TYPE = [
        self::BANK       => 'Bank',
        self::CASH       => 'Cash',
        self::PETTY_CASH => 'Petty Cash'
    ];

    public static $CLIENT_PAYMENT_TYPE = [
        self::BANK => 'Bank',
        self::CASH => 'Cash',
    ];
    public $fillable = [
        'id',
        'type',
        'property_id',
        'booking_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'          => 'integer',
        'type'        => 'integer',
        'property_id' => 'integer',
        'booking_id'  => 'integer'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
//        'id'          => 'required',
        'type'        => 'required',
        'property_id' => 'required',
        'booking_id'  => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
//        'id'          => 'required',
        'type'        => 'required',
        'property_id' => 'required',
        'booking_id'  => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'id'          => 'required',
        'type'        => 'required',
        'property_id' => 'required',
        'booking_id'  => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'id'          => 'required',
        'type'        => 'required',
        'property_id' => 'required',
        'booking_id'  => 'required'
    ];

    public function detail()
    {
//        return $this->hasMany(Payment);
    }

}
