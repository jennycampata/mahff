<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Property",
 *      required={"id", "project_id", "block_id", "area_id", "extra_land", "road_facing", "park_facing", "west_open", "corner"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="project_id",
 *          description="project_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="block_id",
 *          description="block_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="area_id",
 *          description="area_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="extra_land",
 *          description="extra_land",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="road_facing",
 *          description="road_facing",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="park_facing",
 *          description="park_facing",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="west_open",
 *          description="west_open",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="corner",
 *          description="corner",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Property extends Model
{
    use SoftDeletes;

    public $table = 'properties';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'project_id',
        'block_id',
        'area_id',
        'name',
        'extra_land',
        'road_facing',
        'park_facing',
        'west_open',
        'corner'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'project_id' => 'integer',
        'block_id' => 'integer',
        'area_id' => 'integer',
        'name' => 'string',
        'extra_land' => 'integer',
        'road_facing' => 'integer',
        'park_facing' => 'integer',
        'west_open' => 'integer',
        'corner' => 'integer',
        'status' => 'integer'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
     protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'project_id' => 'required',
        'block_id' => 'required',
        'area_id' => 'required',
        'name'=> 'required',
        'extra_land' => 'required',
        'road_facing' => 'required',
        'park_facing' => 'required',
        'west_open' => 'required',
        'corner' => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'project_id' => 'required',
        'block_id' => 'required',
        'area_id' => 'required',
        'name'=>'required',
        'extra_land' => 'required',
        'road_facing' => 'required',
        'park_facing' => 'required',
        'west_open' => 'required',
        'corner' => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'project_id' => 'required',
        'block_id' => 'required',
        'area_id' => 'required',
        'name' => 'required',
        'extra_land' => 'required',
        'road_facing' => 'required',
        'park_facing' => 'required',
        'west_open' => 'required',
        'corner' => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'project_id' => 'required',
        'block_id' => 'required',
        'area_id' => 'required',
        'name' => 'required',
        'extra_land' => 'required',
        'road_facing' => 'required',
        'park_facing' => 'required',
        'west_open' => 'required',
        'corner' => 'required'
    ];


    public  function area (){
        return $this->belongsTo(Area::class);
    }
    public  function block (){
        return $this->belongsTo(Block::class);
    }
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
