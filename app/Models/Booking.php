<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property integer id
 * @property string name
 * @property string created_at
 * @property string updated_at
 * @property string deleted_at
 *
 * @SWG\Definition(
 *      definition="Booking",
 *      required={"project_id", "area_id", "block_id", "schedule_id", "client_id", "booking_date", "employee_id", "file_no"},
 *      @SWG\Property(
 *          property="Id",
 *          description="Id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="project_id",
 *          description="project_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="area_id",
 *          description="area_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="block_id",
 *          description="block_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="schedule_id",
 *          description="schedule_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="client_id",
 *          description="client_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="booking_date",
 *          description="booking_date",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="employee_id",
 *          description="employee_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="file_no",
 *          description="file_no",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="deleted_at",
 *          description="deleted_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Booking extends Model
{
    use SoftDeletes;

    public $table = 'bookings';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'project_id',
        'property_id',
        'area_id',
        'block_id',
        'schedule_id',
        'client_id',
        'booking_date',
        'employee_id',
        'file_no'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'Id'           => 'integer',
        'project_id'   => 'integer',
        'area_id'      => 'integer',
        'block_id'     => 'integer',
        'schedule_id'  => 'integer',
        'client_id'    => 'integer',
        'booking_date' => 'datetime',
        'employee_id'  => 'integer',
        'file_no'      => 'string'
    ];

    /**
     * The objects that should be append to toArray.
     *
     * @var array
     */
    protected $with = [];

    /**
     * The attributes that should be append to toArray.
     *
     * @var array
     */
    protected $appends = [];

    /**
     * The attributes that should be visible in toArray.
     *
     * @var array
     */
    protected $visible = [];

    /**
     * Validation create rules
     *
     * @var array
     */
    public static $rules = [
        'project_id'   => 'required',
        'area_id'      => 'required',
        'block_id'     => 'required',
        'schedule_id'  => 'required',
        'client_id'    => 'required',
        'booking_date' => 'required',
        'employee_id'  => 'required',
        'file_no'      => 'required'
    ];

    /**
     * Validation update rules
     *
     * @var array
     */
    public static $update_rules = [
        'project_id'   => 'required',
        'area_id'      => 'required',
        'block_id'     => 'required',
        'schedule_id'  => 'required',
        'client_id'    => 'required',
        'booking_date' => 'required',
        'employee_id'  => 'required',
        'file_no'      => 'required'
    ];

    /**
     * Validation api rules
     *
     * @var array
     */
    public static $api_rules = [
        'project_id'   => 'required',
        'area_id'      => 'required',
        'block_id'     => 'required',
        'schedule_id'  => 'required',
        'client_id'    => 'required',
        'booking_date' => 'required',
        'employee_id'  => 'required',
        'file_no'      => 'required'
    ];

    /**
     * Validation api update rules
     *
     * @var array
     */
    public static $api_update_rules = [
        'project_id'   => 'required',
        'area_id'      => 'required',
        'block_id'     => 'required',
        'schedule_id'  => 'required',
        'client_id'    => 'required',
        'booking_date' => 'required',
        'employee_id'  => 'required',
        'file_no'      => 'required'
    ];


    public function payment()
    {
        return $this->hasMany(Payment::class, 'booking_id');
    }

    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'schedule_id');
    }
//
//    public function project(){
//
//        return $this->belongsTo(Project::class);
//    }
//
//    public function block(){
//
//        return $this->belongsTo(Block::class);
//    }

}
