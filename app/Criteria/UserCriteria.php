<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class UserCriteria.
 *
 * @package namespace App\Criteria;
 */
class UserCriteria extends BaseCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    protected $role = null;
    protected $query = null;


    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->exists('role')) {
            $role = $this->role;
            $model = $model->whereHas('roles', function ($q) use ($role) {
                return $q->where('id', $role);
            });
        }
        if ($this->exists('query')) {
            $model = $model->where('name', 'like', $this->getLikeQuery($this->query));
        }
        return $model;

    }
}
