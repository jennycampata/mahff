<?php

namespace App\DataTables\Admin;

use App\Models\Block;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

/**
 * Class BlockDataTable
 * @package App\DataTables\Admin
 */
class BlockDataTable extends DataTable
{
    public $project_id = null;
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'admin.blocks.datatables_actions');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Block $model
     * @return \Illuminate\Database\Eloquent\Builder
     */





    public function query(Block $model)
    {
        $query= $model->newQuery();
        if(!is_null($this->project_id)){
            $query->where('project_id',$this->project_id);
        }
        return $query;
//       return $model->newQuery()->orderBy('updated_at', SORT_DESC);
    }
    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        $buttons = [];
        if (\Entrust::can('blocks.create') || \Entrust::hasRole('super-admin')) {
            $buttons = ['create'];
        }
        $buttons = array_merge($buttons, [
            'export',
            'print',
            'reset',
            'reload',
        ]);
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '80px', 'printable' => false])
            ->parameters([
                'dom'     => 'Bfrtip',
                'order'   => [[0, 'desc']],
                'buttons' => $buttons,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'project_id',
            'area_id',
            'name'
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'blocksdatatable_' . time();
    }
}