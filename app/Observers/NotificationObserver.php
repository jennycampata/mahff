<?php

namespace App\Observers;

use App\Jobs\SendPushNotification;
use App\Models\NotificationUser;

/**
 * Class NotificationObserver
 * @package App\Observers
 */
class NotificationObserver
{
    /**
     * @param NotificationUser $notificationUser
     */
    public function created(NotificationUser $notificationUser)
    {
        $message = $notificationUser->notification->message;
        $deviceData = $notificationUser->user->devices()->where('push_notification', 1)->get()->toArray();
        if (!empty($deviceData)) {

            $extraPayload = [
                'ref_id' => $notificationUser->notification->ref_id,
                'type'   => $notificationUser->notification->action_type
            ];

            $job = new SendPushNotification($message, $deviceData, $extraPayload);
            dispatch($job);
        }
    }
}
