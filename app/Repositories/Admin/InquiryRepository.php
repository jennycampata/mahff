<?php

namespace App\Repositories\Admin;

use App\Models\Inquiry;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class InquiryRepository
 * @package App\Repositories\Admin
 * @version September 4, 2019, 9:00 pm UTC
 *
 * @method Inquiry findWithoutFail($id, $columns = ['*'])
 * @method Inquiry find($id, $columns = ['*'])
 * @method Inquiry first($columns = ['*'])
*/
class InquiryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'contact_no',
        'email'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Inquiry::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $inquiry = $this->create($input);
        return $inquiry;
    }

    /**
     * @param $request
     * @param $inquiry
     * @return mixed
     */
    public function updateRecord($request, $inquiry)
    {
        $input = $request->all();
        $inquiry = $this->update($input, $inquiry->id);
        return $inquiry;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $inquiry = $this->delete($id);
        return $inquiry;
    }
}
