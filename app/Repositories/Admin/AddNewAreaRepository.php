<?php

namespace App\Repositories\Admin;

use App\Models\AddNewArea;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AddNewAreaRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 10:48 pm UTC
 *
 * @method AddNewArea findWithoutFail($id, $columns = ['*'])
 * @method AddNewArea find($id, $columns = ['*'])
 * @method AddNewArea first($columns = ['*'])
*/
class AddNewAreaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AddNewArea::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $addNewArea = $this->create($input);
        return $addNewArea;
    }

    /**
     * @param $request
     * @param $addNewArea
     * @return mixed
     */
    public function updateRecord($request, $addNewArea)
    {
        $input = $request->all();
        $addNewArea = $this->update($input, $addNewArea->id);
        return $addNewArea;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $addNewArea = $this->delete($id);
        return $addNewArea;
    }
}
