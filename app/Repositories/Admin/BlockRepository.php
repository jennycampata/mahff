<?php

namespace App\Repositories\Admin;

use App\Models\Block;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BlockRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 10:52 pm UTC
 *
 * @method Block findWithoutFail($id, $columns = ['*'])
 * @method Block find($id, $columns = ['*'])
 * @method Block first($columns = ['*'])
*/
class BlockRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'project_id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Block::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $block = $this->create($input);
        return $block;
    }

    /**
     * @param $request
     * @param $block
     * @return mixed
     */
    public function updateRecord($request, $block)
    {
        $input = $request->all();
        $block = $this->update($input, $block->id);
        return $block;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $block = $this->delete($id);
        return $block;
    }
}
