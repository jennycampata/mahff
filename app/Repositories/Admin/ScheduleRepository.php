<?php

namespace App\Repositories\Admin;

use App\Models\Schedule;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ScheduleRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 11:01 pm UTC
 *
 * @method Schedule findWithoutFail($id, $columns = ['*'])
 * @method Schedule find($id, $columns = ['*'])
 * @method Schedule first($columns = ['*'])
*/
class ScheduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'area_id',
        'project_id',
        'name',
//        'custom_schedule',
        'mode_id',
        'time_duration',
        'amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Schedule::class;
    }

//    public function multi($request){
//        $input = $request->$amount*$time_duration();
//        $schedule = $this->create($input);
//        return $schedule;
//    }
    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->only(['area_id','project_id','name','status']);
        $schedule = $this->create($input);
        return $schedule;
    }

    /**
     * @param $request
     * @param $schedule
     * @return mixed
     */
    public function updateRecord($request, $schedule)
    {

        if($request['status'] == 1){
            $input['status'] = $request['status'];
        }
//        else if($request['status'] == 3){
//            $input['status'] = $request['status'];
//        }
        else{
            $input = $request->all();
        }
        $schedule = $this->update($input, $schedule->id);
        return $schedule;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $schedule = $this->delete($id);
        return $schedule;
    }
}
