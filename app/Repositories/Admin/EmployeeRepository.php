<?php

namespace App\Repositories\Admin;

use App\Models\Employee;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EmployeeRepository
 * @package App\Repositories\Admin
 * @version July 16, 2019, 8:23 pm UTC
 *
 * @method Employee findWithoutFail($id, $columns = ['*'])
 * @method Employee find($id, $columns = ['*'])
 * @method Employee first($columns = ['*'])
*/
class EmployeeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'email',
        'password'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Employee::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->only(['name','email','password']);
        $input['password'] = bcrypt($request->password);
        $employee = $this->create($input);
        return $employee;
    }

    /**
     * @param $request
     * @param $employee
     * @return mixed
     */
    public function updateRecord($request, $employee)
    {
        $input = $request->all();
        $employee = $this->update($input, $employee->id);
        return $employee;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $employee = $this->delete($id);
        return $employee;
    }
}
