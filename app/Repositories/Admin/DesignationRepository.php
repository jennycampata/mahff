<?php

namespace App\Repositories\Admin;

use App\Models\Designation;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class DesignationRepository
 * @package App\Repositories\Admin
 * @version July 16, 2019, 11:51 pm UTC
 *
 * @method Designation findWithoutFail($id, $columns = ['*'])
 * @method Designation find($id, $columns = ['*'])
 * @method Designation first($columns = ['*'])
*/
class DesignationRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Designation::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $designation = $this->create($input);
        return $designation;
    }

    /**
     * @param $request
     * @param $designation
     * @return mixed
     */
    public function updateRecord($request, $designation)
    {
        $input = $request->all();
        $designation = $this->update($input, $designation->id);
        return $designation;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $designation = $this->delete($id);
        return $designation;
    }
}
