<?php

namespace App\Repositories\Admin;

use App\Models\Booking;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BookingRepository
 * @package App\Repositories\Admin
 * @version September 18, 2019, 9:51 pm UTC
 *
 * @method Booking findWithoutFail($id, $columns = ['*'])
 * @method Booking find($id, $columns = ['*'])
 * @method Booking first($columns = ['*'])
*/
class BookingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'project_id',
        'area_id',
        'block_id',
        'schedule_id',
        'client_id',
        'booking_date',
        'employee_id',
        'file_no'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Booking::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $booking = $this->create($input);
        return $booking;
    }

    /**
     * @param $request
     * @param $booking
     * @return mixed
     */
    public function updateRecord($request, $booking)
    {
        $input = $request->all();
        $booking = $this->update($input, $booking->id);
        return $booking;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $booking = $this->delete($id);
        return $booking;
    }
}
