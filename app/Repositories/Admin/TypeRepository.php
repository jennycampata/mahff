<?php

namespace App\Repositories\Admin;

use App\Models\Type;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class TypeRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 11:05 pm UTC
 *
 * @method Type findWithoutFail($id, $columns = ['*'])
 * @method Type find($id, $columns = ['*'])
 * @method Type first($columns = ['*'])
*/
class TypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'created_at'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Type::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $type = $this->create($input);
        return $type;
    }

    /**
     * @param $request
     * @param $type
     * @return mixed
     */
    public function updateRecord($request, $type)
    {
        $input = $request->all();
        $type = $this->update($input, $type->id);
        return $type;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $type = $this->delete($id);
        return $type;
    }
}
