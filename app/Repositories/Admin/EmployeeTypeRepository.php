<?php

namespace App\Repositories\Admin;

use App\Models\EmployeeType;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class EmployeeTypeRepository
 * @package App\Repositories\Admin
 * @version July 16, 2019, 11:50 pm UTC
 *
 * @method EmployeeType findWithoutFail($id, $columns = ['*'])
 * @method EmployeeType find($id, $columns = ['*'])
 * @method EmployeeType first($columns = ['*'])
*/
class EmployeeTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EmployeeType::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $employeeType = $this->create($input);
        return $employeeType;
    }

    /**
     * @param $request
     * @param $employeeType
     * @return mixed
     */
    public function updateRecord($request, $employeeType)
    {
        $input = $request->all();
        $employeeType = $this->update($input, $employeeType->id);
        return $employeeType;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $employeeType = $this->delete($id);
        return $employeeType;
    }
}
