<?php

namespace App\Repositories\Admin;

use App\Models\Client;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ClientRepository
 * @package App\Repositories\Admin
 * @version July 16, 2019, 8:48 pm UTC
 *
 * @method Client findWithoutFail($id, $columns = ['*'])
 * @method Client find($id, $columns = ['*'])
 * @method Client first($columns = ['*'])
*/
class ClientRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'email',
        'password'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Client::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->only(['name','email','password']);
        $input['password'] = bcrypt($request->password);
        $client = $this->create($input);
        return $client;
    }

    /**
     * @param $request
     * @param $client
     * @return mixed
     */
    public function updateRecord($request, $client)
    {
        $input = $request->all();
        $client = $this->update($input, $client->id);
        return $client;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $client = $this->delete($id);
        return $client;
    }
}
