<?php

namespace App\Repositories\Admin;

use App\Models\Payment;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PaymentRepository
 * @package App\Repositories\Admin
 * @version September 23, 2019, 10:41 pm UTC
 *
 * @method Payment findWithoutFail($id, $columns = ['*'])
 * @method Payment find($id, $columns = ['*'])
 * @method Payment first($columns = ['*'])
 */
class PaymentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'type',
        'property_id',
        'schedule_id'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Payment::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->only(['type', 'property_id', 'booking_id']);
        $payment = $this->create($input);
        return $payment;
    }

    /**
     * @param $request
     * @param $payment
     * @return mixed
     */
    public function updateRecord($request, $payment)
    {
        $input = $request->all();
        $payment = $this->update($input, $payment->id);
        return $payment;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $payment = $this->delete($id);
        return $payment;
    }
}
