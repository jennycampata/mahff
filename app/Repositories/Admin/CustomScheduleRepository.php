<?php

namespace App\Repositories\Admin;

use App\Models\CustomSchedule;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class CustomScheduleRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 11:03 pm UTC
 *
 * @method CustomSchedule findWithoutFail($id, $columns = ['*'])
 * @method CustomSchedule find($id, $columns = ['*'])
 * @method CustomSchedule first($columns = ['*'])
*/
class CustomScheduleRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'area_id',
        'project_id',
        'mode_id',
        'time_duration',
        'amount'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CustomSchedule::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $customSchedule = $this->create($input);
        return $customSchedule;
    }

    /**
     * @param $request
     * @param $customSchedule
     * @return mixed
     */
    public function updateRecord($request, $customSchedule)
    {
        $input = $request->all();
        $customSchedule = $this->update($input, $customSchedule->id);
        return $customSchedule;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $customSchedule = $this->delete($id);
        return $customSchedule;
    }
}
