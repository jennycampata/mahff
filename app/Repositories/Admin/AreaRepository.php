<?php

namespace App\Repositories\Admin;

use App\Models\Area;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class AreaRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 10:52 pm UTC
 *
 * @method Area findWithoutFail($id, $columns = ['*'])
 * @method Area find($id, $columns = ['*'])
 * @method Area first($columns = ['*'])
*/
class AreaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Area::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $area = $this->create($input);
        return $area;
    }

    /**
     * @param $request
     * @param $area
     * @return mixed
     */
    public function updateRecord($request, $area)
    {
        $input = $request->all();
        $area = $this->update($input, $area->id);
        return $area;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $area = $this->delete($id);
        return $area;
    }
}
