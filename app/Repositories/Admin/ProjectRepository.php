<?php

namespace App\Repositories\Admin;

use App\Models\Project;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ProjectRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 10:55 pm UTC
 *
 * @method Project findWithoutFail($id, $columns = ['*'])
 * @method Project find($id, $columns = ['*'])
 * @method Project first($columns = ['*'])
*/
class ProjectRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name',
        'type_id',
        'detail',
        'address',
        'starting_date',
        'ending_date',
        'count'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Project::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $project = $this->create($input);
        return $project;
    }

    /**
     * @param $request
     * @param $project
     * @return mixed
     */
    public function updateRecord($request, $project)
    {
        $input = $request->all();
        $project = $this->update($input, $project->id);
        return $project;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $project = $this->delete($id);
        return $project;
    }
}
