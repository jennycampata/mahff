<?php

namespace App\Repositories\Admin;

use App\Models\Property;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PropertyRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 10:56 pm UTC
 *
 * @method Property findWithoutFail($id, $columns = ['*'])
 * @method Property find($id, $columns = ['*'])
 * @method Property first($columns = ['*'])
*/
class PropertyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'project_id',
        'block_id',
        'area_id',
        'extra_land',
        'road_facing',
        'park_facing',
        'west_open',
        'corner'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Property::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $property = $this->create($input);
        return $property;
    }

    /**
     * @param $request
     * @param $property
     * @return mixed
     */
    public function updateRecord($request, $property)
    {
        $input = $request->all();
        $property = $this->update($input, $property->id);
        return $property;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $property = $this->delete($id);
        return $property;
    }
}
