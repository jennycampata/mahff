<?php

namespace App\Repositories\Admin;

use App\Models\Bank;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class BankRepository
 * @package App\Repositories\Admin
 * @version September 23, 2019, 9:11 pm UTC
 *
 * @method Bank findWithoutFail($id, $columns = ['*'])
 * @method Bank find($id, $columns = ['*'])
 * @method Bank first($columns = ['*'])
*/
class BankRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'project_id',
        'name',
        'branch_name',
        'account'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Bank::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $bank = $this->create($input);
        return $bank;
    }

    /**
     * @param $request
     * @param $bank
     * @return mixed
     */
    public function updateRecord($request, $bank)
    {
        $input = $request->all();
        $bank = $this->update($input, $bank->id);
        return $bank;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $bank = $this->delete($id);
        return $bank;
    }
}
