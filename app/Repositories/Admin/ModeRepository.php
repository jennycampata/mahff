<?php

namespace App\Repositories\Admin;

use App\Models\Mode;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ModeRepository
 * @package App\Repositories\Admin
 * @version July 15, 2019, 11:00 pm UTC
 *
 * @method Mode findWithoutFail($id, $columns = ['*'])
 * @method Mode find($id, $columns = ['*'])
 * @method Mode first($columns = ['*'])
*/
class ModeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id',
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mode::class;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function saveRecord($request)
    {
        $input = $request->all();
        $mode = $this->create($input);
        return $mode;
    }

    /**
     * @param $request
     * @param $mode
     * @return mixed
     */
    public function updateRecord($request, $mode)
    {
        $input = $request->all();
        $mode = $this->update($input, $mode->id);
        return $mode;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteRecord($id)
    {
        $mode = $this->delete($id);
        return $mode;
    }
}
