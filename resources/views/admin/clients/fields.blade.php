<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Enter name']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder'=>'Enter email']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', null, ['class' => 'form-control', 'placeholder'=>'Enter password']) !!}
</div>

<!-- Client Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cleint_name', 'Client Name:') !!}
    {!! Form::text('cleint_name', null, ['class' => 'form-control', 'placeholder'=>'Enter name']) !!}
</div>

<!-- Client Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Client Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder'=>'Enter Email']) !!}
</div>

<!-- Client Contact_no Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_no', 'Client Contact No:') !!}
    {!! Form::text('contact_no', null, ['class' => 'form-control', 'placeholder'=>'Enter Contact']) !!}
</div>

<!-- Client Contact no2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contactno2', 'Contact No2:') !!}
    {!! Form::text('contact_no2', null, ['class' => 'form-control', 'placeholder'=>'Enter contact']) !!}
</div>


<!-- Cleint address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder'=>'Enter address']) !!}
</div>

<!-- Client Postal Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('postal_address', 'Postal Address:') !!}
    {!! Form::text('postal_address', null, ['class' => 'form-control', 'placeholder'=>'Enter Postal Address']) !!}
</div>


<!-- CNIC  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cnic_no', 'Cnic_no') !!}
    {!! Form::text('cnic_no', null, ['class' => 'form-control', 'placeholder'=>'Enter Cnic no']) !!}
</div>

<div class="form-group col-sm-6">
    {!! Form::label('father_name_husband_name', 'Father Name / Husband Name') !!}
    {!! Form::text('father_name_husband_name', null, ['class' => 'form-control', 'placeholder'=>'Enter']) !!}
</div>

<!-- Nominee Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominee_name', 'Nominee Name:') !!}
    {!! Form::text('nominee_name', null, ['class' => 'form-control', 'placeholder'=>'Enter Nominee name']) !!}
</div>

<!-- Nominee Relation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominee_relation', 'Nominee Relation:') !!}
    {!! Form::text('nominee_relation', null, ['class' => 'form-control', 'placeholder'=>'Enter Nominee Relation']) !!}
</div>

<!-- Nominee Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominee_address', 'Nominee Address:') !!}
    {!! Form::text('nominee_address', null, ['class' => 'form-control', 'placeholder'=>'Enter Nominee Address']) !!}
</div>

<!-- Nominee Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominee_email', 'Nominee Email:') !!}
    {!! Form::text('nominee_email', null, ['class' => 'form-control', 'placeholder'=>'Enter Nominee Email']) !!}
</div>

<!--  Nominee Contact Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominee_contactno', 'Nominee Contact:') !!}
    {!! Form::text('nominee_contactno', null, ['class' => 'form-control', 'placeholder'=>'Enter Nominee Contact No']) !!}
</div>

<!-- Nominee Contactno2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nominee_contactno2', 'Nominee Contact No 2:') !!}
    {!! Form::text('nominee_contactno2', null, ['class' => 'form-control', 'placeholder'=>'Enter Nominee Contact No']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($client))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.clients.index') !!}" class="btn btn-default">Cancel</a>
</div>