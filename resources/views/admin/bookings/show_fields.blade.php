<!-- Id Field -->
<dt>{!! Form::label('Id', 'Id:') !!}</dt>
<dd>{!! $booking->Id !!}</dd>

<!-- Project Id Field -->
<dt>{!! Form::label('project_id', 'Project Id:') !!}</dt>
<dd>{!! $booking->project_id !!}</dd>

<!-- Area Id Field -->
<dt>{!! Form::label('area_id', 'Area Id:') !!}</dt>
<dd>{!! $booking->area_id !!}</dd>

<!-- Block Id Field -->
<dt>{!! Form::label('block_id', 'Block Id:') !!}</dt>
<dd>{!! $booking->block_id !!}</dd>

<!-- Schedule Id Field -->
<dt>{!! Form::label('schedule_id', 'Schedule Id:') !!}</dt>
<dd>{!! $booking->schedule_id !!}</dd>

<!-- Client Id Field -->
<dt>{!! Form::label('client_id', 'Client Id:') !!}</dt>
<dd>{!! $booking->client_id !!}</dd>

<!-- Booking Date Field -->
<dt>{!! Form::label('booking_date', 'Booking Date:') !!}</dt>
<dd>{!! $booking->booking_date !!}</dd>

<!-- Employee Id Field -->
<dt>{!! Form::label('employee_id', 'Employee Id:') !!}</dt>
<dd>{!! $booking->employee_id !!}</dd>

<!-- File No Field -->
<dt>{!! Form::label('file_no', 'File No:') !!}</dt>
<dd>{!! $booking->file_no !!}</dd>

<dt>{!! Form::label('file_no', 'Total Amount:') !!}</dt>
<dd>{!! $total !!}</dd>

<dt>{!! Form::label('installment_amount', 'Installment Amount:') !!}</dt>
<dd>{!! $installment !!}</dd>

<dt>{!! Form::label('amount_paid', 'Amount Paid:') !!}</dt>
<dd>{!! $paid !!}</dd>

<div class='btn-group'>
    <a href="{{url('admin/payments/create/'.$booking->id)}}" class="btn btn-primary">
        <i class="glyphicon glyphicon-"></i>Add Payment
    </a>
</div>


