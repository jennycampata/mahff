<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('project_id', 'Project:') !!}
    {!! Form::select('project_id', $project,null, ['class' => 'form-control select2 project_id',]) !!}
</div>

<!-- Area Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area_id', 'Area:') !!}
    {!! Form::select('area_id',$area ,null,['class' => 'form-control select2 area_id', ]) !!}
</div>

<!-- Block Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('block_id', 'Block:') !!}
    {!! Form::select('block_id', $block ,null, ['class' => 'form-control select2 block_id',]) !!}
</div>


<!-- Block Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('property_id', 'Property:') !!}
    {!! Form::select('property_id', [] ,null, ['class' => 'form-control select2 property_id']) !!}
</div>
<!-- Schedule Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('schedule_id', 'Schedule:') !!}
    {!! Form::select('schedule_id', $schedule ,null, ['class' => 'form-control select2']) !!}
</div>

<!-- Client Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_id', 'Client:') !!}
    {!! Form::select('client_id', $client, null, ['class' => 'form-control select2']) !!}
</div>

<!-- Booking Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('booking_date', 'Booking Date:') !!}
    {!! Form::date('booking_date', null, ['class' => 'form-control', 'placeholder'=>'Enter booking_date']) !!}
</div>

<!-- Employee Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employee_id', 'Employee:') !!}
    {!! Form::select('employee_id', $employee,null, ['class' => 'form-control select2', 'placeholder'=>'Enter Employee']) !!}
</div>

<!-- File No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('file_no', 'File No:') !!}
    {!! Form::text('file_no', null, ['class' => 'form-control', 'placeholder'=>'Enter file_no']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($booking))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.bookings.index') !!}" class="btn btn-default">Cancel</a>
</div>