<!-- Client dropdown Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Client_id', 'Client_id:') !!}
    {!! Form::text('Client_id',['raza'], null, ['class' => 'form-control', 'placeholder'=>'Client_id']) !!}
</div>

<!-- Transfer Fee  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Transfer_Fee', 'Transfer Fee:') !!}
    {!! Form::text('transfer_fee', null, ['class' => 'form-control', 'placeholder'=>'Transfer_fee']) !!}
</div>


<!-- Transfer Date  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Transfer_Date', 'Transfer Date:') !!}
    {!! Form::text('transfer_date', null, ['class' => 'form-control', 'placeholder'=>'Transfer_date']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($payment))
            {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.payments.index') !!}" class="btn btn-default">Cancel</a>
</div>