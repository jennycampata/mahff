@extends('admin.layouts.app')

@section('title')
    {{ $designation->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($designation, ['route' => ['admin.designations.update', $designation->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.designations.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection