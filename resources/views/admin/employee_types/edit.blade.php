@extends('admin.layouts.app')

@section('title')
    {{ $employeeType->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($employeeType, ['route' => ['admin.employee-types.update', $employeeType->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.employee_types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection