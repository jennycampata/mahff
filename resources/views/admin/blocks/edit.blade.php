@extends('admin.layouts.app')

@section('title')
    {{ $block->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($block, ['route' => ['admin.blocks.update', $block->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.blocks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection