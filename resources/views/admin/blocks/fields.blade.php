
<!-- area Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area_id', 'Area Id:') !!}
    {!! Form::select('area_id', $area_id ,null, ['class' => 'form-control select2']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Enter name']) !!}
</div>



<!-- project_id Field -->
<div class="form-group col-sm-6">
    {{--{!! Form::label('project_id', 'Project Name:') !!}--}}
    {!! Form::hidden('project_id',isset($project_id) ? $project_id:null, ['class' => 'form-control', 'placeholder'=>'Enter name']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($block))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.blocks.index') !!}" class="btn btn-default">Cancel</a>
</div>