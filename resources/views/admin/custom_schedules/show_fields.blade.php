<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $customSchedule->id !!}</dd>

<!-- Area Id Field -->
<dt>{!! Form::label('area_id', 'Area Id:') !!}</dt>
<dd>{!! $customSchedule->area_id !!}</dd>

<!-- Project Id Field -->
<dt>{!! Form::label('project_id', 'Project Id:') !!}</dt>
<dd>{!! $customSchedule->project_id !!}</dd>

<!-- Mode Id Field -->
<dt>{!! Form::label('mode_id', 'Mode Id:') !!}</dt>
<dd>{!! $customSchedule->mode_id !!}</dd>

<!-- Time Duration Field -->
<dt>{!! Form::label('time_duration', 'Time Duration:') !!}</dt>
<dd>{!! $customSchedule->time_duration !!}</dd>

<!-- Amount Field -->
<dt>{!! Form::label('amount', 'Amount:') !!}</dt>
<dd>{!! $customSchedule->amount !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $customSchedule->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $customSchedule->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $customSchedule->deleted_at !!}</dd>

<!-- Status Field -->
<dt>{!! Form::label('status', 'Status:') !!}</dt>
<dd>{!! $customSchedule->status !!}</dd>

