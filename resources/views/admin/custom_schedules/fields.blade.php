<!-- Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::text('id', null, ['class' => 'form-control', 'placeholder'=>'Enter id']) !!}
</div>

<!-- Area Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area_id', 'Area Id:') !!}
    {!! Form::text('area_id', null, ['class' => 'form-control', 'placeholder'=>'Enter area_id']) !!}
</div>

<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('project_id', 'Project Id:') !!}
    {!! Form::text('project_id', null, ['class' => 'form-control', 'placeholder'=>'Enter project_id']) !!}
</div>

<!-- Mode Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('mode_id', 'Mode Id:') !!}
    {!! Form::text('mode_id', null, ['class' => 'form-control', 'placeholder'=>'Enter mode_id']) !!}
</div>

<!-- Time Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('time_duration', 'Time Duration:') !!}
    {!! Form::text('time_duration', null, ['class' => 'form-control', 'placeholder'=>'Enter time_duration']) !!}
</div>

<!-- Amount Field -->
<div class="form-group col-sm-6">
    {!! Form::label('amount', 'Amount:') !!}
    {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder'=>'Enter amount']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($customSchedule))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.custom-schedules.index') !!}" class="btn btn-default">Cancel</a>
</div>