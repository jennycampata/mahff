@extends('admin.layouts.app')

@section('title')
    {{ $customSchedule->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($customSchedule, ['route' => ['admin.custom-schedules.update', $customSchedule->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.custom_schedules.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection