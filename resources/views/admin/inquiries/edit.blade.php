@extends('admin.layouts.app')

@section('title')
    {{ $inquiry->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($inquiry, ['route' => ['admin.inquiries.update', $inquiry->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.inquiries.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection