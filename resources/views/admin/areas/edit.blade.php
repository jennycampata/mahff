@extends('admin.layouts.app')

@section('title')
    {{ $area->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($area, ['route' => ['admin.areas.update', $area->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.areas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection