@extends('admin.layouts.app')

@section('title')
    {{ $type->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($type, ['route' => ['admin.types.update', $type->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.types.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection