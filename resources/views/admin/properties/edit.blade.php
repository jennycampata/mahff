@extends('admin.layouts.app')

@section('title')
    {{ $property->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($property, ['route' => ['admin.properties.update', $property->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.properties.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection