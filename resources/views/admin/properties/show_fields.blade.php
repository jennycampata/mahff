<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $property->id !!}</dd>

<!-- Project Id Field -->
<dt>{!! Form::label('project_id', 'Project Id:') !!}</dt>
<dd>{!! $property->project_id !!}</dd>

<!-- Block Id Field -->
<dt>{!! Form::label('block_id', 'Block Id:') !!}</dt>
<dd>{!! $property->block_id !!}</dd>

<!-- Area Id Field -->
<dt>{!! Form::label('area_id', 'Area Id:') !!}</dt>
<dd>{!! $property->area_id !!}</dd>

<!-- Extra Land Field -->
<dt>{!! Form::label('extra_land', 'Extra Land:') !!}</dt>
<dd>{!! $property->extra_land !!}</dd>

<!-- Road Facing Field -->
<dt>{!! Form::label('road_facing', 'Road Facing:') !!}</dt>
<dd>{!! $property->road_facing !!}</dd>

<!-- Park Facing Field -->
<dt>{!! Form::label('park_facing', 'Park Facing:') !!}</dt>
<dd>{!! $property->park_facing !!}</dd>

<!-- West Open Field -->
<dt>{!! Form::label('west_open', 'West Open:') !!}</dt>
<dd>{!! $property->west_open !!}</dd>

<!-- Corner Field -->
<dt>{!! Form::label('corner', 'Corner:') !!}</dt>
<dd>{!! $property->corner !!}</dd>

<!-- Status Field -->
<dt>{!! Form::label('status', 'Status:') !!}</dt>
<dd>{!! $property->status !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $property->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $property->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $property->deleted_at !!}</dd>

