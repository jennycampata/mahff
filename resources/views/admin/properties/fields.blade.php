<!-- Block Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('block_id', 'Block Id:') !!}
    {!! Form::select('block_id', $block ,null, ['class' => 'form-control select2']) !!}
</div>

<!-- Area Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area_id', 'Area Id:') !!}
    {!! Form::select('area_id', $area,null, ['class' => 'form-control select2']) !!}
</div>

<!-- name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Property Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control ']) !!}
</div>

<!-- Extra Land Field -->
<div class="form-group col-sm-6">
    {!! Form::label('extra_land', 'Extra Land:') !!}
    {!! Form::text('extra_land', null, ['class' => 'form-control', 'placeholder'=>'Enter extra_land']) !!}
</div>

<!-- Road Facing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('road_facing', 'Road Facing:') !!}
    {!! Form::text('road_facing', null, ['class' => 'form-control', 'placeholder'=>'Enter road_facing']) !!}
</div>

<!-- Park Facing Field -->
<div class="form-group col-sm-6">
    {!! Form::label('park_facing', 'Park Facing:') !!}
    {!! Form::text('park_facing', null, ['class' => 'form-control', 'placeholder'=>'Enter park_facing']) !!}
</div>

<!-- West Open Field -->
<div class="form-group col-sm-6">
    {!! Form::label('west_open', 'West Open:') !!}
    {!! Form::text('west_open', null, ['class' => 'form-control', 'placeholder'=>'Enter west_open']) !!}
</div>

<!-- Corner Field -->
<div class="form-group col-sm-6">
    {!! Form::label('corner', 'Corner:') !!}
    {!! Form::text('corner', null, ['class' => 'form-control', 'placeholder'=>'Enter corner']) !!}
</div>


<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {{--{!! Form::label('project_id', 'Project Id:') !!}--}}
    {!! Form::hidden('project_id', isset($project_id) ? $project_id:null, ['class' => 'form-control', 'placeholder'=>'Enter project_id' ]) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.properties.index') !!}" class="btn btn-default">Cancel</a>
</div>