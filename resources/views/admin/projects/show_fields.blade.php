<div class="box box-primary">
    <div class="box-body">
        <!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $project->id !!}</dd>

<!-- Name Field -->
<dt>{!! Form::label('name', 'Name:') !!}</dt>
<dd>{!! $project->name !!}</dd>

<!-- Type Id Field -->
<dt>{!! Form::label('type_id', 'Type Id:') !!}</dt>
<dd>{!! $project->type_id !!}</dd>

<!-- Detail Field -->
<dt>{!! Form::label('detail', 'Detail:') !!}</dt>
<dd>{!! $project->detail !!}</dd>

<!-- Address Field -->
<dt>{!! Form::label('address', 'Address:') !!}</dt>
<dd>{!! $project->address !!}</dd>

<!-- Starting Date Field -->
<dt>{!! Form::label('starting_date', 'Starting Date:') !!}</dt>
<dd>{!! $project->starting_date !!}</dd>

<!-- Ending Date Field -->
<dt>{!! Form::label('ending_date', 'Ending Date:') !!}</dt>
<dd>{!! $project->ending_date !!}</dd>

<!-- Count Field -->
<dt>{!! Form::label('count', 'Count:') !!}</dt>
<dd>{!! $project->count !!}</dd>


    {{--<a href="{{url('admin/blocks/create/'.$project->id)}}"><button>Add Blocks</button></a>--}}
            {{--<a href="{{url('admin/schedules/create/'.$project->id)}}"><button>Add Schedule</button></a>--}}

        <div class='btn-group'>
            <a href="{{url('admin/schedules/create/'.$project->id)}}" class="btn btn-primary">
                <i class="glyphicon glyphicon-"></i>Add Schedule
            </a>
        </div>

        <div class='btn-group'>
            <a href="{{url('admin/blocks/create/'.$project->id)}}" class="btn btn-success">
                <i class="glyphicon glyphicon-"></i>Add Block
            </a>
        </div>


    </div>
</div>


<div class="box box-primary">
    <div class="box-header with-border">
        <h2 class="box-title">Add Property</h2>
    </div>
    <div class="box-body">
        <div class="row datatable-action-urls"
             data-action-create="{{route('admin.properties.create',['project_id'=>$project->id])}}">
            <div class="col-sm-12">
                @include('admin.properties.table')
            </div>
        </div>
    </div>
</div>




{{--<div class="box box-primary">--}}
    {{--<div class="box-header with-border">--}}
        {{--<h2 class="box-title">Add Block</h2>--}}
    {{--</div>--}}
    {{--<div class="box-body">--}}
        {{--<div class="row datatable-action-urls"--}}
             {{--data-action-create="{{route('admin.blocks.create',['project_id'=>$project->id])}}">--}}
            {{--<div class="col-sm-12">--}}
                {{--@include('admin.blocks.table')--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
