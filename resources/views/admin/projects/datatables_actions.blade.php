{!! Form::open(['route' => ['admin.projects.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    @ability('super-admin' ,'projects.show')
    <a href="{{ route('admin.projects.show', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-eye-open"></i>
    </a>
    @endability

    @ability('super-admin' ,'projects.edit')
    <a href="{{ route('admin.projects.edit', $id) }}" class='btn btn-default btn-xs'>
        <i class="glyphicon glyphicon-edit"></i>
    </a>
    @endability
    @ability('super-admin' ,'projects.destroy')
    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', [
        'type' => 'submit',
        'class' => 'btn btn-danger btn-xs',
        'onclick' => "confirmDelete($(this).parents('form')[0]); return false;"
    ]) !!}
    @endability
</div>
{!! Form::close() !!}
{{--<script>--}}
    {{--$(document).ready(function () {--}}

        {{--console.log('asdasd');--}}

        {{--$('.yes').on ('click',function () {--}}
            {{--// var token = $('meta[name="csrf-token"]').attr('content');--}}
            {{--var id = $(this).data("id");--}}
            {{--console.log('sss',id);--}}
            {{--$.ajax--}}
            {{--({--}}
                {{--type: "GET",--}}
                {{--url: 'http://localhost:84/mahff/admin/project/view',--}}
                {{--data:--}}
                    {{--{--}}
                        {{--data: id,--}}
                        {{--//_token: token--}}
                    {{--}--}}
                {{--,--}}
                {{--cache: false,--}}
                {{--success:--}}
                    {{--function (data) {--}}

                    {{--console.log(id);--}}
                    {{--//    location.reload();--}}
                    {{--}--}}
            {{--});--}}

        {{--});--}}
    {{--});--}}

{{--</script>--}}