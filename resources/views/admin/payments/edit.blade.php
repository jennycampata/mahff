@extends('admin.layouts.app')

@section('title')
    {{ $payment->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($payment, ['route' => ['admin.payments.update', $payment->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.payments.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection