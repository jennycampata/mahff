<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $payment->id !!}</dd>

<!-- Type Field -->
<dt>{!! Form::label('type', 'Type:') !!}</dt>
<dd>{!! $payment->type !!}</dd>

<!-- Property Id Field -->
<dt>{!! Form::label('property_id', 'Property Id:') !!}</dt>
<dd>{!! $payment->property_id !!}</dd>

<!-- Schedule Id Field -->
<dt>{!! Form::label('schedule_id', 'Schedule Id:') !!}</dt>
<dd>{!! $payment->schedule_id !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $payment->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $payment->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $payment->deleted_at !!}</dd>

