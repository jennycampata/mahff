<div class="form-group col-sm-12">
    <h3>Personal</h3>
</div>

{!! Form::hidden('booking_id',$booking_id, null, ['class' => 'form-control', 'placeholder'=>'Enter id']) !!}
{!! Form::hidden('property_id',$property_id, null, ['class' => 'form-control', 'placeholder'=>'Enter id']) !!}


<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id', 'Payment type:') !!}
    {!! Form::select('type', \App\Models\Payment::$PAYMENT_TYPE ,null, [ 'id' =>'payment1','class' => 'form-control select2 type']) !!}
</div>


<div style="display: none" id="payment2" class="form-group col-sm-6">
    {!! Form::label('payment_type', 'Drawn on bank:') !!}
    {!! Form::select('payment_type',[] ,null, ['class' => 'form-control select2 type', 'placeholder'=>'payment_type']) !!}
</div>

<div id="branch" class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control', 'placeholder'=>'Enter Description']) !!}
</div>

<div class="form-group col-sm-12">
    <h3>Client</h3>
</div>

<!-- Payment type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('client_payment_type', 'Client Payment Type:') !!}
    {!! Form::select('client_payment_type', \App\Models\Payment::$CLIENT_PAYMENT_TYPE  ,null, ['class' => 'form-control select2 client_type']) !!}
</div>

<!-- Payment_mode Field -->
<div class="form-group col-sm-6 ">
    {!! Form::label('id', 'Payment Mode:') !!}
    {!! Form::select('Payment_mode', $mode ,null, ['class' => 'form-control select2 type']) !!}
</div>

<!-- bank Id Field -->
<div class="form-group col-sm-6 client_bank">
    {!! Form::label('bank', 'Bank:') !!}
    {!! Form::text('bank', null, ['class' => 'form-control', 'placeholder'=>'bank']) !!}
</div>

<!-- cheque Field -->
<div class="form-group col-sm-6 client_bank">
    {!! Form::label('cheque', 'Cheque NO:') !!}
    {!! Form::text('cheque', null, ['class' => 'form-control', 'placeholder'=>'cheque no' ]) !!}
</div>


<!-- Property Id Field -->
<div class="form-group col-sm-6 client_bank">
    {!! Form::label('date', 'Cheque Date:') !!}
    {!! Form::date('date', null, ['class' => 'form-control', 'placeholder'=>'Enter property_id']) !!}
</div>


<!-- Installment  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('Installment Amount', 'Installment Amount:') !!}
    {!! Form::text('Installment Amount', null, ['class' => 'form-control', 'placeholder'=>'Installment Amount']) !!}
</div>

<!-- project_id Field -->
<div class="form-group col-sm-6">
    {{--{!! Form::label('project_id', 'Project Name:') !!}--}}
    {!! Form::hidden('booking_id',isset($booking_id) ? $booking_id:null, ['class' => 'form-control', 'placeholder'=>'Enter name']) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($payment))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.payments.index') !!}" class="btn btn-default">Cancel</a>
</div>
<script>
    $(document).ready(function () {
        var type = $('#payment1 option:selected').val();
        console.log(type);
        if (type == 0) {
            $("#one").css("display", "block");
            $("#drawn").css("display", "block");
            $("#payment2").css("display", "block");
            $("#two").css("display", "none");
            $("#branch").css("display", "none");
        }

        $("#payment1").on('change', function () {
            $(this).find("option:selected").each(function () {
                var optionValue = $(this).attr("value");
                if (optionValue == 1) {
                    console.log(optionValue);
                    $("#drawn").css("display", "none");
                    $("#payment2").css("display", "none");
                    $("#one").css("display", "none");
                    $("#two").css("display", "none");
                    $("#branch").css("display", "block");
                } else if (optionValue == 0) {
                    console.log(optionValue);
                    $("#one").css("display", "block");
                    $("#drawn").css("display", "block");
                    $("#payment2").css("display", "block");
                    $("#two").css("display", "none");
                    $("#branch").css("display", "none");

                } else if (optionValue == 2) {
                    console.log(optionValue);
                    $("#one").css("display", "none");
                    $("#drawn").css("display", "none");
                    $("#payment2").css("display", "none");
                    $("#two").css("display", "none");
                    $("#branch").css("display", "block");

                } else {
                    console.log(optionValue);
                    $("#one").css("display", "none");
                    $("#drawn").css("display", "none");
                    $("#payment2").css("display", "none");
                    $("#two").css("display", "block");
                    $("#branch").css("display", "none");

                }
            });
        });

        $('.client_type').on('change', function () {
            var type = $('.client_type option:selected').val();
            if (type == 1) {
                $('.client_bank').hide();
            } else {
                $('.client_bank').show();
            }
        });
    })


</script>