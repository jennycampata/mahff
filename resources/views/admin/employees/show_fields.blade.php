<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $employee->id !!}</dd>

<!-- Name Field -->
<dt>{!! Form::label('name', 'Name:') !!}</dt>
<dd>{!! $employee->name !!}</dd>

<!-- Email Field -->
<dt>{!! Form::label('email', 'Email:') !!}</dt>
<dd>{!! $employee->email !!}</dd>

<!-- Password Field -->
<dt>{!! Form::label('password', 'Password:') !!}</dt>
<dd>{!! $employee->password !!}</dd>

<!-- Remember Token Field -->
<dt>{!! Form::label('remember_token', 'Remember Token:') !!}</dt>
<dd>{!! $employee->remember_token !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $employee->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $employee->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $employee->deleted_at !!}</dd>

