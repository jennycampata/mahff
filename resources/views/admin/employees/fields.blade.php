

<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Enter name']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::text('email', null, ['class' => 'form-control', 'placeholder'=>'Enter email']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::text('password', null, ['class' => 'form-control', 'placeholder'=>'Enter password']) !!}
</div>



<!-- Project_id  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('project_id', 'Project_id') !!}
    {!! Form::select('project_id', $project ,null, ['class' => 'form-control select2' ]) !!}
</div>

<!-- Designation_id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('designation_id', 'Designation_id') !!}
    {!! Form::select('designation_id',$designation ,null, ['class' => 'form-control select2']) !!}
</div>


<!-- Employee_type_id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employee_type_id', 'Employee_Type_id') !!}
    {!! Form::select('employee_type_id', $employeetype,null, ['class' => 'form-control select2']) !!}
</div>

<!-- Employee Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('employee_name', 'Employee_name') !!}
    {!! Form::text('employee_name', null, ['class' => 'form-control', 'placeholder'=>'Enter Employee name']) !!}
</div>


<!-- Contact No Field -->
<div class="form-group col-sm-6">
    {!! Form::label('contact_no', 'Contact_no') !!}
    {!! Form::text('contact_no', null, ['class' => 'form-control', 'placeholder'=>'Enter number']) !!}
</div>


<!-- Employee Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address') !!}
    {!! Form::text('address', null, ['class' => 'form-control', 'placeholder'=>'Enter Address']) !!}
</div>


<!-- Employee CNIC  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('cnic_no', 'Cnic_no') !!}
    {!! Form::text('cnic_no', null, ['class' => 'form-control', 'placeholder'=>'Enter CNIC']) !!}
</div>


<!-- Employee Gender  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('gender', 'Gender') !!}
    {!! Form::select('gender', ['Male','Female'], null, ['class' => 'form-control']) !!}
</div>


<!-- Employee Marriage status  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('marriage_status', 'Marriage_status') !!}
    {!! Form::select('marriage_status', ['Single','Married'], null, ['class' => 'form-control']) !!}
</div>

<!-- Employee salary  Field -->
<div class="form-group col-sm-6">
    {!! Form::label('salary', 'Salary') !!}
    {!! Form::text('salary', null, ['class' => 'form-control', 'placeholder'=>'Enter salary']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($employee))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.employees.index') !!}" class="btn btn-default">Cancel</a>
</div>