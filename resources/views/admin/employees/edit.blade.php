@extends('admin.layouts.app')

@section('title')
    {{ $employee->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($employee, ['route' => ['admin.employees.update', $employee->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.employees.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection