@extends('admin.layouts.app')

@section('title')
    {{ $mode->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($mode, ['route' => ['admin.modes.update', $mode->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.modes.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection