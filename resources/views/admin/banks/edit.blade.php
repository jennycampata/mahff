@extends('admin.layouts.app')

@section('title')
    {{ $bank->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($bank, ['route' => ['admin.banks.update', $bank->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.banks.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection