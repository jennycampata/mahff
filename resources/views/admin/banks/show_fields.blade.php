<!-- Id Field -->
<dt>{!! Form::label('id', 'Id:') !!}</dt>
<dd>{!! $bank->id !!}</dd>

<!-- Project Id Field -->
<dt>{!! Form::label('project_id', 'Project Id:') !!}</dt>
<dd>{!! $bank->project_id !!}</dd>

<!-- Name Field -->
<dt>{!! Form::label('name', 'Name:') !!}</dt>
<dd>{!! $bank->name !!}</dd>

<!-- Branch Name Field -->
<dt>{!! Form::label('branch_name', 'Branch Name:') !!}</dt>
<dd>{!! $bank->branch_name !!}</dd>

<!-- Account Field -->
<dt>{!! Form::label('account', 'Account:') !!}</dt>
<dd>{!! $bank->account !!}</dd>

<!-- Created At Field -->
<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>
<dd>{!! $bank->created_at !!}</dd>

<!-- Updated At Field -->
<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>
<dd>{!! $bank->updated_at !!}</dd>

<!-- Deleted At Field -->
<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>
<dd>{!! $bank->deleted_at !!}</dd>

