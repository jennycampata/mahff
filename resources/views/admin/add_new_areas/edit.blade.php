@extends('admin.layouts.app')

@section('title')
    {{ $addNewArea->name }} <small>{{ $title }}</small>
@endsection

@section('content')
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($addNewArea, ['route' => ['admin.add-new-areas.update', $addNewArea->id], 'method' => 'patch', 'files' => true]) !!}

                        @include('admin.add_new_areas.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection