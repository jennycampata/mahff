<div class="col-sm-4">
<!-- Id Field -->
{!! Form::label('id', 'Id:') !!}
{!! $schedule->id !!}
</div>
<div class="col-sm-4">
<!-- Area Id Field -->
{!! Form::label('area_id', 'Area Id:') !!}
{!! $schedule->area_id !!}
</div>
<div class="col-sm-4">
<!-- Project Id Field -->
{!! Form::label('project_id', 'Project Id:') !!}
{!! $schedule->project_id !!}
</div>
<br>
<hr>
@foreach($schedule->detail as $row)
    <div class="col-sm-12">
    <div class="col-sm-3">
    <!-- Mode Id Field -->
   {!! Form::label('mode_id', 'Mode Id:') !!}
        {!! $row->mode_id !!}
</div>

<div class="col-sm-3">    <!-- Time Duration Field -->
    {!! Form::label('time_duration', 'Time Duration:') !!}
    {!! $row->time_duration !!}
</div>

<div class="col-sm-3">
    <!-- Amount Field -->
    {!! Form::label('amount', 'Amount:') !!}
    {!! $row->amount !!}
</div>
<div class="col-sm-3">
<!-- Amount Field -->
    {!! Form::label('total_amount', 'Total Amount:') !!}
    {!! $row->time_duration * $row->amount !!}
</div>
</div>
<br>
@endforeach
<br>

{{--<!-- Created At Field -->--}}
{{--<dt>{!! Form::label('created_at', 'Created At:') !!}</dt>--}}
{{--<dd>{!! $schedule->created_at !!}</dd>--}}

{{--<!-- Updated At Field -->--}}
{{--<dt>{!! Form::label('updated_at', 'Updated At:') !!}</dt>--}}
{{--<dd>{!! $schedule->updated_at !!}</dd>--}}

{{--<!-- Deleted At Field -->--}}
{{--<dt>{!! Form::label('deleted_at', 'Deleted At:') !!}</dt>--}}
{{--<dd>{!! $schedule->deleted_at !!}</dd>--}}

