<style>
.mode{
 }
</style>
{{--<!-- Id Field -->--}}
{{--<div class="form-group col-sm-6">--}}
    {{--{!! Form::label('id', 'Id:') !!}--}}
    {{--{!! Form::text('id', null, ['class' => 'form-control', 'placeholder'=>'Enter id']) !!}--}}
{{--</div>--}}

<!-- Area Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('area_id', 'Area Id:') !!}
    {!! Form::select('area_id', $area,null, ['class' => 'form-control select2', 'placeholder'=>'Enter area_id']) !!}
</div>

<!-- Name Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Schedule Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Schedule Name']) !!}
</div>

<div class="form-group col-sm-12">
    {!! Form::label('custom Schedule', 'Custom Schedule:') !!}
    <div class="clearfix"></div>
    {!! Form::hidden('custom_schedule', false) !!}
    {!! Form::checkbox('custom_schedule', 1,  false, ['data-toggle'=>'toggle']) !!}
</div>

<!-- Project Id Field -->
<div class="form-group col-sm-6">
    {{--{!! Form::label('project_id', 'Project Id:') !!}--}}
    {!! Form::hidden('project_id', isset($project_id) ? $project_id:null, ['class' => 'form-control select2', 'placeholder'=>'Enter project_id']) !!}
</div>
<br>
<br>

<div class="clearfix"></div>
<div class=" form-group col-sm-12">
    <div class="form-group col-sm-4 mode"><b>Mode Of Payment</b></div>
    <div class="form-group col-sm-4 mode"><b>Payment time</b></div>
    <div class="form-group col-sm-3 mode"><b>Amount</b></div>
    <div class="col-sm-1 plus"><i id="cloneschedule" class="fa fa-plus"></i></div>
</div>

        <div class="col-sm-12 schedule" id="cards">

        <div class=" form-group col-sm-4">
            {!! Form::select('mode_id[]', $mode, null, ['class' => 'form-control select2', 'placeholder'=>'Enter Mode']) !!}
        </div>
        <div class="form-group col-sm-4">
            {!! Form::text('time_duration[]', null, ['class' => 'form-control', 'placeholder'=>'Enter time_duration']) !!}
        </div>
        <div class="form-group col-sm-3">
            {!! Form::text('amount[]', null, ['class' => 'form-control', 'placeholder'=>'Enter amount']) !!}
        </div>
        <div class="remove form-group col-sm-1"><i class="fa fa-close"></i></div>
            <div class="clearfix"></div>

    </div>

    <div id="schedule">
     </div>

        <!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    @if(!isset($schedule))
        {!! Form::submit(__('Save And Add Translations'), ['class' => 'btn btn-primary', 'name'=>'translation']) !!}
    @endif
    {!! Form::submit(__('Save And Add More'), ['class' => 'btn btn-primary', 'name'=>'continue']) !!}
    <a href="{!! route('admin.schedules.index') !!}" class="btn btn-default">Cancel</a>
</div>


<script>
$(document).ready(function () {
    $('#cloneschedule').on('click',function () {
        console.log('clone');
        $('#cards').clone().appendTo('#schedule');
    });

    $('div').on('click', ".remove" , function () {
       if($(".schedule").length !=1 ){
        $(this).closest(".schedule").remove();
       }
       else{
           event.preventDefault();
       }
    });

})
</script>
