<?php

use App\Models\Mode;
use App\Repositories\Admin\ModeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModeRepositoryTest extends TestCase
{
    use MakeModeTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var ModeRepository
     */
    protected $modeRepo;

    public function setUp()
    {
        parent::setUp();
        $this->modeRepo = App::make(ModeRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateMode()
    {
        $mode = $this->fakeModeData();
        $createdMode = $this->modeRepo->create($mode);
        $createdMode = $createdMode->toArray();
        $this->assertArrayHasKey('id', $createdMode);
        $this->assertNotNull($createdMode['id'], 'Created Mode must have id specified');
        $this->assertNotNull(Mode::find($createdMode['id']), 'Mode with given id must be in DB');
        $this->assertModelData($mode, $createdMode);
    }

    /**
     * @test read
     */
    public function testReadMode()
    {
        $mode = $this->makeMode();
        $dbMode = $this->modeRepo->find($mode->id);
        $dbMode = $dbMode->toArray();
        $this->assertModelData($mode->toArray(), $dbMode);
    }

    /**
     * @test update
     */
    public function testUpdateMode()
    {
        $mode = $this->makeMode();
        $fakeMode = $this->fakeModeData();
        $updatedMode = $this->modeRepo->update($fakeMode, $mode->id);
        $this->assertModelData($fakeMode, $updatedMode->toArray());
        $dbMode = $this->modeRepo->find($mode->id);
        $this->assertModelData($fakeMode, $dbMode->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteMode()
    {
        $mode = $this->makeMode();
        $resp = $this->modeRepo->delete($mode->id);
        $this->assertTrue($resp);
        $this->assertNull(Mode::find($mode->id), 'Mode should not exist in DB');
    }
}
