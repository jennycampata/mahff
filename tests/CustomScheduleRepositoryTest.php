<?php

use App\Models\CustomSchedule;
use App\Repositories\Admin\CustomScheduleRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomScheduleRepositoryTest extends TestCase
{
    use MakeCustomScheduleTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var CustomScheduleRepository
     */
    protected $customScheduleRepo;

    public function setUp()
    {
        parent::setUp();
        $this->customScheduleRepo = App::make(CustomScheduleRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateCustomSchedule()
    {
        $customSchedule = $this->fakeCustomScheduleData();
        $createdCustomSchedule = $this->customScheduleRepo->create($customSchedule);
        $createdCustomSchedule = $createdCustomSchedule->toArray();
        $this->assertArrayHasKey('id', $createdCustomSchedule);
        $this->assertNotNull($createdCustomSchedule['id'], 'Created CustomSchedule must have id specified');
        $this->assertNotNull(CustomSchedule::find($createdCustomSchedule['id']), 'CustomSchedule with given id must be in DB');
        $this->assertModelData($customSchedule, $createdCustomSchedule);
    }

    /**
     * @test read
     */
    public function testReadCustomSchedule()
    {
        $customSchedule = $this->makeCustomSchedule();
        $dbCustomSchedule = $this->customScheduleRepo->find($customSchedule->id);
        $dbCustomSchedule = $dbCustomSchedule->toArray();
        $this->assertModelData($customSchedule->toArray(), $dbCustomSchedule);
    }

    /**
     * @test update
     */
    public function testUpdateCustomSchedule()
    {
        $customSchedule = $this->makeCustomSchedule();
        $fakeCustomSchedule = $this->fakeCustomScheduleData();
        $updatedCustomSchedule = $this->customScheduleRepo->update($fakeCustomSchedule, $customSchedule->id);
        $this->assertModelData($fakeCustomSchedule, $updatedCustomSchedule->toArray());
        $dbCustomSchedule = $this->customScheduleRepo->find($customSchedule->id);
        $this->assertModelData($fakeCustomSchedule, $dbCustomSchedule->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteCustomSchedule()
    {
        $customSchedule = $this->makeCustomSchedule();
        $resp = $this->customScheduleRepo->delete($customSchedule->id);
        $this->assertTrue($resp);
        $this->assertNull(CustomSchedule::find($customSchedule->id), 'CustomSchedule should not exist in DB');
    }
}
