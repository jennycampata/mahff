<?php

use Faker\Factory as Faker;
use App\Models\Property;
use App\Repositories\Admin\PropertyRepository;

trait MakePropertyTrait
{
    /**
     * Create fake instance of Property and save it in database
     *
     * @param array $propertyFields
     * @return Property
     */
    public function makeProperty($propertyFields = [])
    {
        /** @var PropertyRepository $propertyRepo */
        $propertyRepo = App::make(PropertyRepository::class);
        $theme = $this->fakePropertyData($propertyFields);
        return $propertyRepo->create($theme);
    }

    /**
     * Get fake instance of Property
     *
     * @param array $propertyFields
     * @return Property
     */
    public function fakeProperty($propertyFields = [])
    {
        return new Property($this->fakePropertyData($propertyFields));
    }

    /**
     * Get fake data of Property
     *
     * @param array $postFields
     * @return array
     */
    public function fakePropertyData($propertyFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'project_id' => $fake->word,
            'block_id' => $fake->word,
            'area_id' => $fake->word,
            'extra_land' => $fake->word,
            'road_facing' => $fake->word,
            'park_facing' => $fake->word,
            'west_open' => $fake->word,
            'corner' => $fake->word,
            'status' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $propertyFields);
    }
}
