<?php

use Faker\Factory as Faker;
use App\Models\CustomSchedule;
use App\Repositories\Admin\CustomScheduleRepository;

trait MakeCustomScheduleTrait
{
    /**
     * Create fake instance of CustomSchedule and save it in database
     *
     * @param array $customScheduleFields
     * @return CustomSchedule
     */
    public function makeCustomSchedule($customScheduleFields = [])
    {
        /** @var CustomScheduleRepository $customScheduleRepo */
        $customScheduleRepo = App::make(CustomScheduleRepository::class);
        $theme = $this->fakeCustomScheduleData($customScheduleFields);
        return $customScheduleRepo->create($theme);
    }

    /**
     * Get fake instance of CustomSchedule
     *
     * @param array $customScheduleFields
     * @return CustomSchedule
     */
    public function fakeCustomSchedule($customScheduleFields = [])
    {
        return new CustomSchedule($this->fakeCustomScheduleData($customScheduleFields));
    }

    /**
     * Get fake data of CustomSchedule
     *
     * @param array $postFields
     * @return array
     */
    public function fakeCustomScheduleData($customScheduleFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'area_id' => $fake->word,
            'project_id' => $fake->word,
            'mode_id' => $fake->word,
            'time_duration' => $fake->word,
            'amount' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s'),
            'status' => $fake->word
        ], $customScheduleFields);
    }
}
