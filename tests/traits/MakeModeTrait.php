<?php

use Faker\Factory as Faker;
use App\Models\Mode;
use App\Repositories\Admin\ModeRepository;

trait MakeModeTrait
{
    /**
     * Create fake instance of Mode and save it in database
     *
     * @param array $modeFields
     * @return Mode
     */
    public function makeMode($modeFields = [])
    {
        /** @var ModeRepository $modeRepo */
        $modeRepo = App::make(ModeRepository::class);
        $theme = $this->fakeModeData($modeFields);
        return $modeRepo->create($theme);
    }

    /**
     * Get fake instance of Mode
     *
     * @param array $modeFields
     * @return Mode
     */
    public function fakeMode($modeFields = [])
    {
        return new Mode($this->fakeModeData($modeFields));
    }

    /**
     * Get fake data of Mode
     *
     * @param array $postFields
     * @return array
     */
    public function fakeModeData($modeFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $modeFields);
    }
}
