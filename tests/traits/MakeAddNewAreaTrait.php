<?php

use Faker\Factory as Faker;
use App\Models\AddNewArea;
use App\Repositories\Admin\AddNewAreaRepository;

trait MakeAddNewAreaTrait
{
    /**
     * Create fake instance of AddNewArea and save it in database
     *
     * @param array $addNewAreaFields
     * @return AddNewArea
     */
    public function makeAddNewArea($addNewAreaFields = [])
    {
        /** @var AddNewAreaRepository $addNewAreaRepo */
        $addNewAreaRepo = App::make(AddNewAreaRepository::class);
        $theme = $this->fakeAddNewAreaData($addNewAreaFields);
        return $addNewAreaRepo->create($theme);
    }

    /**
     * Get fake instance of AddNewArea
     *
     * @param array $addNewAreaFields
     * @return AddNewArea
     */
    public function fakeAddNewArea($addNewAreaFields = [])
    {
        return new AddNewArea($this->fakeAddNewAreaData($addNewAreaFields));
    }

    /**
     * Get fake data of AddNewArea
     *
     * @param array $postFields
     * @return array
     */
    public function fakeAddNewAreaData($addNewAreaFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $addNewAreaFields);
    }
}
