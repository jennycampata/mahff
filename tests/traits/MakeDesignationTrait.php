<?php

use Faker\Factory as Faker;
use App\Models\Designation;
use App\Repositories\Admin\DesignationRepository;

trait MakeDesignationTrait
{
    /**
     * Create fake instance of Designation and save it in database
     *
     * @param array $designationFields
     * @return Designation
     */
    public function makeDesignation($designationFields = [])
    {
        /** @var DesignationRepository $designationRepo */
        $designationRepo = App::make(DesignationRepository::class);
        $theme = $this->fakeDesignationData($designationFields);
        return $designationRepo->create($theme);
    }

    /**
     * Get fake instance of Designation
     *
     * @param array $designationFields
     * @return Designation
     */
    public function fakeDesignation($designationFields = [])
    {
        return new Designation($this->fakeDesignationData($designationFields));
    }

    /**
     * Get fake data of Designation
     *
     * @param array $postFields
     * @return array
     */
    public function fakeDesignationData($designationFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'name' => $fake->word,
            'created_at' => $fake->date('Y-m-d H:i:s'),
            'updated_at' => $fake->date('Y-m-d H:i:s'),
            'deleted_at' => $fake->date('Y-m-d H:i:s')
        ], $designationFields);
    }
}
