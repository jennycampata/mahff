<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddNewAreaApiTest extends TestCase
{
    use MakeAddNewAreaTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateAddNewArea()
    {
        $addNewArea = $this->fakeAddNewAreaData();
        $this->json('POST', '/api/v1/addNewAreas', $addNewArea);

        $this->assertApiResponse($addNewArea);
    }

    /**
     * @test
     */
    public function testReadAddNewArea()
    {
        $addNewArea = $this->makeAddNewArea();
        $this->json('GET', '/api/v1/addNewAreas/'.$addNewArea->id);

        $this->assertApiResponse($addNewArea->toArray());
    }

    /**
     * @test
     */
    public function testUpdateAddNewArea()
    {
        $addNewArea = $this->makeAddNewArea();
        $editedAddNewArea = $this->fakeAddNewAreaData();

        $this->json('PUT', '/api/v1/addNewAreas/'.$addNewArea->id, $editedAddNewArea);

        $this->assertApiResponse($editedAddNewArea);
    }

    /**
     * @test
     */
    public function testDeleteAddNewArea()
    {
        $addNewArea = $this->makeAddNewArea();
        $this->json('DELETE', '/api/v1/addNewAreas/'.$addNewArea->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/addNewAreas/'.$addNewArea->id);

        $this->assertResponseStatus(404);
    }
}
