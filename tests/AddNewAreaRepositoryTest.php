<?php

use App\Models\AddNewArea;
use App\Repositories\Admin\AddNewAreaRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AddNewAreaRepositoryTest extends TestCase
{
    use MakeAddNewAreaTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var AddNewAreaRepository
     */
    protected $addNewAreaRepo;

    public function setUp()
    {
        parent::setUp();
        $this->addNewAreaRepo = App::make(AddNewAreaRepository::class);
    }

    /**
     * @test create
     */
    public function testCreateAddNewArea()
    {
        $addNewArea = $this->fakeAddNewAreaData();
        $createdAddNewArea = $this->addNewAreaRepo->create($addNewArea);
        $createdAddNewArea = $createdAddNewArea->toArray();
        $this->assertArrayHasKey('id', $createdAddNewArea);
        $this->assertNotNull($createdAddNewArea['id'], 'Created AddNewArea must have id specified');
        $this->assertNotNull(AddNewArea::find($createdAddNewArea['id']), 'AddNewArea with given id must be in DB');
        $this->assertModelData($addNewArea, $createdAddNewArea);
    }

    /**
     * @test read
     */
    public function testReadAddNewArea()
    {
        $addNewArea = $this->makeAddNewArea();
        $dbAddNewArea = $this->addNewAreaRepo->find($addNewArea->id);
        $dbAddNewArea = $dbAddNewArea->toArray();
        $this->assertModelData($addNewArea->toArray(), $dbAddNewArea);
    }

    /**
     * @test update
     */
    public function testUpdateAddNewArea()
    {
        $addNewArea = $this->makeAddNewArea();
        $fakeAddNewArea = $this->fakeAddNewAreaData();
        $updatedAddNewArea = $this->addNewAreaRepo->update($fakeAddNewArea, $addNewArea->id);
        $this->assertModelData($fakeAddNewArea, $updatedAddNewArea->toArray());
        $dbAddNewArea = $this->addNewAreaRepo->find($addNewArea->id);
        $this->assertModelData($fakeAddNewArea, $dbAddNewArea->toArray());
    }

    /**
     * @test delete
     */
    public function testDeleteAddNewArea()
    {
        $addNewArea = $this->makeAddNewArea();
        $resp = $this->addNewAreaRepo->delete($addNewArea->id);
        $this->assertTrue($resp);
        $this->assertNull(AddNewArea::find($addNewArea->id), 'AddNewArea should not exist in DB');
    }
}
