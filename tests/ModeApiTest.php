<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ModeApiTest extends TestCase
{
    use MakeModeTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateMode()
    {
        $mode = $this->fakeModeData();
        $this->json('POST', '/api/v1/modes', $mode);

        $this->assertApiResponse($mode);
    }

    /**
     * @test
     */
    public function testReadMode()
    {
        $mode = $this->makeMode();
        $this->json('GET', '/api/v1/modes/'.$mode->id);

        $this->assertApiResponse($mode->toArray());
    }

    /**
     * @test
     */
    public function testUpdateMode()
    {
        $mode = $this->makeMode();
        $editedMode = $this->fakeModeData();

        $this->json('PUT', '/api/v1/modes/'.$mode->id, $editedMode);

        $this->assertApiResponse($editedMode);
    }

    /**
     * @test
     */
    public function testDeleteMode()
    {
        $mode = $this->makeMode();
        $this->json('DELETE', '/api/v1/modes/'.$mode->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/modes/'.$mode->id);

        $this->assertResponseStatus(404);
    }
}
