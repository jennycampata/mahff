<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CustomScheduleApiTest extends TestCase
{
    use MakeCustomScheduleTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreateCustomSchedule()
    {
        $customSchedule = $this->fakeCustomScheduleData();
        $this->json('POST', '/api/v1/customSchedules', $customSchedule);

        $this->assertApiResponse($customSchedule);
    }

    /**
     * @test
     */
    public function testReadCustomSchedule()
    {
        $customSchedule = $this->makeCustomSchedule();
        $this->json('GET', '/api/v1/customSchedules/'.$customSchedule->id);

        $this->assertApiResponse($customSchedule->toArray());
    }

    /**
     * @test
     */
    public function testUpdateCustomSchedule()
    {
        $customSchedule = $this->makeCustomSchedule();
        $editedCustomSchedule = $this->fakeCustomScheduleData();

        $this->json('PUT', '/api/v1/customSchedules/'.$customSchedule->id, $editedCustomSchedule);

        $this->assertApiResponse($editedCustomSchedule);
    }

    /**
     * @test
     */
    public function testDeleteCustomSchedule()
    {
        $customSchedule = $this->makeCustomSchedule();
        $this->json('DELETE', '/api/v1/customSchedules/'.$customSchedule->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/customSchedules/'.$customSchedule->id);

        $this->assertResponseStatus(404);
    }
}
