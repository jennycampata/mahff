-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 08:53 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mahff`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_queries`
--

CREATE TABLE `admin_queries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `api_calls_count`
--

CREATE TABLE `api_calls_count` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `areas`
--

CREATE TABLE `areas` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `areas`
--

INSERT INTO `areas` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '120 Sq Yard', '2019-11-27 17:20:16', '2019-11-27 17:20:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `id` int(11) UNSIGNED NOT NULL,
  `project_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `branch_name` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`id`, `project_id`, `name`, `branch_name`, `account`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'asd', 'asdasd', '3213', '2019-09-30 17:48:26', '2019-09-30 17:48:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` int(11) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bookings`
--

CREATE TABLE `bookings` (
  `id` int(11) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `property_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `block_id` int(11) DEFAULT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `client_id` int(11) DEFAULT NULL,
  `booking_date` datetime DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `file_no` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookings`
--

INSERT INTO `bookings` (`id`, `project_id`, `property_id`, `area_id`, `block_id`, `schedule_id`, `client_id`, `booking_date`, `employee_id`, `file_no`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 2, 10, 1, 2, 5, 17, '2019-09-25 00:00:00', 16, '233', '2019-09-23 17:51:44', '2019-09-23 17:51:44', NULL),
(5, 2, 10, 1, 2, 5, 17, '2019-10-10 00:00:00', 16, 'AAA', '2019-09-30 18:05:45', '2019-09-30 18:05:45', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_details`
--

CREATE TABLE `client_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact_no` int(20) DEFAULT NULL,
  `contact_no2` int(20) DEFAULT NULL,
  `postal_address` varchar(255) DEFAULT NULL,
  `cnic_no` int(20) DEFAULT NULL,
  `father_name_husband_name` varchar(255) DEFAULT NULL,
  `nominee_name` varchar(255) DEFAULT NULL,
  `nominee_relation` varchar(255) DEFAULT NULL,
  `nominee_address` varchar(255) DEFAULT NULL,
  `nominee_email` varchar(255) DEFAULT NULL,
  `nominee_contactno` int(20) DEFAULT NULL,
  `nominee_contactno2` int(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_details`
--

INSERT INTO `client_details` (`id`, `user_id`, `email`, `address`, `contact_no`, `contact_no2`, `postal_address`, `cnic_no`, `father_name_husband_name`, `nominee_name`, `nominee_relation`, `nominee_address`, `nominee_email`, `nominee_contactno`, `nominee_contactno2`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 11, NULL, '30061445', NULL, NULL, 'jaskhdk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-17 16:30:28', '2019-07-17 16:30:28', NULL),
(2, 17, NULL, '03061931997', NULL, NULL, 'hsdsak', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-08-21 16:28:57', '2019-08-21 16:28:57', NULL),
(3, 19, NULL, '032124558', NULL, NULL, 'sdasd', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-12-04 09:00:52', '2019-12-04 09:00:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_schedules`
--

CREATE TABLE `custom_schedules` (
  `id` int(11) UNSIGNED NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `mode_id` int(11) DEFAULT NULL,
  `time_duration` int(11) DEFAULT NULL,
  `amount` int(20) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `deposit`
--

CREATE TABLE `deposit` (
  `id` int(11) UNSIGNED NOT NULL,
  `bank_id` int(11) UNSIGNED DEFAULT NULL,
  `payment_id` int(11) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `designaitons`
--

CREATE TABLE `designaitons` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designaitons`
--

INSERT INTO `designaitons` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'CEO', '2019-07-18 14:41:56', '2019-07-18 14:41:56', NULL),
(2, 'employee', '2019-11-20 17:15:19', '2019-11-20 17:15:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_details`
--

CREATE TABLE `employee_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `designation_id` int(11) DEFAULT NULL,
  `employee_type_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `contact_no` varchar(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `cnic_no` varchar(11) DEFAULT NULL,
  `gender` tinyint(2) DEFAULT NULL,
  `marriage_status` tinyint(11) DEFAULT NULL,
  `salary` varchar(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_details`
--

INSERT INTO `employee_details` (`id`, `user_id`, `designation_id`, `employee_type_id`, `project_id`, `contact_no`, `address`, `cnic_no`, `gender`, `marriage_status`, `salary`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 7, 0, 0, 0, 'asd', 'asd', 'asd', 0, 0, '5000', '2019-07-17 15:19:26', '2019-07-17 15:19:26', NULL),
(2, 12, 1, 1, 2, '21312312313', 'asdasd12123', '4212122', 0, 0, '25000', '2019-08-21 16:09:13', '2019-08-21 16:09:13', NULL),
(3, 14, 1, 1, 2, '21312312313', 'asdasd12123', '4212122', 0, 0, '25000', '2019-08-21 16:11:20', '2019-08-21 16:11:20', NULL),
(4, 16, 1, 1, 2, '21312312313', 'asdasd12123', '4212122', 0, 0, '25000', '2019-08-21 16:12:04', '2019-08-21 16:12:04', NULL),
(5, 18, 2, 3, 1, 'demo', 'demo', '123456789', 1, 0, '0000', '2019-11-20 17:16:31', '2019-11-20 17:16:31', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_types`
--

CREATE TABLE `employee_types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee_types`
--

INSERT INTO `employee_types` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'freelancer', '2019-07-18 15:16:58', '2019-07-18 15:16:58', NULL),
(2, 'salary+commision', '2019-07-18 15:17:30', '2019-07-18 15:17:30', NULL),
(3, 'salary', '2019-11-20 17:15:33', '2019-11-20 17:15:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `inquirys`
--

CREATE TABLE `inquirys` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `contact_no` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locales`
--

CREATE TABLE `locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `native_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direction` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `locales`
--

INSERT INTO `locales` (`id`, `code`, `title`, `native_name`, `direction`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'en', 'English', NULL, 'LTR', 1, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(2, 'ar', 'Arabic', NULL, 'RTL', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(3, 'ab', 'Abkhaz', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(4, 'aa', 'Afar', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(5, 'af', 'Afrikaans', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(6, 'ak', 'Akan', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(7, 'sq', 'Albanian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(8, 'am', 'Amharic', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(9, 'an', 'Aragonese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(10, 'hy', 'Armenian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(11, 'as', 'Assamese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(12, 'av', 'Avaric', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(13, 'ae', 'Avestan', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(14, 'ay', 'Aymara', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(15, 'az', 'Azerbaijani', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(16, 'bm', 'Bambara', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(17, 'ba', 'Bashkir', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(18, 'eu', 'Basque', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(19, 'be', 'Belarusian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(20, 'bn', 'Bengali', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(21, 'bh', 'Bihari', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(22, 'bi', 'Bislama', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(23, 'bs', 'Bosnian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(24, 'br', 'Breton', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(25, 'bg', 'Bulgarian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(26, 'my', 'Burmese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(27, 'ca', 'Catalan; Valencian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(28, 'ch', 'Chamorro', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(29, 'ce', 'Chechen', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(30, 'ny', 'Chichewa; Chewa; Nyanja', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(31, 'zh', 'Chinese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(32, 'cv', 'Chuvash', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(33, 'kw', 'Cornish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(34, 'co', 'Corsican', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(35, 'cr', 'Cree', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(36, 'hr', 'Croatian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(37, 'cs', 'Czech', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(38, 'da', 'Danish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(39, 'dv', 'Divehi; Dhivehi; Maldivian;', NULL, 'RTL', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(40, 'nl', 'Dutch', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(41, 'eo', 'Esperanto', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(42, 'et', 'Estonian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(43, 'ee', 'Ewe', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(44, 'fo', 'Faroese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(45, 'fj', 'Fijian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(46, 'fi', 'Finnish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(47, 'fr', 'French', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(48, 'ff', 'Fula; Fulah; Pulaar; Pular', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(49, 'gl', 'Galician', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(50, 'ka', 'Georgian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(51, 'de', 'German', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(52, 'el', 'Greek, Modern', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(53, 'gn', 'Guaraní', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(54, 'gu', 'Gujarati', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(55, 'ht', 'Haitian; Haitian Creole', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(56, 'ha', 'Hausa', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(57, 'he', 'Hebrew (modern)', NULL, 'RTL', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(58, 'hz', 'Herero', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(59, 'hi', 'Hindi', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(60, 'ho', 'Hiri Motu', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(61, 'hu', 'Hungarian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(62, 'ia', 'Interlingua', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(63, 'id', 'Indonesian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(64, 'ie', 'Interlingue', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(65, 'ga', 'Irish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(66, 'ig', 'Igbo', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(67, 'ik', 'Inupiaq', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(68, 'io', 'Ido', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(69, 'is', 'Icelandic', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(70, 'it', 'Italian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(71, 'iu', 'Inuktitut', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(72, 'ja', 'Japanese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(73, 'jv', 'Javanese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(74, 'kl', 'Kalaallisut, Greenlandic', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(75, 'kn', 'Kannada', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(76, 'kr', 'Kanuri', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(77, 'ks', 'Kashmiri', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(78, 'kk', 'Kazakh', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(79, 'km', 'Khmer', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(80, 'ki', 'Kikuyu, Gikuyu', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(81, 'rw', 'Kinyarwanda', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(82, 'ky', 'Kirghiz, Kyrgyz', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(83, 'kv', 'Komi', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(84, 'kg', 'Kongo', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(85, 'ko', 'Korean', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(86, 'ku', 'Kurdish', NULL, 'RTL', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(87, 'kj', 'Kwanyama, Kuanyama', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(88, 'la', 'Latin', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(89, 'lb', 'Luxembourgish, Letzeburgesch', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(90, 'lg', 'Luganda', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(91, 'li', 'Limburgish, Limburgan, Limburger', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(92, 'ln', 'Lingala', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(93, 'lo', 'Lao', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(94, 'lt', 'Lithuanian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(95, 'lu', 'Luba-Katanga', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(96, 'lv', 'Latvian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(97, 'gv', 'Manx', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(98, 'mk', 'Macedonian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(99, 'mg', 'Malagasy', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(100, 'ms', 'Malay', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(101, 'ml', 'Malayalam', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(102, 'mt', 'Maltese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(103, 'mi', 'Māori', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(104, 'mr', 'Marathi (Marāṭhī)', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(105, 'mh', 'Marshallese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(106, 'mn', 'Mongolian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(107, 'na', 'Nauru', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(108, 'nv', 'Navajo, Navaho', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(109, 'nb', 'Norwegian Bokmål', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(110, 'nd', 'North Ndebele', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(111, 'ne', 'Nepali', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(112, 'ng', 'Ndonga', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(113, 'nn', 'Norwegian Nynorsk', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(114, 'no', 'Norwegian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(115, 'ii', 'Nuosu', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(116, 'nr', 'South Ndebele', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(117, 'oc', 'Occitan', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(118, 'oj', 'Ojibwe, Ojibwa', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(119, 'cu', 'Old Church Slavonic, Church Slavic, Church Slavonic, Old Bulgarian, Old Slavonic', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(120, 'om', 'Oromo', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(121, 'or', 'Oriya', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(122, 'os', 'Ossetian, Ossetic', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(123, 'pa', 'Panjabi, Punjabi', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(124, 'pi', 'Pāli', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(125, 'fa', 'Persian', NULL, 'RTL', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(126, 'pl', 'Polish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(127, 'ps', 'Pashto, Pushto', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(128, 'pt', 'Portuguese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(129, 'qu', 'Quechua', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(130, 'rm', 'Romansh', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(131, 'rn', 'Kirundi', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(132, 'ro', 'Romanian, Moldavian, Moldovan', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(133, 'ru', 'Russian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(134, 'sa', 'Sanskrit (Saṁskṛta)', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(135, 'sc', 'Sardinian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(136, 'sd', 'Sindhi', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(137, 'se', 'Northern Sami', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(138, 'sm', 'Samoan', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(139, 'sg', 'Sango', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(140, 'sr', 'Serbian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(141, 'gd', 'Scottish Gaelic; Gaelic', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(142, 'sn', 'Shona', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(143, 'si', 'Sinhala, Sinhalese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(144, 'sk', 'Slovak', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(145, 'sl', 'Slovene', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(146, 'so', 'Somali', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(147, 'st', 'Southern Sotho', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(148, 'es', 'Spanish; Castilian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(149, 'su', 'Sundanese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(150, 'sw', 'Swahili', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(151, 'ss', 'Swati', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(152, 'sv', 'Swedish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(153, 'ta', 'Tamil', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(154, 'te', 'Telugu', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(155, 'tg', 'Tajik', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(156, 'th', 'Thai', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(157, 'ti', 'Tigrinya', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(158, 'bo', 'Tibetan Standard, Tibetan, Central', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(159, 'tk', 'Turkmen', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(160, 'tl', 'Tagalog', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(161, 'tn', 'Tswana', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(162, 'to', 'Tonga (Tonga Islands)', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(163, 'tr', 'Turkish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(164, 'ts', 'Tsonga', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(165, 'tt', 'Tatar', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(166, 'tw', 'Twi', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(167, 'ty', 'Tahitian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(168, 'ug', 'Uighur, Uyghur', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(169, 'uk', 'Ukrainian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(170, 'ur', 'Urdu', NULL, 'RTL', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(171, 'uz', 'Uzbek', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(172, 've', 'Venda', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(173, 'vi', 'Viettitlese', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(174, 'vo', 'Volapük', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(175, 'wa', 'Walloon', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(176, 'cy', 'Welsh', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(177, 'wo', 'Wolof', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(178, 'fy', 'Western Frisian', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(179, 'xh', 'Xhosa', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(180, 'yi', 'Yiddish', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(181, 'yo', 'Yoruba', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL),
(182, 'za', 'Zhuang, Chuang', NULL, 'LTR', 0, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` tinyint(4) NOT NULL DEFAULT '0',
  `is_protected` tinyint(4) NOT NULL DEFAULT '0',
  `static` tinyint(4) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `module_id`, `name`, `icon`, `slug`, `position`, `is_protected`, `static`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Dashboard', 'fa fa-dashboard', 'dashboard', 1, 1, 1, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(2, 3, 'Users', 'fa fa-user', 'users', 2, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(3, 4, 'Roles', 'fa fa-edit', 'roles', 3, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(4, 5, 'Permissions', 'fa fa-check-square-o', 'permissions', 4, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(5, 6, 'Modules', 'fa fa-database', 'modules', 5, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(6, 7, 'Languages', 'fa fa-comments-o', 'languages', 6, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(7, 8, 'Pages', 'fa fa-wpforms', 'pages', 7, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(8, 9, 'Menus', 'fa fa-th', 'menus', 8, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(9, 10, 'Contact us', 'fa fa-mail-forward', 'contactus', 9, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(10, 11, 'Notifications', 'fa fa-bell', 'notifications', 10, 1, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(11, 12, 'Settings', 'fa fa-gears', 'settings', 11, 0, 0, 1, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(13, 14, 'Areas', 'fa fa-map', 'areas', 12, 0, 0, 1, '2019-07-15 17:52:11', '2019-07-15 17:52:11', NULL),
(14, 15, 'Blocks', 'fa fa-road', 'blocks', 13, 0, 0, 0, '2019-07-15 17:52:56', '2019-11-20 17:38:25', NULL),
(15, 17, 'Projects', 'fa fa-product-hunt', 'projects', 14, 0, 0, 1, '2019-07-15 17:55:00', '2019-07-15 17:55:00', NULL),
(16, 18, 'Properties', 'fa fa-building-o', 'properties', 15, 0, 0, 0, '2019-07-15 17:56:11', '2019-11-20 17:39:49', NULL),
(17, 19, 'Modes', 'fa fa-cc-paypal', 'modes', 16, 0, 0, 1, '2019-07-15 18:00:13', '2019-07-15 18:00:13', NULL),
(18, 20, 'Schedules', 'fa fa-newspaper-o', 'schedules', 17, 0, 0, 1, '2019-07-15 18:01:28', '2019-11-27 17:16:24', NULL),
(19, 21, 'Custom Schedules', 'fa fa-newspaper-o', 'custom-schedules', 18, 0, 0, 1, '2019-07-15 18:03:20', '2019-11-27 17:14:03', NULL),
(20, 22, 'Types', 'fa fa-glass', 'types', 19, 0, 0, 1, '2019-07-15 18:05:29', '2019-07-15 18:05:29', NULL),
(21, 23, 'Employees', 'fa fa-user', 'employees', 20, 0, 0, 1, '2019-07-16 15:23:50', '2019-07-16 15:23:50', NULL),
(23, 26, 'Clients', 'fa fa-users', 'clients', 21, 0, 0, 1, '2019-07-16 15:48:03', '2019-07-16 15:48:03', NULL),
(24, 27, 'Employee Types', 'fa fa-user', 'employee-types', 22, 0, 0, 1, '2019-07-16 18:50:14', '2019-07-16 18:50:14', NULL),
(25, 28, 'Designations', 'fa fa-user', 'designations', 23, 0, 0, 1, '2019-07-16 18:51:47', '2019-07-16 18:51:47', NULL),
(26, 30, 'Inquiries', 'fa fa-th-list', 'inquiries', 24, 0, 0, 1, '2019-09-04 16:00:36', '2019-09-04 16:00:36', NULL),
(27, 31, 'Bookings', 'fa fa-file-o', 'bookings', 25, 0, 0, 1, '2019-09-18 16:51:39', '2019-09-18 16:51:39', NULL),
(28, 32, 'Banks', 'fa fa-bank', 'banks', 26, 0, 0, 1, '2019-09-23 16:11:37', '2019-09-23 16:11:37', NULL),
(29, 33, 'Payments', 'fa fa-dollar', 'payments', 27, 0, 0, 0, '2019-09-23 17:41:10', '2019-09-23 18:16:19', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_06_134936_create_modules_table', 1),
(4, '2018_04_06_151644_entrust_setup_tables', 1),
(5, '2018_04_09_111106_add_soft_delete_in_users_table', 1),
(6, '2018_04_09_152013_create_menus_table', 1),
(7, '2018_07_12_083021_create_locales_table', 1),
(8, '2018_07_12_084644_create_pages_table', 1),
(9, '2018_07_13_181040_create_notification_table', 1),
(10, '2018_07_13_191027_create_admin_query_table', 1),
(11, '2018_10_02_055325_create_settings_table', 1),
(12, '2019_02_13_124216_create_api_calls_count_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modes`
--

CREATE TABLE `modes` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modes`
--

INSERT INTO `modes` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Booking', '2019-07-25 13:08:00', '2019-07-25 13:08:00', NULL),
(2, 'Allocation', '2019-07-25 13:08:11', '2019-07-25 13:08:11', NULL),
(3, 'Installment', '2019-07-25 13:08:24', '2019-07-25 13:08:24', NULL),
(4, 'Position', '2019-07-25 13:08:47', '2019-07-25 13:08:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `is_module` tinyint(4) NOT NULL DEFAULT '0',
  `config` text COLLATE utf8mb4_unicode_ci,
  `is_protected` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `slug`, `table_name`, `icon`, `status`, `is_module`, `config`, `is_protected`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin Panel', 'adminpanel', '-', 'fa fa-dashboard', 1, 0, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(2, 'Dashboard', 'dashboard', '-', 'fa fa-dashboard', 1, 0, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(3, 'Users', 'users', 'users', 'fa fa-user', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(4, 'Roles', 'roles', 'roles', 'fa fa-edit', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(5, 'Permissions', 'permissions', 'permissions', 'fa fa-check-square-o', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(6, 'Modules', 'modules', 'modules', 'fa fa-database', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(7, 'Languages', 'languages', 'locales', 'fa fa-comments-o', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(8, 'Page', 'pages', 'pages', 'fa fa-wpforms', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(9, 'ContactUs', 'contactus', 'admin_queries', 'fa fa-mail-forward', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(10, 'Notification', 'notifications', 'notifications', 'fa fa-bell', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(11, 'Menu', 'menus', 'menus', 'fa fa-th', 1, 1, 'null', 1, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(12, 'Setting', 'settings', 'app_settings', 'fa fa-gears', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"default_language\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"select\",\"validations\":\"required|exists:locales,code\",\"inIndex\":true,\"searchable\":true},{\"name\":\"email\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"email\",\"validations\":\"required|email\",\"inIndex\":true,\"searchable\":true},{\"name\":\"logo\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required|image|mimetypes:text\\/plain,...\",\"inIndex\":true,\"searchable\":true},{\"name\":\"phone\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"latitude\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"number\",\"validations\":\"required\",\"inIndex\":false,\"searchable\":false},{\"name\":\"longitude\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"number\",\"validations\":\"required\",\"inIndex\":false,\"searchable\":false},{\"name\":\"playstore\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":false,\"searchable\":false},{\"name\":\"appstore\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":false,\"searchable\":false},{\"name\":\"social_links\",\"primary\":false,\"dbType\":\"text,65535\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":false,\"searchable\":false},{\"name\":\"app_version\",\"primary\":false,\"dbType\":\"float\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"number\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"force_update\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"checkbox\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":true,\"searchable\":true},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 22:45:27', '2019-07-15 22:45:27', NULL),
(14, 'Area', 'areas', 'areas', 'fa fa-map', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 17:51:52', '2019-07-15 17:52:10', NULL),
(15, 'Block', 'blocks', 'blocks', 'fa fa-road', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"project_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 17:52:39', '2019-07-15 17:52:56', NULL),
(17, 'Project', 'projects', 'projects', 'fa fa-product-hunt', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"type_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"detail\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"address\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"starting_date\",\"primary\":false,\"dbType\":\"date\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"ending_date\",\"primary\":false,\"dbType\":\"date\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"count\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 17:54:34', '2019-07-15 17:55:00', NULL),
(18, 'Property', 'properties', 'properties', 'fa fa-building-o', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"project_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"block_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"area_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"extra_land\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"road_facing\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"park_facing\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"west_open\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"corner\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"status\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 17:55:45', '2019-07-15 17:56:11', NULL),
(19, 'Mode', 'modes', 'modes', 'fa fa-cc-paypal', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 17:59:05', '2019-07-15 18:00:12', NULL),
(20, 'Schedule', 'schedules', 'schedules', 'fa fa-newspaper-o', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"area_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"project_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"mode_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"time_duration\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"amount\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 18:00:56', '2019-07-15 18:01:28', NULL),
(21, 'CustomSchedule', 'custom-schedules', 'custom_schedules', 'fa fa-newspaper-o', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"area_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"project_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"mode_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"time_duration\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"amount\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"status\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 18:02:00', '2019-07-15 18:03:19', NULL),
(22, 'Type', 'types', 'types', 'fa fa-glass', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":true,\"searchable\":true},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-15 18:04:47', '2019-07-15 18:05:28', NULL),
(23, 'Employee', 'employees', 'users', 'fa fa-user', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"email\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"password\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"remember_token\",\"primary\":false,\"dbType\":\"string,100\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-16 15:22:58', '2019-07-16 15:23:49', NULL),
(26, 'Client', 'clients', 'users', 'fa fa-users', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"email\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"password\",\"primary\":false,\"dbType\":\"string,191\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"remember_token\",\"primary\":false,\"dbType\":\"string,100\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-16 15:47:45', '2019-07-16 15:48:03', NULL),
(27, 'EmployeeType', 'employee-types', 'employee_types', 'fa fa-user', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-16 18:49:02', '2019-07-16 18:50:14', NULL),
(28, 'Designation', 'designations', 'designaitons', 'fa fa-user', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-07-16 18:51:31', '2019-07-16 18:51:47', NULL),
(29, 'Inquiry', 'inquiries', 'inquirys', 'fa fa-glass', 0, 1, NULL, 0, '2019-09-04 15:58:17', '2019-09-04 15:58:17', NULL),
(30, 'Inquiry', 'inquiries', 'inquirys', 'fa fa-th-list', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"contact_no\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"email\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-09-04 16:00:02', '2019-09-04 16:00:36', NULL),
(31, 'Booking', 'bookings', 'bookings', 'fa fa-file-o', 1, 1, '[{\"name\":\"Id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"project_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"area_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"block_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"schedule_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"client_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"booking_date\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"employee_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"file_no\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-09-18 16:50:47', '2019-09-18 16:51:39', NULL),
(32, 'Bank', 'banks', 'banks', 'fa fa-bank', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"project_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"branch_name\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"account\",\"primary\":false,\"dbType\":\"string,255\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-09-23 16:11:17', '2019-09-23 16:11:37', NULL),
(33, 'Payment', 'payments', 'payments', 'fa fa-dollar', 1, 1, '[{\"name\":\"id\",\"primary\":true,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"type\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"property_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"schedule_id\",\"primary\":false,\"dbType\":\"increments\",\"fillable\":true,\"inForm\":true,\"htmlType\":\"text\",\"validations\":\"required\",\"inIndex\":true,\"searchable\":true},{\"name\":\"created_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"updated_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false},{\"name\":\"deleted_at\",\"primary\":false,\"dbType\":\"datetime\",\"fillable\":false,\"inForm\":false,\"htmlType\":false,\"validations\":false,\"inIndex\":false,\"searchable\":false}]', 0, '2019-09-23 17:40:48', '2019-09-23 17:41:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `sender_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ref_id` int(11) DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `notification_users`
--

CREATE TABLE `notification_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `notification_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '10=Sent, 20=Delivered, 30=Read',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `page_translations`
--

CREATE TABLE `page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) UNSIGNED NOT NULL,
  `type` int(11) DEFAULT NULL,
  `booking_id` int(11) UNSIGNED NOT NULL,
  `property_id` int(11) UNSIGNED DEFAULT NULL,
  `schedule_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE `payment_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `payment_id` int(11) UNSIGNED DEFAULT NULL,
  `description` text,
  `type` tinyint(2) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `cheque` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `module_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(4) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `module_id`, `name`, `display_name`, `is_protected`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'adminpanel', 'Admin Panel', 1, 'Admin Panel', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(2, 2, 'dashboard', 'Dashboard', 1, 'Dashboard', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(3, 3, 'users.index', 'List Users', 1, 'List Roles', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(4, 3, 'users.create', 'Create Users', 1, 'Create Users', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(5, 3, 'users.show', 'View User', 1, 'View User', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(6, 3, 'users.edit', 'Edit User', 1, 'Edit User', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(7, 3, 'users.destroy', 'Delete User', 1, 'Delete User', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(8, 4, 'roles.index', 'List Roles', 1, 'List Roles', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(9, 4, 'roles.create', 'Create Role', 1, 'Create Role', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(10, 4, 'roles.show', 'View Role', 1, 'View Role', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(11, 4, 'roles.edit', 'Edit Role', 1, 'Edit Role', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(12, 4, 'roles.destroy', 'Delete Role', 1, 'Delete Role', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(13, 5, 'permissions.index', 'List Permissions', 1, 'List Permissions', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(14, 5, 'permissions.create', 'Create Permission', 1, 'Create Permission', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(15, 5, 'permissions.show', 'View Permission', 1, 'View Permission', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(16, 5, 'permissions.edit', 'Edit Permission', 1, 'Edit Permission', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(17, 5, 'permissions.destroy', 'Delete Permission', 1, 'Delete Permission', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(18, 6, 'modules.index', 'List Modules', 1, 'List Modules', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(19, 6, 'modules.create', 'Create Module', 1, 'Create Module', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(20, 6, 'modules.show', 'View Module', 1, 'View Module', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(21, 6, 'modules.edit', 'Edit Module', 1, 'Edit Module', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(22, 6, 'modules.destroy', 'Delete Module', 1, 'Delete Module', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(23, 7, 'languages.index', 'List Languages', 1, 'List Languages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(24, 7, 'languages.create', 'Create Languages', 1, 'Create Languages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(25, 7, 'languages.show', 'View Languages', 1, 'View Languages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(26, 7, 'languages.edit', 'Edit Languages', 1, 'Edit Languages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(27, 7, 'languages.destroy', 'Delete Languages', 1, 'Delete Languages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(28, 8, 'pages.index', 'List Pages', 1, 'List Pages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(29, 8, 'pages.create', 'Create Pages', 1, 'Create Pages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(30, 8, 'pages.show', 'View Pages', 1, 'View Pages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(31, 8, 'pages.edit', 'Edit Pages', 1, 'Edit Pages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(32, 8, 'pages.destroy', 'Delete Pages', 1, 'Delete Pages', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(33, 9, 'contactus.index', 'List Contact Us', 1, 'List Contact Us Record', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(34, 9, 'contactus.create', 'Create Contact Us', 1, 'Create Contact Us Record', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(35, 9, 'contactus.show', 'View Contact Us', 1, 'View Contact Us Record', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(36, 9, 'contactus.edit', 'Edit Contact Us', 1, 'Edit Contact Us Record', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(37, 9, 'contactus.destroy', 'Delete Contact Us', 1, 'Delete Contact Us Record', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(38, 10, 'notifications.index', 'List Notification', 1, 'List Notification', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(39, 10, 'notifications.create', 'Create Notification', 1, 'Create Notification', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(40, 10, 'notifications.show', 'View Notification', 1, 'View Notification', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(41, 10, 'notifications.edit', 'Edit Notification', 1, 'Edit Notification', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(42, 10, 'notifications.destroy', 'Delete Notification', 1, 'Delete Notification', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(43, 11, 'menus.index', 'List Menu', 1, 'List Menu', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(44, 11, 'menus.create', 'Create Menu', 1, 'Create Menu', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(45, 11, 'menus.show', 'View Menu', 1, 'View Menu', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(46, 11, 'menus.edit', 'Edit Menu', 1, 'Edit Menu', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(47, 11, 'menus.destroy', 'Delete Menu', 1, 'Delete Menu', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(48, 12, 'settings.index', 'Settings', 0, 'Index Settings', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(49, 12, 'settings.create', 'Settings', 0, 'Create Settings', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(50, 12, 'settings.show', 'Settings', 0, 'Show Settings', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(51, 12, 'settings.edit', 'Settings', 0, 'Edit Settings', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(52, 12, 'settings.destroy', 'Settings', 0, 'Destroy Settings', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(58, 14, 'areas.index', 'Areas', 0, 'Index Areas', '2019-07-15 22:52:10', '2019-07-15 22:52:10', NULL),
(59, 14, 'areas.create', 'Areas', 0, 'Create Areas', '2019-07-15 22:52:10', '2019-07-15 22:52:10', NULL),
(60, 14, 'areas.show', 'Areas', 0, 'Show Areas', '2019-07-15 22:52:11', '2019-07-15 22:52:11', NULL),
(61, 14, 'areas.edit', 'Areas', 0, 'Edit Areas', '2019-07-15 22:52:11', '2019-07-15 22:52:11', NULL),
(62, 14, 'areas.destroy', 'Areas', 0, 'Destroy Areas', '2019-07-15 22:52:11', '2019-07-15 22:52:11', NULL),
(63, 15, 'blocks.index', 'Blocks', 0, 'Index Blocks', '2019-07-15 22:52:56', '2019-07-15 22:52:56', NULL),
(64, 15, 'blocks.create', 'Blocks', 0, 'Create Blocks', '2019-07-15 22:52:56', '2019-07-15 22:52:56', NULL),
(65, 15, 'blocks.show', 'Blocks', 0, 'Show Blocks', '2019-07-15 22:52:56', '2019-07-15 22:52:56', NULL),
(66, 15, 'blocks.edit', 'Blocks', 0, 'Edit Blocks', '2019-07-15 22:52:56', '2019-07-15 22:52:56', NULL),
(67, 15, 'blocks.destroy', 'Blocks', 0, 'Destroy Blocks', '2019-07-15 22:52:56', '2019-07-15 22:52:56', NULL),
(68, 17, 'projects.index', 'Projects', 0, 'Index Projects', '2019-07-15 22:55:00', '2019-07-15 22:55:00', NULL),
(69, 17, 'projects.create', 'Projects', 0, 'Create Projects', '2019-07-15 22:55:00', '2019-07-15 22:55:00', NULL),
(70, 17, 'projects.show', 'Projects', 0, 'Show Projects', '2019-07-15 22:55:00', '2019-07-15 22:55:00', NULL),
(71, 17, 'projects.edit', 'Projects', 0, 'Edit Projects', '2019-07-15 22:55:00', '2019-07-15 22:55:00', NULL),
(72, 17, 'projects.destroy', 'Projects', 0, 'Destroy Projects', '2019-07-15 22:55:00', '2019-07-15 22:55:00', NULL),
(73, 18, 'properties.index', 'Properties', 0, 'Index Properties', '2019-07-15 22:56:11', '2019-07-15 22:56:11', NULL),
(74, 18, 'properties.create', 'Properties', 0, 'Create Properties', '2019-07-15 22:56:11', '2019-07-15 22:56:11', NULL),
(75, 18, 'properties.show', 'Properties', 0, 'Show Properties', '2019-07-15 22:56:11', '2019-07-15 22:56:11', NULL),
(76, 18, 'properties.edit', 'Properties', 0, 'Edit Properties', '2019-07-15 22:56:11', '2019-07-15 22:56:11', NULL),
(77, 18, 'properties.destroy', 'Properties', 0, 'Destroy Properties', '2019-07-15 22:56:11', '2019-07-15 22:56:11', NULL),
(78, 19, 'modes.index', 'Modes', 0, 'Index Modes', '2019-07-15 23:00:12', '2019-07-15 23:00:12', NULL),
(79, 19, 'modes.create', 'Modes', 0, 'Create Modes', '2019-07-15 23:00:12', '2019-07-15 23:00:12', NULL),
(80, 19, 'modes.show', 'Modes', 0, 'Show Modes', '2019-07-15 23:00:12', '2019-07-15 23:00:12', NULL),
(81, 19, 'modes.edit', 'Modes', 0, 'Edit Modes', '2019-07-15 23:00:12', '2019-07-15 23:00:12', NULL),
(82, 19, 'modes.destroy', 'Modes', 0, 'Destroy Modes', '2019-07-15 23:00:12', '2019-07-15 23:00:12', NULL),
(83, 20, 'schedules.index', 'Schedules', 0, 'Index Schedules', '2019-07-15 23:01:28', '2019-07-15 23:01:28', NULL),
(84, 20, 'schedules.create', 'Schedules', 0, 'Create Schedules', '2019-07-15 23:01:28', '2019-07-15 23:01:28', NULL),
(85, 20, 'schedules.show', 'Schedules', 0, 'Show Schedules', '2019-07-15 23:01:28', '2019-07-15 23:01:28', NULL),
(86, 20, 'schedules.edit', 'Schedules', 0, 'Edit Schedules', '2019-07-15 23:01:28', '2019-07-15 23:01:28', NULL),
(87, 20, 'schedules.destroy', 'Schedules', 0, 'Destroy Schedules', '2019-07-15 23:01:28', '2019-07-15 23:01:28', NULL),
(88, 21, 'custom-schedules.index', 'Custom Schedules', 0, 'Index Custom Schedules', '2019-07-15 23:03:19', '2019-07-15 23:03:19', NULL),
(89, 21, 'custom-schedules.create', 'Custom Schedules', 0, 'Create Custom Schedules', '2019-07-15 23:03:20', '2019-07-15 23:03:20', NULL),
(90, 21, 'custom-schedules.show', 'Custom Schedules', 0, 'Show Custom Schedules', '2019-07-15 23:03:20', '2019-07-15 23:03:20', NULL),
(91, 21, 'custom-schedules.edit', 'Custom Schedules', 0, 'Edit Custom Schedules', '2019-07-15 23:03:20', '2019-07-15 23:03:20', NULL),
(92, 21, 'custom-schedules.destroy', 'Custom Schedules', 0, 'Destroy Custom Schedules', '2019-07-15 23:03:20', '2019-07-15 23:03:20', NULL),
(93, 22, 'types.index', 'Types', 0, 'Index Types', '2019-07-15 23:05:28', '2019-07-15 23:05:28', NULL),
(94, 22, 'types.create', 'Types', 0, 'Create Types', '2019-07-15 23:05:28', '2019-07-15 23:05:28', NULL),
(95, 22, 'types.show', 'Types', 0, 'Show Types', '2019-07-15 23:05:29', '2019-07-15 23:05:29', NULL),
(96, 22, 'types.edit', 'Types', 0, 'Edit Types', '2019-07-15 23:05:29', '2019-07-15 23:05:29', NULL),
(97, 22, 'types.destroy', 'Types', 0, 'Destroy Types', '2019-07-15 23:05:29', '2019-07-15 23:05:29', NULL),
(98, 23, 'employees.index', 'Employees', 0, 'Index Employees', '2019-07-16 20:23:49', '2019-07-16 20:23:49', NULL),
(99, 23, 'employees.create', 'Employees', 0, 'Create Employees', '2019-07-16 20:23:49', '2019-07-16 20:23:49', NULL),
(100, 23, 'employees.show', 'Employees', 0, 'Show Employees', '2019-07-16 20:23:50', '2019-07-16 20:23:50', NULL),
(101, 23, 'employees.edit', 'Employees', 0, 'Edit Employees', '2019-07-16 20:23:50', '2019-07-16 20:23:50', NULL),
(102, 23, 'employees.destroy', 'Employees', 0, 'Destroy Employees', '2019-07-16 20:23:50', '2019-07-16 20:23:50', NULL),
(108, 26, 'clients.index', 'Clients', 0, 'Index Clients', '2019-07-16 20:48:03', '2019-07-16 20:48:03', NULL),
(109, 26, 'clients.create', 'Clients', 0, 'Create Clients', '2019-07-16 20:48:03', '2019-07-16 20:48:03', NULL),
(110, 26, 'clients.show', 'Clients', 0, 'Show Clients', '2019-07-16 20:48:03', '2019-07-16 20:48:03', NULL),
(111, 26, 'clients.edit', 'Clients', 0, 'Edit Clients', '2019-07-16 20:48:03', '2019-07-16 20:48:03', NULL),
(112, 26, 'clients.destroy', 'Clients', 0, 'Destroy Clients', '2019-07-16 20:48:03', '2019-07-16 20:48:03', NULL),
(113, 27, 'employee-types.index', 'Employee Types', 0, 'Index Employee Types', '2019-07-16 23:50:14', '2019-07-16 23:50:14', NULL),
(114, 27, 'employee-types.create', 'Employee Types', 0, 'Create Employee Types', '2019-07-16 23:50:14', '2019-07-16 23:50:14', NULL),
(115, 27, 'employee-types.show', 'Employee Types', 0, 'Show Employee Types', '2019-07-16 23:50:14', '2019-07-16 23:50:14', NULL),
(116, 27, 'employee-types.edit', 'Employee Types', 0, 'Edit Employee Types', '2019-07-16 23:50:14', '2019-07-16 23:50:14', NULL),
(117, 27, 'employee-types.destroy', 'Employee Types', 0, 'Destroy Employee Types', '2019-07-16 23:50:14', '2019-07-16 23:50:14', NULL),
(118, 28, 'designations.index', 'Designations', 0, 'Index Designations', '2019-07-16 23:51:47', '2019-07-16 23:51:47', NULL),
(119, 28, 'designations.create', 'Designations', 0, 'Create Designations', '2019-07-16 23:51:47', '2019-07-16 23:51:47', NULL),
(120, 28, 'designations.show', 'Designations', 0, 'Show Designations', '2019-07-16 23:51:47', '2019-07-16 23:51:47', NULL),
(121, 28, 'designations.edit', 'Designations', 0, 'Edit Designations', '2019-07-16 23:51:47', '2019-07-16 23:51:47', NULL),
(122, 28, 'designations.destroy', 'Designations', 0, 'Destroy Designations', '2019-07-16 23:51:47', '2019-07-16 23:51:47', NULL),
(123, 30, 'inquiries.index', 'Inquiries', 0, 'Index Inquiries', '2019-09-04 21:00:36', '2019-09-04 21:00:36', NULL),
(124, 30, 'inquiries.create', 'Inquiries', 0, 'Create Inquiries', '2019-09-04 21:00:36', '2019-09-04 21:00:36', NULL),
(125, 30, 'inquiries.show', 'Inquiries', 0, 'Show Inquiries', '2019-09-04 21:00:36', '2019-09-04 21:00:36', NULL),
(126, 30, 'inquiries.edit', 'Inquiries', 0, 'Edit Inquiries', '2019-09-04 21:00:36', '2019-09-04 21:00:36', NULL),
(127, 30, 'inquiries.destroy', 'Inquiries', 0, 'Destroy Inquiries', '2019-09-04 21:00:36', '2019-09-04 21:00:36', NULL),
(128, 31, 'bookings.index', 'Bookings', 0, 'Index Bookings', '2019-09-18 21:51:39', '2019-09-18 21:51:39', NULL),
(129, 31, 'bookings.create', 'Bookings', 0, 'Create Bookings', '2019-09-18 21:51:39', '2019-09-18 21:51:39', NULL),
(130, 31, 'bookings.show', 'Bookings', 0, 'Show Bookings', '2019-09-18 21:51:39', '2019-09-18 21:51:39', NULL),
(131, 31, 'bookings.edit', 'Bookings', 0, 'Edit Bookings', '2019-09-18 21:51:39', '2019-09-18 21:51:39', NULL),
(132, 31, 'bookings.destroy', 'Bookings', 0, 'Destroy Bookings', '2019-09-18 21:51:39', '2019-09-18 21:51:39', NULL),
(133, 32, 'banks.index', 'Banks', 0, 'Index Banks', '2019-09-23 21:11:37', '2019-09-23 21:11:37', NULL),
(134, 32, 'banks.create', 'Banks', 0, 'Create Banks', '2019-09-23 21:11:37', '2019-09-23 21:11:37', NULL),
(135, 32, 'banks.show', 'Banks', 0, 'Show Banks', '2019-09-23 21:11:37', '2019-09-23 21:11:37', NULL),
(136, 32, 'banks.edit', 'Banks', 0, 'Edit Banks', '2019-09-23 21:11:37', '2019-09-23 21:11:37', NULL),
(137, 32, 'banks.destroy', 'Banks', 0, 'Destroy Banks', '2019-09-23 21:11:37', '2019-09-23 21:11:37', NULL),
(138, 33, 'payments.index', 'Payments', 0, 'Index Payments', '2019-09-23 22:41:09', '2019-09-23 22:41:09', NULL),
(139, 33, 'payments.create', 'Payments', 0, 'Create Payments', '2019-09-23 22:41:10', '2019-09-23 22:41:10', NULL),
(140, 33, 'payments.show', 'Payments', 0, 'Show Payments', '2019-09-23 22:41:10', '2019-09-23 22:41:10', NULL),
(141, 33, 'payments.edit', 'Payments', 0, 'Edit Payments', '2019-09-23 22:41:10', '2019-09-23 22:41:10', NULL),
(142, 33, 'payments.destroy', 'Payments', 0, 'Destroy Payments', '2019-09-23 22:41:10', '2019-09-23 22:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(1, 4),
(2, 1),
(2, 2),
(2, 4),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(8, 2),
(9, 1),
(9, 2),
(10, 1),
(10, 2),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 2),
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(23, 2),
(24, 1),
(24, 2),
(25, 1),
(25, 2),
(26, 1),
(26, 2),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(37, 2),
(38, 1),
(38, 2),
(39, 1),
(39, 2),
(40, 1),
(40, 2),
(41, 1),
(41, 2),
(42, 1),
(42, 2),
(43, 1),
(43, 2),
(44, 1),
(44, 2),
(45, 1),
(45, 2),
(46, 1),
(46, 2),
(47, 1),
(47, 2),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(58, 1),
(58, 2),
(58, 4),
(59, 1),
(59, 2),
(59, 4),
(60, 1),
(60, 2),
(60, 4),
(61, 1),
(61, 2),
(61, 4),
(62, 1),
(62, 2),
(63, 1),
(63, 2),
(63, 4),
(64, 1),
(64, 2),
(64, 4),
(65, 1),
(65, 2),
(65, 4),
(66, 1),
(66, 2),
(66, 4),
(67, 1),
(67, 2),
(68, 1),
(68, 2),
(68, 4),
(69, 1),
(69, 2),
(69, 4),
(70, 1),
(70, 2),
(70, 4),
(71, 1),
(71, 2),
(71, 4),
(72, 1),
(72, 2),
(73, 1),
(73, 2),
(74, 1),
(74, 2),
(74, 4),
(75, 1),
(75, 2),
(75, 4),
(76, 1),
(76, 2),
(76, 4),
(77, 1),
(77, 2),
(78, 1),
(78, 2),
(79, 1),
(79, 2),
(80, 1),
(80, 2),
(81, 1),
(81, 2),
(82, 1),
(82, 2),
(83, 1),
(83, 2),
(84, 1),
(84, 2),
(85, 1),
(85, 2),
(86, 1),
(86, 2),
(87, 1),
(87, 2),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(93, 2),
(94, 1),
(94, 2),
(95, 1),
(95, 2),
(96, 1),
(96, 2),
(97, 1),
(97, 2),
(98, 1),
(98, 2),
(99, 1),
(99, 2),
(100, 1),
(100, 2),
(101, 1),
(101, 2),
(102, 1),
(102, 2),
(108, 1),
(108, 2),
(109, 1),
(109, 2),
(110, 1),
(110, 2),
(111, 1),
(111, 2),
(112, 1),
(112, 2),
(113, 1),
(113, 2),
(114, 1),
(114, 2),
(115, 1),
(115, 2),
(116, 1),
(116, 2),
(117, 1),
(117, 2),
(118, 1),
(118, 2),
(119, 1),
(119, 2),
(120, 1),
(120, 2),
(121, 1),
(121, 2),
(122, 1),
(122, 2),
(123, 1),
(123, 2),
(124, 1),
(124, 2),
(125, 1),
(125, 2),
(126, 1),
(126, 2),
(127, 1),
(127, 2),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1);

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `detail` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `starting_date` varchar(255) DEFAULT NULL,
  `ending_date` varchar(255) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `type_id`, `detail`, `address`, `starting_date`, `ending_date`, `count`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Raji City', 1, 'basdjhjd', 'dnsajkdasj', '01--01-2019', '01-01-2019', 30000, '2019-09-04 23:55:13', '2019-07-30 14:57:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) UNSIGNED NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `block_id` int(11) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `extra_land` int(11) DEFAULT NULL,
  `road_facing` int(11) DEFAULT NULL,
  `park_facing` int(11) DEFAULT NULL,
  `west_open` int(11) DEFAULT NULL,
  `corner` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(4) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `is_protected`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'super-admin', 'Super Admin', 1, 'Super Admin has all permissions', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(2, 'admin', 'Administrators', 1, 'Assign this role to all the users who are administrators.', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(3, 'authenticated', 'Authenticated', 1, 'Authenticated users will be able to access front-end functionality', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(4, 'employee', 'employee', 0, 'employee', '2019-07-16 15:22:10', '2019-07-16 15:22:10', NULL),
(5, 'client', 'client', 0, 'client', '2019-07-16 15:33:17', '2019-07-16 15:33:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(2, 2),
(2, 3),
(16, 4),
(17, 5),
(18, 4),
(19, 5);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) UNSIGNED NOT NULL,
  `area_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `status` tinyint(2) DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_details`
--

CREATE TABLE `schedule_details` (
  `id` int(11) UNSIGNED NOT NULL,
  `schedule_id` int(11) DEFAULT NULL,
  `mode_id` int(11) DEFAULT NULL,
  `time_duration` int(11) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `default_language` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'en',
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `playstore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `appstore` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_links` text COLLATE utf8mb4_unicode_ci,
  `app_version` double(3,1) NOT NULL,
  `force_update` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `default_language`, `email`, `logo`, `phone`, `latitude`, `longitude`, `playstore`, `appstore`, `social_links`, `app_version`, `force_update`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'en', NULL, 'public/logo.png', NULL, NULL, NULL, NULL, NULL, NULL, 1.0, '1', '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `setting_translations`
--

CREATE TABLE `setting_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `setting_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `setting_translations`
--

INSERT INTO `setting_translations` (`id`, `setting_id`, `locale`, `title`, `address`, `about`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'en', 'Infyom Ingic', NULL, NULL, '2019-07-15 22:45:29', '2019-07-15 22:45:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Society', '2019-07-25 12:56:23', '2019-07-25 12:56:23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Super Admin', 'superadmin@ingic.com', '$2y$10$Baf6eM3qwljMHR6AWfXw5exLOV1LqFOUx.zYQIssa.IJOhZTOgkOW', 'eIAt9I1YDLFJ7bEGB4xNszKw4yzQNRLCORtecwgUEtwFOpWP65KrnVVFM7Bn', '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(2, 'Admin', 'admin@ingic.com', '$2y$10$zVVKc8xNk9LZEKv10XJblu/QA8j.qiO5IhrVLg/WdpR5WIu2uuHsG', NULL, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(7, 'raza dar', 'razadar97@gmail.com', '$2y$10$2zLvgOB6oaoR.YqML4shbOX9JvwEnto3DP0BeDY5fCVLq0CLlld82', NULL, '2019-07-17 15:19:26', '2019-07-17 15:19:26', NULL),
(11, 'farhaad', 'farhad.surani@gmail.com', '$2y$10$GLTlmxmci1YhSNj/ENpB2.Llq2ecd5wHiDVqX2WzJKAe6LEjifulG', NULL, '2019-07-17 16:30:28', '2019-07-17 16:30:28', NULL),
(12, 'New', 'new@gmail.com', '$2y$10$yZOj6PQLelDq.O5ksasxsewL9ddtsEQQBEV58WpMHDW0hl7ZmYCa2', NULL, '2019-08-21 16:09:13', '2019-08-21 16:09:13', NULL),
(14, 'New', 'snew@gmail.com', '$2y$10$HrHfoiRjzbRPuKqyG5zdruRsAwwfrtTPCMNVlPatuQ/lvpwQcrRr6', NULL, '2019-08-21 16:11:20', '2019-08-21 16:11:20', NULL),
(16, 'New', 'snesw@gmail.com', '$2y$10$ERSonLGqjt9.mN4DvCHoAOphtydHEBOA2CDvEQontvbwhEZLhnhD2', NULL, '2019-08-21 16:12:04', '2019-08-21 16:12:04', NULL),
(17, 'shaz', 'shaz@gmail.com', '$2y$10$ZXY2HQN4Z4qQI/HAjp4N8ODplg5PF/fuaoQdrz3wBD/fd5Q0gH2C2', NULL, '2019-08-21 16:28:57', '2019-08-21 16:28:57', NULL),
(18, 'demo', 'demo@mahff.com', '$2y$10$WThXVZvzwXJs.TM9Ad4XieYLQ2Y6D0GmbLrciDU3cYyzwD7Aq/rqW', 'hkfDF8m60w2hXnacCgYl1DwR2FodcQHEzUg8g5JJhNndkjPiIcj5NN2I8hNE', '2019-11-20 17:16:31', '2019-11-20 17:16:31', NULL),
(19, 'test', 'test@gmail.com', '$2y$10$a5/UoT/.pbmnLpd8GSsRheeiZJ4tfRGY24x8RHjGQ6M9HZueXrQdy', NULL, '2019-12-04 09:00:52', '2019-12-04 09:00:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_verified` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0,1',
  `email_updates` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0,1',
  `is_social_login` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`id`, `user_id`, `first_name`, `last_name`, `phone`, `address`, `image`, `is_verified`, `email_updates`, `is_social_login`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'Super', 'Admin', NULL, NULL, NULL, 1, 1, 0, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(2, 2, 'Admin', 'User', NULL, NULL, NULL, 1, 1, 0, '2019-07-15 22:45:28', '2019-07-15 22:45:28', NULL),
(3, 18, NULL, NULL, NULL, NULL, NULL, 1, 0, 0, '2019-11-20 22:25:05', '2019-11-20 22:25:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_devices`
--

CREATE TABLE `user_devices` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `device_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `device_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `push_notification` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_social_accounts`
--

CREATE TABLE `user_social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `platform` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0,1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_queries`
--
ALTER TABLE `admin_queries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_calls_count`
--
ALTER TABLE `api_calls_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_details`
--
ALTER TABLE `client_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_schedules`
--
ALTER TABLE `custom_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposit`
--
ALTER TABLE `deposit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designaitons`
--
ALTER TABLE `designaitons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_details`
--
ALTER TABLE `employee_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee_types`
--
ALTER TABLE `employee_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inquirys`
--
ALTER TABLE `inquirys`
  ADD PRIMARY KEY (`id`,`created_at`,`updated_at`);

--
-- Indexes for table `locales`
--
ALTER TABLE `locales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_module_id_foreign` (`module_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modes`
--
ALTER TABLE `modes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_sender_id_foreign` (`sender_id`);

--
-- Indexes for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notification_users_user_id_foreign` (`user_id`),
  ADD KEY `notification_users_notification_id_foreign` (`notification_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `page_translations_page_id_foreign` (`page_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`),
  ADD KEY `permissions_module_id_foreign` (`module_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedule_details`
--
ALTER TABLE `schedule_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `setting_translations`
--
ALTER TABLE `setting_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `setting_translations_setting_id_foreign` (`setting_id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_details_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_devices`
--
ALTER TABLE `user_devices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_devices_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_social_accounts`
--
ALTER TABLE `user_social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_social_accounts_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_queries`
--
ALTER TABLE `admin_queries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `api_calls_count`
--
ALTER TABLE `api_calls_count`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `client_details`
--
ALTER TABLE `client_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `custom_schedules`
--
ALTER TABLE `custom_schedules`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deposit`
--
ALTER TABLE `deposit`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `designaitons`
--
ALTER TABLE `designaitons`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_details`
--
ALTER TABLE `employee_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `employee_types`
--
ALTER TABLE `employee_types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `inquirys`
--
ALTER TABLE `inquirys`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `locales`
--
ALTER TABLE `locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=183;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `modes`
--
ALTER TABLE `modes`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_users`
--
ALTER TABLE `notification_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `page_translations`
--
ALTER TABLE `page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_details`
--
ALTER TABLE `payment_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedule_details`
--
ALTER TABLE `schedule_details`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `setting_translations`
--
ALTER TABLE `setting_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `types`
--
ALTER TABLE `types`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_devices`
--
ALTER TABLE `user_devices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user_social_accounts`
--
ALTER TABLE `user_social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_sender_id_foreign` FOREIGN KEY (`sender_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `notification_users`
--
ALTER TABLE `notification_users`
  ADD CONSTRAINT `notification_users_notification_id_foreign` FOREIGN KEY (`notification_id`) REFERENCES `notifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `notification_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `page_translations`
--
ALTER TABLE `page_translations`
  ADD CONSTRAINT `page_translations_page_id_foreign` FOREIGN KEY (`page_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `setting_translations`
--
ALTER TABLE `setting_translations`
  ADD CONSTRAINT `setting_translations_setting_id_foreign` FOREIGN KEY (`setting_id`) REFERENCES `settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_details`
--
ALTER TABLE `user_details`
  ADD CONSTRAINT `user_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_devices`
--
ALTER TABLE `user_devices`
  ADD CONSTRAINT `user_devices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user_social_accounts`
--
ALTER TABLE `user_social_accounts`
  ADD CONSTRAINT `user_social_accounts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
